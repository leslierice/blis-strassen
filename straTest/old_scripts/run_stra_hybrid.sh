#!/bin/bash
#export KMP_AFFINITY=compact,verbose
export KMP_AFFINITY=compact
export OMP_NUM_THREADS=1
export BLIS_IC_NT=1
export BLIS_JR_NT=1


#k_start=240
#k_end=2400
#k_blocksize=240

#200, 8000, 200
#256, 1032*32, 256
#1280, 1280*32, 1280
#1920, 1920*32, 1920
#240, 240*32, 240

k_start=256
k_end=8192
k_blocksize=256

echo "sb_stra=["
for (( k=k_start; k<=k_end; k+=k_blocksize ))
do
    #./test_stra_hybrid_blis.x     $k 256 $k 
    ./test_stra_hybrid_blis.x     $k $k $k 
done
echo "];"

