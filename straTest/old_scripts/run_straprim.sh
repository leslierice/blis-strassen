#!/bin/bash
#export KMP_AFFINITY=compact,verbose
export KMP_AFFINITY=compact
export OMP_NUM_THREADS=1
export BLIS_IC_NT=1
export BLIS_JR_NT=1


#./test_straprim_blis.x  33 1 33
#./test_straprim_blis.x  33 1 1

#k_start=240
#k_end=2400
#k_blocksize=240

#k_start=8
#k_end=2048
#k_blocksize=8

k_start=32
k_end=2048
k_blocksize=32

#k_start=1920
#k_end=1920
#k_blocksize=32

echo "sb_straprim=["
for (( k=k_start; k<=k_end; k+=k_blocksize ))
do
    #./test_straprim_blis.x     $k 256 $k 
    ./test_straprim_blis.x     $k $k $k 
done
echo "];"

