#include <unistd.h>
#include "blis.h"
#include "assert.h"

#include "blis_straprim_ref.h"


int main( int argc, char** argv )
{
    dim_t m, k, n;

    if ( argc != 4 ) {
        printf( "Error: require 3 arguments, but only %d provided.\n", argc - 1 );
        exit( 0 );
    }

    sscanf( argv[ 1 ], "%lu", &m );
    sscanf( argv[ 2 ], "%lu", &k );
    sscanf( argv[ 3 ], "%lu", &n );

    //common used for the interface
    obj_t alpha1, alpha2, beta1, beta2, gamma, delta;
    //STRAPRIM implementation
    obj_t aA, aB, bA, bB, cA, cB;
    obj_t cA_strassen, cB_strassen;
    //For reference implementation
    obj_t cA_conventional, cB_conventional;


    dim_t p;
    dim_t p_begin, p_end, p_inc;
    dim_t   m_input;
    num_t dt;
    dim_t   r, n_repeats;
    trans_t  transa;
    trans_t  transb;

    double dtime;
    double dtime_save;
    double dtime_conventional=1.0e9;
    double dtime_strassen=1.0e9;
    double gflops_conventional;
    double egflops_strassen;

    //printf("sizeof(dim_t):%d\n", sizeof(dim_t) );

    bli_init();

    bli_error_checking_level_set( BLIS_NO_ERROR_CHECKING );

    n_repeats = 3;

    dt = BLIS_DOUBLE;
    transa = BLIS_NO_TRANSPOSE;
    transb = BLIS_NO_TRANSPOSE;

    // Initialize the scalar: alpha1, alpha2, beta1, beta2, gamma, delta
    alpha1 = BLIS_ONE; alpha2 = BLIS_ONE;
    //gamma = BLIS_MINUS_ONE;  delta = BLIS_MINUS_ONE;
    gamma = BLIS_ONE;  delta = BLIS_ONE;
    //beta1 = BLIS_ZERO;  beta2 = BLIS_ZERO;
    beta1 = BLIS_ONE;  beta2 = BLIS_ONE;


    bli_obj_create( dt, m, k, 0, 0, &aA );
    bli_obj_create( dt, m, k, 0, 0, &aB );
    bli_obj_create( dt, k, n, 0, 0, &bA );
    bli_obj_create( dt, k, n, 0, 0, &bB );
    bli_obj_create( dt, m, n, 0, 0, &cA_conventional );
    bli_obj_create( dt, m, n, 0, 0, &cB_conventional );
    bli_obj_create( dt, m, n, 0, 0, &cA_strassen );
    bli_obj_create( dt, m, n, 0, 0, &cB_strassen );


    bli_obj_create( dt, m, n, 0, 0, &cA );
    bli_obj_create( dt, m, n, 0, 0, &cB );


    bli_randm( &aA ); bli_randm( &aB );
    bli_randm( &bA ); bli_randm( &bB );
    bli_randm( &cA ); bli_randm( &cB );


    //bli_setm( &BLIS_ONE, &aA );
    //bli_setm( &BLIS_ONE, &aB );
    //bli_setm( &BLIS_ONE, &bA );
    //bli_setm( &BLIS_ONE, &bB );
    //bli_setm( &BLIS_ONE, &cA );
    //bli_setm( &BLIS_TWO, &cB );

    bli_obj_set_conjtrans( transb, bB );
    bli_obj_set_conjtrans( transb, bA );
    bli_obj_set_conjtrans( transa, aA );
    bli_obj_set_conjtrans( transa, aB );


    dtime_save = 1.0e9;

    for ( r = 0; r < n_repeats; ++r )
    {
        dtime_conventional = 1.0e9;
        dtime_strassen =1.0e9;


        //Zero out result matrices
        bli_setm( &BLIS_ONE, &cA_conventional );
        bli_setm( &BLIS_ONE, &cA_strassen );
        bli_setm( &BLIS_TWO, &cB_conventional );
        bli_setm( &BLIS_TWO, &cB_strassen );

        bli_copym( &cA, &cA_conventional );
        bli_copym( &cB, &cB_conventional );
        bli_copym( &cA, &cA_strassen );
        bli_copym( &cB, &cB_strassen );


        dtime = bli_clock();

        //printf( "before straprim_ref\n" ); fflush(stdout);
        bli_straprim_ref( &alpha1, &alpha2,
                &aA, &gamma, &aB, 
                &bA, &delta, &bB, 
                &beta1, &beta2,
                &cA_conventional, &cB_conventional );
        //printf( "after straprim_ref\n" ); fflush(stdout);

        dtime_conventional = bli_clock_min_diff( dtime_conventional, dtime );

        //bli_printm( "aA", &aA, "%4.1f", "" );
        //bli_printm( "aB", &aB, "%4.1f", "" );
        //bli_printm( "bA", &bA, "%4.1f", "" );
        //bli_printm( "bB", &bB, "%4.1f", "" );


        dtime = bli_clock();
        //cA = beta1*cA+alpha1*(aA+gamma*aB)(bA+delta*bB)
        //cB = beta2*cB+alpha2*(aA+gamma*aB)(bA+delta*bB) 
        //printf( "before straprim\n" ); fflush(stdout);
        bli_straprim( &alpha1, &alpha2,
                &aA, &gamma, &aB, 
                &bA, &delta, &bB, 
                &beta1, &beta2,
                &cA_strassen, &cB_strassen );
        //printf( "after straprim\n" ); fflush(stdout);
        dtime_strassen = bli_clock_min_diff( dtime_strassen, dtime );
        //blis_gemm( &alpha, &aA, &bA, &cA_strassen );

    } // end r < n_repeats


    //Print the result explicitly
    //bli_printm( "cA_strassen", &cA_strassen, "%4.1f", "" );
    //bli_printm( "cA_conventional", &cA_conventional, "%4.1f", "" );
    //bli_printm( "cB_strassen", &cB_strassen, "%4.1f", "" );
    //bli_printm( "cB_conventional", &cB_conventional, "%4.1f", "" );

    if ( m < 1000 ) {
        computeError( m, m, m, n, bli_obj_buffer( cA_strassen ), bli_obj_buffer( cA_conventional ) );
        computeError( m, m, m, n, bli_obj_buffer( cB_strassen ), bli_obj_buffer( cB_conventional ) );
    }


    gflops_conventional = ( 2.0 * m * n * k ) / ( dtime_conventional * 1.0e9 );
    egflops_strassen = ( 2.0 * m * n * k ) / ( dtime_strassen * 1.0e9 );


    //gflops_conventional = ( 2.0 * m * k * n ) / ( dtime_conventional * 1.0e9 );
    //egflops_strassen = ( 2.0 * m * k * n ) / ( dtime_strassen * 1.0e9 );

    printf( "%6lu %6lu %6lu %10.3e %6.3f %10.3e %6.3f\n",
            ( uint64_t ) m,
            ( uint64_t ) k,
            ( uint64_t ) n,
            dtime_conventional, gflops_conventional,
            dtime_strassen, egflops_strassen );

    fflush(stdout);

    bli_obj_free( &aA ); bli_obj_free( &aB );
    bli_obj_free( &bA ); bli_obj_free( &bB );
    bli_obj_free( &cA_conventional );
    bli_obj_free( &cB_conventional );
    bli_obj_free( &cA_strassen );
    bli_obj_free( &cB_strassen );

    bli_finalize();

    return 0;
}

