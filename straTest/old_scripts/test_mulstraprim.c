#include <unistd.h>
#include "blis.h"
#include "assert.h"

#include "blis_mulstraprim_ref.h"


int main( int argc, char** argv )
{
    dim_t m, k, n;

    if ( argc != 4 ) {
        printf( "Error: require 3 arguments, but only %d provided.\n", argc - 1 );
        exit( 0 );
    }

    sscanf( argv[ 1 ], "%lu", &m );
    sscanf( argv[ 2 ], "%lu", &k );
    sscanf( argv[ 3 ], "%lu", &n );

    //common used for the interface
    obj_t alpha1, alpha2, beta1, beta2, gamma, delta;
    //STRAPRIM implementation
    obj_t aA, aB, aC, aD, bA, bB, bC, bD, cA, cB;
    obj_t cA_strassen, cB_strassen, cC_strassen, cD_strassen;
    //For reference implementation
    obj_t cA_conventional, cB_conventional, cC_conventional, cD_conventional;

    num_t dt;
    dim_t   r, n_repeats;
    trans_t  transa;
    trans_t  transb;

    int   len = 4;
    obj_t **a_list, **b_list, **c_conventional_list, **c_strassen_list;
    obj_t **alpha_list, **gamma_list, **delta_list, **beta_list;
    a_list = (obj_t **)malloc(len*sizeof(obj_t *));
    b_list = (obj_t **)malloc(len*sizeof(obj_t *));
    c_conventional_list = (obj_t **)malloc(len*sizeof(obj_t *));
    c_strassen_list = (obj_t **)malloc(len*sizeof(obj_t *));
    alpha_list = (obj_t **)malloc(len*sizeof(obj_t *));
    gamma_list = (obj_t **)malloc(len*sizeof(obj_t *));
    delta_list = (obj_t **)malloc(len*sizeof(obj_t *));
    beta_list = (obj_t **)malloc(len*sizeof(obj_t *));

    double dtime;
    double dtime_save;
    double dtime_conventional=1.0e9;
    double dtime_strassen=1.0e9;
    double gflops_conventional;
    double egflops_strassen;

    //printf("sizeof(dim_t):%d\n", sizeof(dim_t) );

    bli_init();

    bli_error_checking_level_set( BLIS_NO_ERROR_CHECKING );

    n_repeats = 3;

    dt = BLIS_DOUBLE;
    transa = BLIS_NO_TRANSPOSE;
    transb = BLIS_NO_TRANSPOSE;

    // Initialize the scalar: alpha1, alpha2, beta1, beta2, gamma, delta
    alpha1 = BLIS_ONE; alpha2 = BLIS_ONE;
    //gamma = BLIS_MINUS_ONE;  delta = BLIS_MINUS_ONE;
    gamma = BLIS_ONE;  delta = BLIS_ONE;
    //beta1 = BLIS_ZERO;  beta2 = BLIS_ZERO;
    beta1 = BLIS_ONE;  beta2 = BLIS_ONE;


    bli_obj_create( dt, m, k, 0, 0, &aA );
    bli_obj_create( dt, m, k, 0, 0, &aB );
    bli_obj_create( dt, m, k, 0, 0, &aC );
    bli_obj_create( dt, m, k, 0, 0, &aD );
    bli_obj_create( dt, k, n, 0, 0, &bA );
    bli_obj_create( dt, k, n, 0, 0, &bB );
    bli_obj_create( dt, k, n, 0, 0, &bC );
    bli_obj_create( dt, k, n, 0, 0, &bD );
    bli_obj_create( dt, m, n, 0, 0, &cA_conventional );
    bli_obj_create( dt, m, n, 0, 0, &cB_conventional );
    bli_obj_create( dt, m, n, 0, 0, &cC_conventional );
    bli_obj_create( dt, m, n, 0, 0, &cD_conventional );
    bli_obj_create( dt, m, n, 0, 0, &cA_strassen );
    bli_obj_create( dt, m, n, 0, 0, &cB_strassen );
    bli_obj_create( dt, m, n, 0, 0, &cC_strassen );
    bli_obj_create( dt, m, n, 0, 0, &cD_strassen );


    bli_obj_create( dt, m, n, 0, 0, &cA );
    bli_obj_create( dt, m, n, 0, 0, &cB );


    //bli_randm( &aA ); bli_randm( &aB );
    //bli_randm( &bA ); bli_randm( &bB );
    //bli_randm( &cA ); bli_randm( &cB );


    bli_setm( &BLIS_ONE, &aA );
    bli_setm( &BLIS_ONE, &aB );
    bli_setm( &BLIS_ONE, &bA );
    bli_setm( &BLIS_ONE, &bB );
    bli_setm( &BLIS_ONE, &cA );
    bli_setm( &BLIS_TWO, &cB );

    bli_obj_set_conjtrans( transb, bB );
    bli_obj_set_conjtrans( transb, bA );
    bli_obj_set_conjtrans( transa, aA );
    bli_obj_set_conjtrans( transa, aB );


    dtime_save = 1.0e9;

    for ( r = 0; r < n_repeats; ++r )
    {
        dtime_conventional = 1.0e9;
        dtime_strassen =1.0e9;

        //Zero out result matrices
        bli_setm( &BLIS_ONE, &cA_conventional );
        bli_setm( &BLIS_ONE, &cA_strassen );
        bli_setm( &BLIS_TWO, &cB_conventional );
        bli_setm( &BLIS_TWO, &cB_strassen );
        bli_setm( &BLIS_ONE, &cC_strassen );
        bli_setm( &BLIS_ONE, &cC_conventional );
        bli_setm( &BLIS_ONE, &cD_strassen );
        bli_setm( &BLIS_ONE, &cD_conventional );

        //bli_copym( &cA, &cA_conventional );
        //bli_copym( &cB, &cB_conventional );
        //bli_copym( &cA, &cA_strassen );
        //bli_copym( &cB, &cB_strassen );


        alpha_list[ 0 ] = &(BLIS_ONE);
        alpha_list[ 1 ] = &(BLIS_ONE);
        alpha_list[ 2 ] = &(BLIS_ONE);
        alpha_list[ 3 ] = &(BLIS_ONE);
        gamma_list[ 0 ] = &(BLIS_ONE);
        gamma_list[ 1 ] = &(BLIS_ONE);
        gamma_list[ 2 ] = &(BLIS_ONE);
        gamma_list[ 3 ] = &(BLIS_ONE);
        delta_list[ 0 ] = &(BLIS_ONE);
        delta_list[ 1 ] = &(BLIS_ONE);
        delta_list[ 2 ] = &(BLIS_ONE);
        delta_list[ 3 ] = &(BLIS_ONE);
        beta_list[ 0 ]  = &(BLIS_ONE);
        beta_list[ 1 ]  = &(BLIS_ONE);
        beta_list[ 2 ]  = &(BLIS_ONE);
        beta_list[ 3 ]  = &(BLIS_ONE);

        a_list[ 0 ] = &aA;
        a_list[ 1 ] = &aB;
        a_list[ 2 ] = &aC;
        a_list[ 3 ] = &aD;
        b_list[ 0 ] = &bA;
        b_list[ 1 ] = &bB;
        b_list[ 2 ] = &bC;
        b_list[ 3 ] = &bD;
        c_conventional_list[ 0 ] = &cA_conventional;
        c_conventional_list[ 1 ] = &cB_conventional;
        c_conventional_list[ 2 ] = &cC_conventional;
        c_conventional_list[ 3 ] = &cD_conventional;
        c_strassen_list[ 0 ] = &cA_strassen;
        c_strassen_list[ 1 ] = &cB_strassen;
        c_strassen_list[ 2 ] = &cC_strassen;
        c_strassen_list[ 3 ] = &cD_strassen;


        dtime = bli_clock();

        bli_mulstraprim_ref( len,
                alpha_list,
                a_list, gamma_list, 
                b_list, delta_list, 
                beta_list,
                c_conventional_list );

        dtime_conventional = bli_clock_min_diff( dtime_conventional, dtime );



        dtime = bli_clock();
        //cA = beta1*cA+alpha1*(aA+gamma*aB)(bA+delta*bB)
        //cB = beta2*cB+alpha2*(aA+gamma*aB)(bA+delta*bB) 
        bli_mulstraprim( len,
                alpha_list,
                a_list, gamma_list, 
                b_list, delta_list, 
                beta_list,
                c_strassen_list );

        dtime_strassen = bli_clock_min_diff( dtime_strassen, dtime );
        //blis_gemm( &alpha, &aA, &bA, &cA_strassen );

    } // end r < n_repeats


    //Print the result explicitly
    //bli_printm( "cA_strassen", &cA_strassen, "%4.1f", "" );
    //bli_printm( "cA_conventional", &cA_conventional, "%4.1f", "" );
    //bli_printm( "cB_strassen", &cB_strassen, "%4.1f", "" );
    //bli_printm( "cB_conventional", &cB_conventional, "%4.1f", "" );


    if ( m < 2000 ) {
        for ( dim_t i = 0; i < len; i ++ ) {
            computeError( m, m, m, n, bli_obj_buffer( *c_strassen_list[ i ] ), bli_obj_buffer( *c_conventional_list[ i ] ) );
        }
    }


    gflops_conventional = ( 2.0 * m * n * k ) / ( dtime_conventional * 1.0e9 );
    egflops_strassen = ( 2.0 * m * n * k ) / ( dtime_strassen * 1.0e9 );



    //gflops_conventional = ( 2.0 * m * k * n ) / ( dtime_conventional * 1.0e9 );
    //egflops_strassen = ( 2.0 * m * k * n ) / ( dtime_strassen * 1.0e9 );

    printf( "%6lu %6lu %6lu %10.3e %6.3f %10.3e %6.3f\n",
            ( uint64_t ) m,
            ( uint64_t ) k,
            ( uint64_t ) n,
            dtime_conventional, gflops_conventional,
            dtime_strassen, egflops_strassen );

    fflush(stdout);

    bli_obj_free( &aA ); bli_obj_free( &aB );
    bli_obj_free( &bA ); bli_obj_free( &bB );
    bli_obj_free( &cA_conventional );
    bli_obj_free( &cB_conventional );
    bli_obj_free( &cA_strassen );
    bli_obj_free( &cB_strassen );

    bli_finalize();

    return 0;
}

