/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2016, The University of Texas

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <unistd.h>
#include "blis.h"
#include "assert.h"

#include "blis_straprim_ref.h"

void one_step_strassen( obj_t* a, obj_t* b, obj_t* c )
{
    dim_t m = bli_obj_length( *c );
    dim_t n = bli_obj_width( *c );
    dim_t k = bli_obj_width( *a );

    //Pretty sure we can handle the following cases,
    //but for now, let's make sure we're working with square even dimensioned matrices.
    assert( m % 2 == 0);
    assert( n % 2 == 0);
    assert( k % 2 == 0);
    //assert( m == n);
    //assert( m == k);

    //Partition
    obj_t a00, a01;
    obj_t a10, a11;
    bli_acquire_mpart_half( BLIS_SUBPART00, a, &a00);
    bli_acquire_mpart_half( BLIS_SUBPART01, a, &a01);
    bli_acquire_mpart_half( BLIS_SUBPART10, a, &a10);
    bli_acquire_mpart_half( BLIS_SUBPART11, a, &a11);

    obj_t b00, b01;
    obj_t b10, b11;
    bli_acquire_mpart_half( BLIS_SUBPART00, b, &b00);
    bli_acquire_mpart_half( BLIS_SUBPART01, b, &b01);
    bli_acquire_mpart_half( BLIS_SUBPART10, b, &b10);
    bli_acquire_mpart_half( BLIS_SUBPART11, b, &b11);

    obj_t c00, c01;
    obj_t c10, c11;
    bli_acquire_mpart_half( BLIS_SUBPART00, c, &c00);
    bli_acquire_mpart_half( BLIS_SUBPART01, c, &c01);
    bli_acquire_mpart_half( BLIS_SUBPART10, c, &c10);
    bli_acquire_mpart_half( BLIS_SUBPART11, c, &c11);

    obj_t a_null, b_null, c_null, scalar_null;
    bli_obj_create_without_buffer( bli_obj_datatype( a00 ), bli_obj_length( a00 ), bli_obj_width( a00 ), &a_null );
    bli_obj_create_without_buffer( bli_obj_datatype( b00 ), bli_obj_length( b00 ), bli_obj_width( b00 ), &b_null );
    bli_obj_create_without_buffer( bli_obj_datatype( c00 ), bli_obj_length( c00 ), bli_obj_width( c00 ), &c_null );
    //bli_obj_create_without_buffer( bli_obj_datatype( c00 ), 1, 1, scalar_null );
    scalar_null = BLIS_ZERO;


    //Now do the 7 straprim thingers

    // M1
    // c00 = 0*c00+1*(a00+a11)(b00+b11)
    // c11 = 0*c11+1*(a00+a11)(b00+b11)
    bli_straprim( &BLIS_ONE, &BLIS_ONE,
                  &a00, &BLIS_ONE, &a11, 
                  &b00, &BLIS_ONE, &b11, 
                  &BLIS_ONE, &BLIS_ONE,
                  &c00, &c11 );
    //bli_straprim( &BLIS_ONE, &BLIS_ONE,
    //              &a00, &BLIS_ONE, &a11, 
    //              &b00, &BLIS_ONE, &b11, 
    //              &BLIS_ZERO, &BLIS_ZERO,
    //              &c00, &c11 );


    // M2
    // c10 = 0*c10+1*(a10+a11)b00
    // c11 = 1*c11-1*(a10+a11)b00
    //bli_straprim( &BLIS_ONE, &BLIS_MINUS_ONE,
    //              &a10, &BLIS_ONE, &a11, 
    //              &b00, &scalar_null, &b_null,
    //              &BLIS_ZERO, &BLIS_ONE,
    //              &c10, &c11 );
    bli_straprim( &BLIS_ONE, &BLIS_MINUS_ONE,
                  &a10, &BLIS_ONE, &a11, 
                  &b00, &scalar_null, &b_null,
                  &BLIS_ONE, &BLIS_ONE,
                  &c10, &c11 );


    // M3
    // c01 = 0*c01+1*a00(b01-b11)
    // c11 = 1*c11+1*a00(b01-b11)
    //bli_straprim( &BLIS_ONE, &BLIS_ONE,
    //              &a00, &scalar_null, &a_null, 
    //              &b01, &BLIS_MINUS_ONE, &b11,
    //              &BLIS_ZERO, &BLIS_ONE,
    //              &c01, &c11 );
    bli_straprim( &BLIS_ONE, &BLIS_ONE,
                  &a00, &scalar_null, &a_null, 
                  &b01, &BLIS_MINUS_ONE, &b11,
                  &BLIS_ONE, &BLIS_ONE,
                  &c01, &c11 );


    // M4
    // c00 = 1*c00+1*a11(b10-b00)
    // c10 = 1*c10+1*a11(b10-b00)
    bli_straprim( &BLIS_ONE, &BLIS_ONE,
                  &a11, &scalar_null, &a_null, 
                  &b10, &BLIS_MINUS_ONE, &b00,
                  &BLIS_ONE, &BLIS_ONE,
                  &c00, &c10 );



    // M5
    // c00 = 1*c00-1*(a00+a01)b11
    // c01 = 1*c01+1*(a00+a01)b11
    bli_straprim( &BLIS_MINUS_ONE, &BLIS_ONE,
                  &a00, &BLIS_ONE, &a01, 
                  &b11, &scalar_null, &b_null,
                  &BLIS_ONE, &BLIS_ONE,
                  &c00, &c01 );


    // M6
    // c11 = 1*c11+(a10-a00)(b00+b01)
    bli_straprim( &BLIS_ONE, &scalar_null,
                  &a10, &BLIS_MINUS_ONE, &a00, 
                  &b00, &BLIS_ONE, &b01,
                  &BLIS_ONE, &scalar_null,
                  &c11, &c_null );

   

    // M7
    // c00 = 1*c00+(a01-a11)(b10+b11)
    bli_straprim( &BLIS_ONE, &scalar_null,
                  &a01, &BLIS_MINUS_ONE, &a11, 
                  &b10, &BLIS_ONE, &b11,
                  &BLIS_ONE, &scalar_null,
                  &c00, &c_null );



    bli_obj_free( &a_null );
    bli_obj_free( &b_null );
    bli_obj_free( &c_null );
}



void bli_straprim_ref2( obj_t*  alpha1, obj_t*  alpha2,
                       obj_t*  aA, obj_t*  gamma, obj_t*  aB,
                       obj_t*  bA, obj_t*  delta, obj_t*  bB,
                       obj_t*  beta1, obj_t*  beta2,
                       obj_t*  cA, obj_t*  cB ) {

	num_t dt = bli_obj_datatype( *cA );
    dim_t m  = bli_obj_length( *cA );
    dim_t n  = bli_obj_width( *cA );
    dim_t k  = bli_obj_width( *aA );

    obj_t a, b, c_tmp;
    bli_obj_create( dt, m, k, 0, 0, &a );
    bli_obj_create( dt, k, n, 0, 0, &b );
    bli_obj_create( dt, m, n, 0, 0, &c_tmp );

    //dtime = bli_clock();
    //bli_straprim_ref( &alpha1, &alpha2,
    //                  &aA, &gamma, aB, 
    //                  &bA, &delta, &bB, 
    //                  &beta1, &beta2,
    //                  &cA_strassen, &cB_strassen );

    //cA = beta1*cA+alpha1*(aA+gamma*aB)(bA+delta*bB)
    //cB = beta2*cB+alpha2*(aA+gamma*aB)(bA+delta*bB) 
    //a:=aA
    //a:=a+gamma*aB
    //b:=bA
    //b:=b+delta*bB
    //c_tmp := a * b
    //cA:=beta1*cA
    //cB:=beta2*cB
    //cA:=cA+alpha1*c_tmp
    //cB:=cB+alpha1*c_tmp

    bli_copym( aA, &a );

    if ( bli_obj_buffer( *aB ) != NULL ) {
        bli_axpym ( gamma, aB, &a );
    }

    bli_copym( bA, &b );

    if ( bli_obj_buffer( *bB ) != NULL ) {
        bli_axpym ( delta, bB, &b );
    }

    bli_setm( &BLIS_ZERO, &c_tmp ); //We have to set c_tmp to zero before we call one_step_strassen
    //bli_gemm( &BLIS_ONE, &a, &b, &BLIS_ZERO, &c_tmp );
    one_step_strassen( &a, &b, &c_tmp );

    //bli_printm( "cA", &cA, "%4.1f", "" );
    //bli_printm( "beta1", &beta1, "%4.1f", "" );

    bli_scalm( beta1, cA );
    bli_axpym( alpha1, &c_tmp, cA );


    if ( bli_obj_buffer( *cB ) != NULL ) {
        bli_scalm( beta2, cB );
        bli_axpym( alpha2, &c_tmp, cB );
    }
    //dtime_conventional = bli_clock_min_diff( dtime_conventional, dtime );

    bli_obj_free( &a );
    bli_obj_free( &b );
    bli_obj_free( &c_tmp );
}



void two_step_strassen( obj_t* a, obj_t* b, obj_t* c )
{
    dim_t m = bli_obj_length( *c );
    dim_t n = bli_obj_width( *c );
    dim_t k = bli_obj_width( *a );

    //Pretty sure we can handle the following cases,
    //but for now, let's make sure we're working with square even dimensioned matrices.
    assert( m % 2 == 0);
    assert( n % 2 == 0);
    assert( k % 2 == 0);
    //assert( m == n);
    //assert( m == k);

    //Partition
    obj_t a00, a01;
    obj_t a10, a11;
    bli_acquire_mpart_half( BLIS_SUBPART00, a, &a00);
    bli_acquire_mpart_half( BLIS_SUBPART01, a, &a01);
    bli_acquire_mpart_half( BLIS_SUBPART10, a, &a10);
    bli_acquire_mpart_half( BLIS_SUBPART11, a, &a11);

    obj_t b00, b01;
    obj_t b10, b11;
    bli_acquire_mpart_half( BLIS_SUBPART00, b, &b00);
    bli_acquire_mpart_half( BLIS_SUBPART01, b, &b01);
    bli_acquire_mpart_half( BLIS_SUBPART10, b, &b10);
    bli_acquire_mpart_half( BLIS_SUBPART11, b, &b11);

    obj_t c00, c01;
    obj_t c10, c11;
    bli_acquire_mpart_half( BLIS_SUBPART00, c, &c00);
    bli_acquire_mpart_half( BLIS_SUBPART01, c, &c01);
    bli_acquire_mpart_half( BLIS_SUBPART10, c, &c10);
    bli_acquire_mpart_half( BLIS_SUBPART11, c, &c11);

    obj_t a_null, b_null, c_null, scalar_null;
    bli_obj_create_without_buffer( bli_obj_datatype( a00 ), bli_obj_length( a00 ), bli_obj_width( a00 ), &a_null );
    bli_obj_create_without_buffer( bli_obj_datatype( b00 ), bli_obj_length( b00 ), bli_obj_width( b00 ), &b_null );
    bli_obj_create_without_buffer( bli_obj_datatype( c00 ), bli_obj_length( c00 ), bli_obj_width( c00 ), &c_null );
    //bli_obj_create_without_buffer( bli_obj_datatype( c00 ), 1, 1, scalar_null );
    scalar_null = BLIS_ZERO;


    //Now do the 7 straprim thingers

    // M1
    // c00 = 0*c00+1*(a00+a11)(b00+b11)
    // c11 = 0*c11+1*(a00+a11)(b00+b11)
    bli_straprim_ref2( &BLIS_ONE, &BLIS_ONE,
                  &a00, &BLIS_ONE, &a11, 
                  &b00, &BLIS_ONE, &b11, 
                  &BLIS_ONE, &BLIS_ONE,
                  &c00, &c11 );
    //bli_straprim_ref( &BLIS_ONE, &BLIS_ONE,
    //              &a00, &BLIS_ONE, &a11, 
    //              &b00, &BLIS_ONE, &b11, 
    //              &BLIS_ZERO, &BLIS_ZERO,
    //              &c00, &c11 );



    // M2
    // c10 = 0*c10+1*(a10+a11)b00
    // c11 = 1*c11-1*(a10+a11)b00
    //bli_straprim_ref( &BLIS_ONE, &BLIS_MINUS_ONE,
    //              &a10, &BLIS_ONE, &a11, 
    //              &b00, &scalar_null, &b_null,
    //              &BLIS_ZERO, &BLIS_ONE,
    //              &c10, &c11 );
    bli_straprim_ref2( &BLIS_ONE, &BLIS_MINUS_ONE,
                  &a10, &BLIS_ONE, &a11, 
                  &b00, &scalar_null, &b_null,
                  &BLIS_ONE, &BLIS_ONE,
                  &c10, &c11 );




    // M3
    // c01 = 0*c01+1*a00(b01-b11)
    // c11 = 1*c11+1*a00(b01-b11)
    //bli_straprim_ref( &BLIS_ONE, &BLIS_ONE,
    //              &a00, &scalar_null, &a_null, 
    //              &b01, &BLIS_MINUS_ONE, &b11,
    //              &BLIS_ZERO, &BLIS_ONE,
    //              &c01, &c11 );
    bli_straprim_ref2( &BLIS_ONE, &BLIS_ONE,
                  &a00, &scalar_null, &a_null, 
                  &b01, &BLIS_MINUS_ONE, &b11,
                  &BLIS_ONE, &BLIS_ONE,
                  &c01, &c11 );


    // M4
    // c00 = 1*c00+1*a11(b10-b00)
    // c10 = 1*c10+1*a11(b10-b00)
    bli_straprim_ref2( &BLIS_ONE, &BLIS_ONE,
                  &a11, &scalar_null, &a_null, 
                  &b10, &BLIS_MINUS_ONE, &b00,
                  &BLIS_ONE, &BLIS_ONE,
                  &c00, &c10 );



    // M5
    // c00 = 1*c00-1*(a00+a01)b11
    // c01 = 1*c01+1*(a00+a01)b11
    bli_straprim_ref2( &BLIS_MINUS_ONE, &BLIS_ONE,
                  &a00, &BLIS_ONE, &a01, 
                  &b11, &scalar_null, &b_null,
                  &BLIS_ONE, &BLIS_ONE,
                  &c00, &c01 );


    // M6
    // c11 = 1*c11+(a10-a00)(b00+b01)
    bli_straprim_ref2( &BLIS_ONE, &scalar_null,
                  &a10, &BLIS_MINUS_ONE, &a00, 
                  &b00, &BLIS_ONE, &b01,
                  &BLIS_ONE, &scalar_null,
                  &c11, &c_null );                     //if (c_null->is_dummy==1) do nothing for cB


    // M7
    // c00 = 1*c00+(a01-a11)(b10+b11)
    bli_straprim_ref2( &BLIS_ONE, &scalar_null,
                  &a01, &BLIS_MINUS_ONE, &a11, 
                  &b10, &BLIS_ONE, &b11,
                  &BLIS_ONE, &scalar_null,
                  &c00, &c_null );


    bli_obj_free( &a_null );
    bli_obj_free( &b_null );
    bli_obj_free( &c_null );
}




int main( int argc, char** argv )
{
	obj_t a, b, c_conventional, c_strassen;
	dim_t m, k, n;

	dim_t p;
	dim_t p_begin, p_end, p_inc;
	dim_t   m_input;
	num_t dt;
	dim_t   r, n_repeats;
	trans_t  transa;
	trans_t  transb;

	double dtime;
	double dtime_conventional = 1.0e9;
	double dtime_strassen =1.0e9;
	double gflops_conventional;
	double egflops_strassen;

	bli_init();

	//bli_error_checking_level_set( BLIS_NO_ERROR_CHECKING );

	n_repeats = 3;

	//p_begin = 200;
	//p_end   = 4000;
	//p_inc   = 200;
	//m_input = -1;


	p_begin = 256;
	p_end   = 1024*32;
	p_inc   = 256;
	m_input = -1;

	//p_begin = 8;
	//p_end   = 8;
	//p_inc   = 8;
	//m_input = -1;


	dt = BLIS_DOUBLE;
	transa = BLIS_NO_TRANSPOSE;
	transb = BLIS_NO_TRANSPOSE;

    //m = 4000;
    //n = 4000;

    //m = 4096;
    //n = 4096;


	for ( p = p_begin; p <= p_end; p += p_inc )
	{
        dtime_conventional = 1.0e9;
        dtime_strassen =1.0e9;

		if ( m_input < 0 ) k = p * ( dim_t )abs(m_input);
		else               k =     ( dim_t )    m_input;

        //k = m * 2;
        //n = m * 4;
        ////k = m;
        ////n = m;
        m = k;
        n = k;

		bli_obj_create( dt, m, k, 0, 0, &a );
		bli_obj_create( dt, k, n, 0, 0, &b );
		bli_obj_create( dt, m, n, 0, 0, &c_conventional );
		bli_obj_create( dt, m, n, 0, 0, &c_strassen );

		bli_randm( &a ); bli_randm( &b );

        //bli_setm( &BLIS_ONE, &a );
        //bli_setm( &BLIS_ONE, &b );
        bli_setm( &BLIS_ZERO, &c_conventional );
        bli_setm( &BLIS_ZERO, &c_strassen );

        //int i, j;
        //double* buf1 = bli_obj_buffer( a );
        //for ( i = 0; i < 8; i++ ) {
        //    for ( j = 0; j < 8; j++ ) {
        //        buf1[ j * 8 + i ] = j * 8 + i;
        //    }
        //}


        //bli_printm( "a", &a, "%4.6lf", "" );
        //bli_printm( "b", &b, "%4.6lf", "" );

		bli_obj_set_conjtrans( transa, a );
		bli_obj_set_conjtrans( transb, b );

		for ( r = 0; r < n_repeats; ++r )
		{
            //Zero out result matrices
            bli_setm( &BLIS_ZERO, &c_conventional );
            bli_setm( &BLIS_ZERO, &c_strassen );

			dtime = bli_clock();
			bli_gemm( &BLIS_ONE, &a, &b, &BLIS_ONE, &c_conventional );
			dtime_conventional = bli_clock_min_diff( dtime_conventional, dtime );

			dtime = bli_clock();
            two_step_strassen( &a, &b, &c_strassen );
			dtime_strassen = bli_clock_min_diff( dtime_strassen, dtime );
		}

        //bli_printm( "c_strassen", &c_strassen, "%4.6lf", "" );
        //bli_printm( "c_conventional", &c_conventional, "%4.6lf", "" );

        //For last iteration, get residuals for both conventional and 1 step strassen gemm.
        obj_t x, y_conv, y_b, y_ab, y_stra;
		bli_obj_create( dt, n, 1, 0, 0, &x );
		bli_obj_create( dt, m, 1, 0, 0, &y_conv );
		bli_obj_create( dt, m, 1, 0, 0, &y_stra );
		bli_obj_create( dt, k, 1, 0, 0, &y_b );
		bli_obj_create( dt, m, 1, 0, 0, &y_ab );
        bli_randv( &x );
        bli_setm( &BLIS_ZERO, &y_conv );
        bli_setm( &BLIS_ZERO, &y_stra );
        bli_setm( &BLIS_ZERO, &y_ab );
        bli_gemv( &BLIS_ONE, &c_conventional, &x, &BLIS_ZERO, &y_conv );
        bli_gemv( &BLIS_ONE, &c_strassen, &x, &BLIS_ZERO, &y_stra );
        bli_gemv( &BLIS_ONE, &b, &x, &BLIS_ZERO, &y_b );
        bli_gemv( &BLIS_ONE, &a, &y_b, &BLIS_ZERO, &y_ab );

        //y_ab is A(Bx), and we'll use it as 'ground truth'
        //y_conv is C_conv * x
        //y_stra is C_stra * x

        //now the norms are the residuals:
        obj_t resid_conv, resid_stra;
		bli_obj_create( dt, 1, 1, 0, 0, &resid_conv );
		bli_obj_create( dt, 1, 1, 0, 0, &resid_stra );

        bli_subv( &y_ab, &y_conv );
        bli_normfv( &y_conv, &resid_conv ); 

        bli_subv( &y_ab, &y_stra );
        bli_normfv( &y_stra, &resid_stra ); 

        //Hack to get double versions of the resids out:
        double* buf = bli_obj_buffer( resid_conv );
        double resid_convd = *buf;
        buf = bli_obj_buffer( resid_stra );
        double resid_strad = *buf;

		gflops_conventional = ( 2.0 * m * n * k ) / ( dtime_conventional * 1.0e9 );
		egflops_strassen = ( 2.0 * m * n * k ) / ( dtime_strassen * 1.0e9 );

#ifdef BLIS
		printf( "data_gemm_blis" );
#else
		printf( "data_gemm_%s", BLAS );
#endif
		printf( "( %2lu, 1:7 ) = [ %4lu  %10.3e %6.3f %10.3e %10.3e %6.3f %10.3e ];\n",
		        ( uint64_t ) (p - p_begin + 1)/p_inc + 1,
		        ( uint64_t ) k,
		        dtime_conventional, gflops_conventional, resid_convd,
                dtime_strassen, egflops_strassen, resid_strad );

        fflush(stdout);

		bli_obj_free( &a );
		bli_obj_free( &b );
		bli_obj_free( &c_conventional );
		bli_obj_free( &c_strassen );
		bli_obj_free( &x );
		bli_obj_free( &y_conv );
		bli_obj_free( &y_b );
		bli_obj_free( &y_ab );
		bli_obj_free( &y_stra );
	}

	bli_finalize();

	return 0;
}

