/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2016, The University of Texas

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <unistd.h>
#include "blis.h"
#include "assert.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main( int argc, char** argv )
{
    dim_t m, k, n;
    if ( argc != 4 ) {
        printf( "Error: require 3 arguments, but only %d provided.\n", argc - 1 );
        exit( 0 );
    }

    sscanf( argv[ 1 ], "%lu", &m );
    sscanf( argv[ 2 ], "%lu", &k );
    sscanf( argv[ 3 ], "%lu", &n );

    obj_t a, b, c_conventional, c_strassen;

    num_t dt;
    dim_t   r, n_repeats;
    trans_t  transa;
    trans_t  transb;

    double dtime;
    double dtime_conventional = 1.0e9;
    double dtime_strassen =1.0e9;
    double gflops_conventional;
    double egflops_strassen;

    bli_init();

    bli_error_checking_level_set( BLIS_NO_ERROR_CHECKING );

    n_repeats = 3;

    dt = BLIS_DOUBLE;
    transa = BLIS_NO_TRANSPOSE;
    transb = BLIS_NO_TRANSPOSE;


    dtime_conventional = 1.0e9;
    dtime_strassen =1.0e9;


    bli_obj_create( dt, m, k, 0, 0, &a );
    bli_obj_create( dt, k, n, 0, 0, &b );
    bli_obj_create( dt, m, n, 0, 0, &c_conventional );
    bli_obj_create( dt, m, n, 0, 0, &c_strassen );

    bli_randm( &a ); bli_randm( &b );

    //bli_setm( &BLIS_ONE, &a );
    //bli_setm( &BLIS_ONE, &b );

    bli_setm( &BLIS_ZERO, &c_conventional );
    bli_setm( &BLIS_ZERO, &c_strassen );

    //int i, j;
    //double* buf1 = bli_obj_buffer( a );
    //for ( i = 0; i < 8; i++ ) {
    //    for ( j = 0; j < 8; j++ ) {
    //        buf1[ j * 8 + i ] = j * 8 + i;
    //    }
    //}
    //buf1[ 0 ] = 1;


    //bli_printm( "a", &a, "%4.6lf", "" );
    //bli_printm( "b", &b, "%4.6lf", "" );

    bli_obj_set_conjtrans( transa, a );
    bli_obj_set_conjtrans( transb, b );

    for ( r = 0; r < n_repeats; ++r )
    {
        //Zero out result matrices
        bli_setm( &BLIS_ZERO, &c_conventional );
        bli_setm( &BLIS_ZERO, &c_strassen );

        dtime = bli_clock();
        bli_gemm( &BLIS_ONE, &a, &b, &BLIS_ONE, &c_conventional );
        dtime_conventional = bli_clock_min_diff( dtime_conventional, dtime );

        dtime = bli_clock();
        bli_stra_2level( &a, &b, &c_strassen );
        dtime_strassen = bli_clock_min_diff( dtime_strassen, dtime );
    }

    //bli_printm( "c_strassen", &c_strassen, "%4.6lf", "" );
    //bli_printm( "c_conventional", &c_conventional, "%4.6lf", "" );

    //For last iteration, get residuals for both conventional and 1 step strassen gemm.
    obj_t x, y_conv, y_b, y_ab, y_stra;
    bli_obj_create( dt, n, 1, 0, 0, &x );
    bli_obj_create( dt, m, 1, 0, 0, &y_conv );
    bli_obj_create( dt, m, 1, 0, 0, &y_stra );
    bli_obj_create( dt, k, 1, 0, 0, &y_b );
    bli_obj_create( dt, m, 1, 0, 0, &y_ab );
    bli_randv( &x );
    bli_setm( &BLIS_ZERO, &y_conv );
    bli_setm( &BLIS_ZERO, &y_stra );
    bli_setm( &BLIS_ZERO, &y_ab );
    bli_gemv( &BLIS_ONE, &c_conventional, &x, &BLIS_ZERO, &y_conv );
    bli_gemv( &BLIS_ONE, &c_strassen, &x, &BLIS_ZERO, &y_stra );
    bli_gemv( &BLIS_ONE, &b, &x, &BLIS_ZERO, &y_b );
    bli_gemv( &BLIS_ONE, &a, &y_b, &BLIS_ZERO, &y_ab );

    //y_ab is A(Bx), and we'll use it as 'ground truth'
    //y_conv is C_conv * x
    //y_stra is C_stra * x

    //now the norms are the residuals:
    obj_t resid_conv, resid_stra;
    bli_obj_create( dt, 1, 1, 0, 0, &resid_conv );
    bli_obj_create( dt, 1, 1, 0, 0, &resid_stra );

    bli_subv( &y_ab, &y_conv );
    bli_normfv( &y_conv, &resid_conv ); 

    bli_subv( &y_ab, &y_stra );
    bli_normfv( &y_stra, &resid_stra ); 

    //Hack to get double versions of the resids out:
    double* buf = bli_obj_buffer( resid_conv );
    double resid_convd = *buf;
    buf = bli_obj_buffer( resid_stra );
    double resid_strad = *buf;

    gflops_conventional = ( 2.0 * m * n * k ) / ( dtime_conventional * 1.0e9 );
    egflops_strassen = ( 2.0 * m * n * k ) / ( dtime_strassen * 1.0e9 );

    printf( "%6lu %6lu %6lu %10.3e %6.3f %10.3e %10.3e %6.3f %10.3e\n",
            ( uint64_t ) m,
            ( uint64_t ) k,
            ( uint64_t ) n,
            dtime_conventional, gflops_conventional, resid_convd,
            dtime_strassen, egflops_strassen, resid_strad );

    fflush(stdout);

    bli_obj_free( &a );
    bli_obj_free( &b );
    bli_obj_free( &c_conventional );
    bli_obj_free( &c_strassen );
    bli_obj_free( &x );
    bli_obj_free( &y_conv );
    bli_obj_free( &y_b );
    bli_obj_free( &y_ab );
    bli_obj_free( &y_stra );

    bli_finalize();

    return 0;
}

