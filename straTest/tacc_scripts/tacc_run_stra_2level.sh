#!/bin/bash
#SBATCH -J stra_job
#SBATCH -o output_stra_2level_blis-%j.txt
#SBATCH -p normal
#SBATCH -t 04:00:00
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -A CompEdu
export OMP_NUM_THREADS=8
export BLIS_IC_NT=8
export BLIS_JR_NT=1
export KMP_AFFINITY=compact,verbose

#ibrun tacc_affinity ./test_stra_blis.x
#ibrun tacc_affinity ./test_stra_ref_blis.x
#ibrun tacc_affinity ./test_stra2_blis.x
ibrun tacc_affinity ./test_stra_2level_blis.x
#ibrun tacc_affinity ./test_stra_3level_blis.x


