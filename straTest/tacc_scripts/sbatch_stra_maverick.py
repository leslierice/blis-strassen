# This function prints a batch script to the command line suitable for sending to sbatch

import shutil

import os, sys, stat

# takes a dictionary of argument value pairs
def run_job(args):

    import datetime

    # number of compute cores per node
    try: 
        num_cores_per_node = args['num_cores_per_node']
    except:
        num_cores_per_node = 20
        #num_cores_per_node = 16

    # allowed time
    try: 
        walltime = args['walltime']
    except:
        walltime = '04:00:00'
        # walltime = '12:00:00'
        
    # the job queue
    try: 
        queue = args['queue']
    except:
        queue = 'vis'
        # queue = 'gpu'
        # queue = 'normal'


    # flag for email notifications when jobs complete
    #send_email = True
    send_email = False

    # email address for job completion notifications
    useremail='hjyahead@gmail.com'

   

    # first, get the number of processors, etc.
    # if these aren't specified, we default to one MPI rank on one node and
    # use the shared executable
    try: 
        nodes = args['nodes']
    except:
        nodes = 1
        
    try:
        procpernode = args['procpernode']
    except:
        procpernode = 1 
        
    do_nested = False
    #do_nested = True

    num_threads = num_cores_per_node / procpernode    


    # add a timestamp to the cout file
    timestamp = str(datetime.datetime.now())
    timestamp = timestamp.replace(' ', '.')
    timestamp = timestamp.replace(':', '.')


    # number of MPI processes to run
    npRequested = nodes * procpernode
    
    try:
        my_jobname=args['jobname']
    except:
        my_jobname='strassen'

    tacc_bash_filename   = "run_"  + my_jobname + ".sh"
    tacc_script_filename = "tacc_" + my_jobname + ".sh"

    # there is a limit on the size of filenames, which these might exceed
    my_outfilename = 'output_' + my_jobname + '-%j' + '-' + timestamp
    # replace '/' in the string
    my_outfilename = my_outfilename.replace('/', '.')

    my_test_routine = args['test_routine']
    my_test_label   = args['test_label']
    my_matrix_type  = args['matrix_type']
    my_core_num     = args['core_num']

    my_exe_dir      = args['my_exe_dir']
    cur_exe_dir     = args['cur_exe_dir']

    # put a copy of the executable in the run my_exe_dir to use with core files
    shutil.copy2(cur_exe_dir + my_test_routine, my_exe_dir)

    insert_step_string = "";

    if my_matrix_type == 'square':
        test_range_string  = 'k_start=256\n'
        test_range_string += 'k_end=20480\n'
        test_range_string += 'k_blocksize=256\n'

        my_m = "$k"
        my_k = "$k"
        my_n = "$k"
       
    elif my_matrix_type == 'rankk':
        test_range_string  = 'k_start=200\n'
        test_range_string += 'k_end=16000\n'
        test_range_string += 'k_blocksize=200\n'

        my_m = "16000"
        my_k = "$k"
        my_n = "16000"

        #insert_point_list = [4000, 8000, 12000]
        insert_point_list = [ 8000, 12000 ]

        for insert_point in insert_point_list:
            insert_step_string += \
"""
    if [ $k -eq {0} ]
    then
        k_blocksize=$(( k_blocksize * 2 ))
    fi\n
""".format(insert_point)
            

    else: # my_matrix_type == 'fixk': #k=2000, m,n=2000, 400, 20000 #k=1024, m,n=1024, 256, 20480
        test_range_string  = 'k_start=2000\n'
        test_range_string += 'k_end=20000\n'
        test_range_string += 'k_blocksize=400\n'

        my_m = "$k"
        my_k = "2000"
        my_n = "$k"

    test_exp_string   = \
"""
echo \"sb_{4}=[\" >> {6}.m
for (( k=k_start; k<=k_end; k+=k_blocksize ))
do
    {5}
    {0} {1} {2} {3} >> {6}.m
done
echo \"];\" >> {6}.m
""".format(my_exe_dir + my_test_routine, my_m, my_k, my_n, my_test_label, insert_step_string, my_jobname )

    if ( my_core_num == 20 ):
        jc_size = 2
        ic_size = my_core_num / 2
    else:
        jc_size = 1
        ic_size = my_core_num / 1

    bash_string   = '#!/bin/bash' + '\n'
    bash_string  += 'export KMP_AFFINITY=compact' + '\n'
    bash_string  += 'export OMP_NUM_THREADS=' + str(my_core_num) +'\n'
    bash_string  += 'export BLIS_JC_NT=' + str(jc_size) +'\n'
    bash_string  += 'export BLIS_IC_NT=' + str(ic_size) +'\n'
    bash_string  += 'export BLIS_JR_NT=' + str(1) +'\n\n'
    bash_string  += test_range_string + '\n' + test_exp_string +'\n'
    
    # now, create the bash script
    script_string = '#!/bin/bash' + '\n'
    script_string = script_string + '#SBATCH -J ' + my_jobname + '\n'
    script_string = script_string + '#SBATCH -o ' + my_outfilename + '\n'
    script_string = script_string + '#SBATCH -p ' + queue + '\n'
    script_string = script_string + '#SBATCH -t ' + walltime + '\n'
    script_string = script_string + '#SBATCH -n ' + str(npRequested) + '\n'
    script_string = script_string + '#SBATCH -N ' + str(nodes) + '\n'
    script_string = script_string + '#SBATCH -A ' + 'CompEdu' + '\n'
    
    if send_email:
        script_string = script_string + '#SBATCH --mail-user=' + useremail + '\n'
        script_string = script_string + '#SBATCH --mail-type=end' + '\n'

    #script_string = script_string + thread_string + '\n'
    #script_string = script_string + 'ulimit -Hc unlimited' + '\n'
    #script_string = script_string + 'ulimit -Sc unlimited' + '\n'
    #script_string = script_string + 'export MV2_DEBUG_CORESIZE=unlimited' + '\n'
    #script_string = script_string + 'export MV2_DEBUG_SHOW_BACKTRACE=1' + '\n'
    # need this for maverick
    #script_string = script_string + 'export KMP_AFFINITY=scatter' + '\n'
    #script_string = script_string + 'export KMP_AFFINITY=compact,verbose' + '\n'
    #script_string = script_string + 'export KMP_AFFINITY=compact' + '\n'
    #script_string = script_string + 'export OMP_NESTED=true' + '\n'
    #script_string = script_string + nested_string
    #script_string = script_string + 'ibrun tacc_affinity ' + my_exe_dir + my_test_routine + '\n'
    script_string = script_string + 'ibrun tacc_affinity ' + my_exe_dir + tacc_bash_filename + '\n'
    #script_string = script_string + 'ibrun tacc_affinity ' + tacc_bash_filename + '\n'
    #script_string = script_string + 'remora ibrun tacc_affinity ' + exe_dir + executable + arg_string + '\n'
    #script_string = script_string + 'ibrun tacc_affinity ' + './' + executable + arg_string + '\n'

    #print bash_string
    #print script_string
    #print tacc_bash_filename
    #print tacc_script_filename

    tacc_bash_file   = open(tacc_bash_filename, 'w')
    tacc_bash_file.write(bash_string)
    tacc_bash_file.close()

    #os.chmod(tacc_bash_filename, stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH )
    os.chmod(tacc_bash_filename, 0755)

    tacc_script_file = open(tacc_script_filename, 'w')
    tacc_script_file.write(script_string)
    tacc_script_file.close()

    return tacc_script_filename

