#!/bin/bash
#export KMP_AFFINITY=compact,verbose
export KMP_AFFINITY=compact
export OMP_NUM_THREADS=1
export BLIS_IC_NT=1
export BLIS_JR_NT=1

k_start=16
k_end=8192
k_blocksize=256

echo "sb_stra=["
for (( k=k_start; k<=k_end; k+=k_blocksize ))
do
    #./test_stra_1level_blis.x     $k 256 $k
    ./test_stra_1level_blis.x     $k $k $k
done
echo "];"
