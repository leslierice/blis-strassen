#!/bin/bash


export KMP_AFFINITY=compact
export OMP_NUM_THREADS=1
export BLIS_IC_NT=1
export BLIS_JR_NT=1

#for size in $(seq 32 32 512)
echo "mkl_gemm=["
for size in $(seq 200 200 2000)
do
    #./test_dgemm_mkl.x $size $size $size
    ./test_dgemm_mkl.x $size 100 $size
done
echo "];"
