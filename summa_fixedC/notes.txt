1. module swap mvapich2 impi
2. make clean
3. make
   it will produce mypdgemm.exe
5. Change last line in submit.sh to full path 
   to your directory and executable
   /home1/02935/msmelyan/summa_fixedC/mypdgemm.exe 6000 2 2 0 0
   parameters are global matrix  dimension (6000 in this case)
   and nrows x ncols processor grid (2 x 2 in this case)
4. Inside submit.sh change -n and -N to the product
   of nrows x ncols (4 in above case)
   Each node is 16 core SNB
4. To submit:
   sbatch submit.sh

