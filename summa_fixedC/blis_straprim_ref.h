/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2016, The University of Texas

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef BLIS_STRAPRIM_REF_H
#define BLIS_STRAPRIM_REF_H


// Allow C++ users to include this header file in their source code. However,
// we make the extern "C" conditional on whether we're using a C++ compiler,
// since regular C compilers don't understand the extern "C" construct.
#ifdef __cplusplus
extern "C" {
#endif


#include "blis.h"


#define TOLERANCE 1E-12
void computeError(
        int    ldc,
        int    ldc_ref,
        int    m,
        int    n,
        double *C,
        double *C_ref
        )
{
    int    i, j;
    //printf( "ldc:%d,ldc_ref:%d,m:%d,n:%d\n", ldc, ldc_ref, m, n );
    for ( i = 0; i < m; i ++ ) {
        for ( j = 0; j < n; j ++ ) {
            if ( fabs( C[ j * ldc + i ] - C_ref[ j * ldc_ref + i ] ) > TOLERANCE ) {
                printf( "C[ %d ][ %d ] != C_gold, %E, %E\n", i, j, C[ j * ldc + i ], C_ref[ j * ldc_ref + i ] );
                //return;
                break;
            }
        }
    }
}


void bli_straprim_ref( obj_t*  alpha1, obj_t*  alpha2,
                       obj_t*  aA, obj_t*  gamma, obj_t*  aB,
                       obj_t*  bA, obj_t*  delta, obj_t*  bB,
                       obj_t*  beta1, obj_t*  beta2,
                       obj_t*  cA, obj_t*  cB ) {

	//num_t dt = bli_obj_datatype( *cA );
	num_t dt = (num_t)bli_obj_datatype( *cA );
    dim_t m  = bli_obj_length( *cA );
    dim_t n  = bli_obj_width( *cA );
    dim_t k  = bli_obj_width( *aA );

    obj_t a, b, c_tmp;
    bli_obj_create( dt, m, k, 0, 0, &a );
    bli_obj_create( dt, k, n, 0, 0, &b );
    bli_obj_create( dt, m, n, 0, 0, &c_tmp );

    //dtime = bli_clock();
    //bli_straprim_ref( &alpha1, &alpha2,
    //                  &aA, &gamma, aB, 
    //                  &bA, &delta, &bB, 
    //                  &beta1, &beta2,
    //                  &cA_strassen, &cB_strassen );

    //cA = beta1*cA+alpha1*(aA+gamma*aB)(bA+delta*bB)
    //cB = beta2*cB+alpha2*(aA+gamma*aB)(bA+delta*bB) 
    //a:=aA
    //a:=a+gamma*aB
    //b:=bA
    //b:=b+delta*bB
    //c_tmp := a * b
    //cA:=beta1*cA
    //cB:=beta2*cB
    //cA:=cA+alpha1*c_tmp
    //cB:=cB+alpha1*c_tmp

    bli_copym( aA, &a );

    if ( bli_obj_buffer( *aB ) != NULL ) {
        bli_axpym ( gamma, aB, &a );
    }

    bli_copym( bA, &b );

    if ( bli_obj_buffer( *bB ) != NULL ) {
        bli_axpym ( delta, bB, &b );
    }

    bli_gemm( &BLIS_ONE, &a, &b, &BLIS_ZERO, &c_tmp );

    //bli_printm( "cA", &cA, "%4.1f", "" );
    //bli_printm( "beta1", &beta1, "%4.1f", "" );

    bli_scalm( beta1, cA );
    bli_axpym( alpha1, &c_tmp, cA );


    if ( bli_obj_buffer( *cB ) != NULL ) {
        bli_scalm( beta2, cB );
        bli_axpym( alpha2, &c_tmp, cB );
    }
    //dtime_conventional = bli_clock_min_diff( dtime_conventional, dtime );

    bli_obj_free( &a );
    bli_obj_free( &b );
    bli_obj_free( &c_tmp );
}




// End extern "C" construct block.
#ifdef __cplusplus
}
#endif

#endif

