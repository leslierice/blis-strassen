#!/bin/bash
#SBATCH -J  base          # job name
#SBATCH -o base.o%j       # output and error file name (%j expands to jobID)
#SBATCH -N 4 -n 4
#SBATCH -p vis       # queue (partition) -- normal, development, etc.
#SBATCH -t 01:00:00         # run time (hh:mm:ss) - 1.5 hours
#SBATCH -A CompEdu

export OMP_NUM_THREADS=20
export BLIS_JC_NT=2
export BLIS_IC_NT=10
export KMP_AFFINITY=granularity=fine,compact,1,0

#ibrun ./mypdgemm.exe 16384 4 4 0 0 # run the MPI executable named a.out
#ibrun tacc_affinity /home/03223/jianyu/Work/Github/blis-strassen/summa_fixedC/mypdgemm.exe 16384 2 2 0 0
#ibrun tacc_affinity /home/03223/jianyu/Work/Github/blis-strassen/summa_fixedC/mypdgemm.exe 2048 2 2 0 0
ibrun tacc_affinity /home/03223/jianyu/Work/Github/blis-strassen/summa_fixedC/mypdgemm.exe 16384 2 2 0 0

