
output_1_stra;
output_2_stra_ref;
output_3_stra2;
output_4_stra_2level;
output_5_stra_3level;

output_6_mulstra_2level;

output_7_mul2stra_1level;

output_8_mul8stra_3level;

output_9_mul4stra_2level;

% ---------------------------------------------------------
% Plotting
% ---------------------------------------------------------
figure;
% hFig = figure(1);
%set(hFig, 'Position', [0 0 160 240])

set( gcf, 'PaperSize', [3 3]);
set( gcf, 'PaperPosition', [0.25 0.25 3 3] );
%set( gcf, 'Position', [0 0 600 400]);
set( gcf, 'Position', [0 0 600*2 400*2]);


hold;

plot( data_gemm_blis5( :, 1 ), data_gemm_blis5( :, 3), '.-', 'LineWidth', 2, 'Color',  'c');

plot( data_gemm_blis1( :, 1 ), data_gemm_blis1( :, 6), '.-', 'LineWidth', 2, 'Color',  [0 0.2 1.0] );
plot( data_gemm_blis2( :, 1 ), data_gemm_blis2( :, 6), '.-', 'LineWidth', 2, 'Color', [1 0 0.2] );
plot( data_gemm_blis3( :, 1 ), data_gemm_blis3( :, 6), '.-', 'LineWidth', 2, 'Color',  'm' );

plot( data_gemm_blis4( :, 1 ), data_gemm_blis4( :, 6), '.-', 'LineWidth', 2, 'Color',  'g' );

plot( data_gemm_blis5( :, 1 ), data_gemm_blis5( :, 6), '.-', 'LineWidth', 2, 'Color',  'k' );

plot( data_gemm_blis6( :, 1 ), data_gemm_blis6( :, 6), '.-', 'LineWidth', 2, 'Color',  [1,0.4,0.6] );

plot( data_gemm_blis7( :, 1 ), data_gemm_blis7( :, 6), '--', 'LineWidth', 2, 'Color',  'k' );

plot( data_gemm_blis8( :, 1 ), data_gemm_blis8( :, 6), '--', 'LineWidth', 2, 'Color',  'c' );

plot( data_gemm_blis9( :, 1 ), data_gemm_blis9( :, 6), '--', 'LineWidth', 2, 'Color',  'r' );


xlabel( 'm=k=n' );
ylabel( 'Practical GFLOPS (2n^3/time)' );
title( 'Strassen(m=k=n)' );

grid on;
axis square;
axis( [ 0 12000 0 30 ] );
%axis( [ 0 5000 0 248 ] );

ax = gca;
ax.YTick = [  0, 5, 10, 15, 20, 23.76, 25, 30 ];
%ax.YTick = [  0, 50, 100, 150, 200, 248];

%ax.XTick = [ 0, 200, 400, 600, 800, 1000];
%ax.XTick = [ 0, 1000, 2000, 3000, 4000 ];
ax.XTick = [ 0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000];
ax.XAxis.Exponent = 3;


set( gca,'FontSize',16 );

legend( 'originl BLIS dgemm', ...
        'one-level Strassen(primitive approach)', ...
        'one-level Strassen(reference approach, gemm+axpy)', ...
        'two-level Strassen(1st level with reference approach, 2nd level with primitive approach)', ...
        'two-level Strassen(both levels with reference approach)', ...
        'three-level Strassen(all 3 levels with reference approach)', ...
        'two-level Strassen(primitive approach)', ...
        'Location','SouthEast');
