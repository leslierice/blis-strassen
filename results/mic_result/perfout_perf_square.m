%Single GFLOP/sec =16 (SP SIMD Lane) x 2 (FMA) x 1.1 (GHZ) x 60 (# cores) = 2112 for single precision arithmetic
%Double GFLOP/sec = 8 (DP SIMD Lane) x 2 (FMA) x 1.1 (GHZ) x 60 (# cores) = 1056 for double precision arithmetic
clear;

perfout_1_change_m;
perfout_2_fixk;
perfout_3_square;
perfout_4_rankk;


mklout_2_fixk;
mklout_3_square;
mklout_4_rankk;
mklout_all;

% ---------------------------------------------------------
% Plotting
% ---------------------------------------------------------
figure;
% hFig = figure(1);
%set(hFig, 'Position', [0 0 160 240])

set( gcf, 'PaperSize', [3 3]);
set( gcf, 'PaperPosition', [0.25 0.25 3 3] );
%set( gcf, 'Position', [0 0 600 400]);
set( gcf, 'Position', [0 0 600*2 400*2]);


hold;

%plot( mic_stra1( :, 1 ), mic_stra1( :, 5), '.-', 'LineWidth', 1.3, 'Color',  'b');
%plot( mic_stra1( :, 1 ), mic_stra1( :, 8), '.-', 'LineWidth', 1.3, 'Color', 'r' );

%plot( mic_stra2( :, 1 ), mic_stra2( :, 5), '.-', 'LineWidth', 1.3, 'Color',  'b');
%plot( mic_stra2( :, 1 ), mic_stra2( :, 8), '.-', 'LineWidth', 1.3, 'Color', 'r' );

plot( mic_stra3( :, 1 ), mic_stra3( :, 5), '.-', 'LineWidth', 1.3, 'Color',  'b');
plot( mic_stra3( :, 1 ), mic_stra3( :, 8), '.-', 'LineWidth', 1.3, 'Color', 'r' );

plot( mic_gemm3( :, 1 ), mic_gemm3( :, 5), '--', 'LineWidth', 1.3, 'Color',  'k');
plot( mic_gemm_square( :, 1 ), mic_gemm_square( :, 5), '--', 'LineWidth', 1.3, 'Color',  'm');

%plot( mic_stra4( :, 2 ), mic_stra4( :, 5), '.-', 'LineWidth', 1.3, 'Color',  'b');
%plot( mic_stra4( :, 2 ), mic_stra4( :, 8), '.-', 'LineWidth', 1.3, 'Color', 'r' );

xlabel( 'm=n=k' );
ylabel( 'Effective GFLOPS (2n^3/time)' );
title( 'm=n=k vary' );

grid on;
axis square;
%axis( [ 0 8000 0 30 ] );
%axis( [ 0 5000 0 248 ] );
axis( [ 0 15000 0 1056 ] );

ax = gca;
ax.YTick = [  0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1056 ];
%ax.YTick = [  0, 50, 100, 150, 200, 248];

%ax.XTick = [ 0, 200, 400, 600, 800, 1000];
%ax.XTick = [ 0, 1000, 2000, 3000, 4000 ];
ax.XTick = [ 0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000, 14000, 15000];
ax.XAxis.Exponent = 3;


set( gca,'FontSize',16 );

legend( 'BLIS DGEMM', ...
        'BLIS Strassen (one-level)', ...
        'MKL DGEMM (244 threads)', ...
        'MKL DGEMM (240 threads)', ...
        'Location','SouthEast');
