data_gemm_blis1(  1, 1:6 ) = [    8    8    8  5.100e-05  0.020  0.000e+00 ];
data_gemm_blis1(  2, 1:6 ) = [   16   16   16  7.000e-05  0.117  0.000e+00 ];
data_gemm_blis1(  3, 1:6 ) = [   24   24   24  9.700e-05  0.285  6.962e-14 ];
data_gemm_blis1(  4, 1:6 ) = [   32   32   32  1.330e-04  0.493  0.000e+00 ];
data_gemm_blis1(  5, 1:6 ) = [   40   40   40  1.680e-04  0.762  1.258e-12 ];
data_gemm_blis1(  6, 1:6 ) = [   48   48   48  2.140e-04  1.034  0.000e+00 ];
data_gemm_blis1(  7, 1:6 ) = [   56   56   56  2.600e-04  1.351  4.254e-12 ];
data_gemm_blis1(  8, 1:6 ) = [   64   64   64  3.650e-04  1.436  3.411e-13 ];
data_gemm_blis1(  9, 1:6 ) = [   72   72   72  4.230e-04  1.765  2.412e-12 ];
data_gemm_blis1( 10, 1:6 ) = [   80   80   80  5.110e-04  2.004  4.576e-12 ];
data_gemm_blis1( 11, 1:6 ) = [   88   88   88  6.060e-04  2.249  3.199e-12 ];
data_gemm_blis1( 12, 1:6 ) = [   96   96   96  8.030e-04  2.204  3.342e-12 ];
data_gemm_blis1( 13, 1:6 ) = [  104  104  104  9.560e-04  2.353  2.319e-12 ];
data_gemm_blis1( 14, 1:6 ) = [  112  112  112  1.127e-03  2.493  8.422e-12 ];
data_gemm_blis1( 15, 1:6 ) = [  120  120  120  1.121e-03  3.083  1.635e-12 ];
data_gemm_blis1( 16, 1:6 ) = [  128  128  128  1.683e-03  2.492  2.572e-12 ];
data_gemm_blis1( 17, 1:6 ) = [  136  136  136  1.855e-03  2.712  6.629e-13 ];
data_gemm_blis1( 18, 1:6 ) = [  144  144  144  2.125e-03  2.810  6.276e-11 ];
data_gemm_blis1( 19, 1:6 ) = [  152  152  152  2.455e-03  2.861  3.154e-12 ];
data_gemm_blis1( 20, 1:6 ) = [  160  160  160  2.756e-03  2.972  1.582e-11 ];
data_gemm_blis1( 21, 1:6 ) = [  168  168  168  3.056e-03  3.103  1.068e-11 ];
data_gemm_blis1( 22, 1:6 ) = [  176  176  176  3.359e-03  3.246  3.167e-11 ];
data_gemm_blis1( 23, 1:6 ) = [  184  184  184  3.747e-03  3.325  1.542e-11 ];
data_gemm_blis1( 24, 1:6 ) = [  192  192  192  4.117e-03  3.438  4.726e-12 ];
data_gemm_blis1( 25, 1:6 ) = [  200  200  200  4.510e-03  3.548  8.039e-11 ];
data_gemm_blis1( 26, 1:6 ) = [  208  208  208  4.940e-03  3.643  4.919e-11 ];
data_gemm_blis1( 27, 1:6 ) = [  216  216  216  5.391e-03  3.739  1.003e-10 ];
data_gemm_blis1( 28, 1:6 ) = [  224  224  224  5.844e-03  3.846  7.487e-11 ];
data_gemm_blis1( 29, 1:6 ) = [  232  232  232  6.398e-03  3.903  8.312e-11 ];
data_gemm_blis1( 30, 1:6 ) = [  240  240  240  6.252e-03  4.422  9.158e-11 ];
data_gemm_blis1( 31, 1:6 ) = [  248  248  248  7.589e-03  4.020  3.581e-12 ];
data_gemm_blis1( 32, 1:6 ) = [  256  256  256  9.104e-03  3.686  0.000e+00 ];
data_gemm_blis1( 33, 1:6 ) = [  264  264  264  8.779e-03  4.192  5.911e-11 ];
data_gemm_blis1( 34, 1:6 ) = [  272  272  272  9.610e-03  4.188  1.800e-10 ];
data_gemm_blis1( 35, 1:6 ) = [  280  280  280  1.020e-02  4.304  3.044e-10 ];
data_gemm_blis1( 36, 1:6 ) = [  288  288  288  1.076e-02  4.440  3.396e-10 ];
data_gemm_blis1( 37, 1:6 ) = [  296  296  296  1.143e-02  4.537  8.606e-11 ];
data_gemm_blis1( 38, 1:6 ) = [  304  304  304  1.311e-02  4.284  2.478e-12 ];
data_gemm_blis1( 39, 1:6 ) = [  312  312  312  1.389e-02  4.372  1.767e-10 ];
data_gemm_blis1( 40, 1:6 ) = [  320  320  320  1.467e-02  4.467  4.555e-10 ];
data_gemm_blis1( 41, 1:6 ) = [  328  328  328  1.589e-02  4.443  1.194e-10 ];
data_gemm_blis1( 42, 1:6 ) = [  336  336  336  1.663e-02  4.562  9.378e-12 ];
data_gemm_blis1( 43, 1:6 ) = [  344  344  344  1.771e-02  4.596  2.109e-11 ];
data_gemm_blis1( 44, 1:6 ) = [  352  352  352  1.854e-02  4.704  4.863e-10 ];
data_gemm_blis1( 45, 1:6 ) = [  360  360  360  1.810e-02  5.154  3.020e-11 ];
data_gemm_blis1( 46, 1:6 ) = [  368  368  368  2.085e-02  4.781  3.577e-10 ];
data_gemm_blis1( 47, 1:6 ) = [  376  376  376  2.203e-02  4.826  3.527e-10 ];
data_gemm_blis1( 48, 1:6 ) = [  384  384  384  2.403e-02  4.713  2.673e-10 ];
data_gemm_blis1( 49, 1:6 ) = [  392  392  392  2.468e-02  4.882  1.621e-09 ];
data_gemm_blis1( 50, 1:6 ) = [  400  400  400  2.571e-02  4.979  3.092e-10 ];
data_gemm_blis1( 51, 1:6 ) = [  408  408  408  2.673e-02  5.082  3.674e-11 ];
data_gemm_blis1( 52, 1:6 ) = [  416  416  416  2.829e-02  5.090  8.116e-12 ];
data_gemm_blis1( 53, 1:6 ) = [  424  424  424  2.979e-02  5.117  1.311e-10 ];
data_gemm_blis1( 54, 1:6 ) = [  432  432  432  3.117e-02  5.174  7.183e-10 ];
data_gemm_blis1( 55, 1:6 ) = [  440  440  440  3.248e-02  5.246  4.960e-10 ];
data_gemm_blis1( 56, 1:6 ) = [  448  448  448  3.406e-02  5.280  1.059e-10 ];
data_gemm_blis1( 57, 1:6 ) = [  456  456  456  3.618e-02  5.242  1.476e-09 ];
data_gemm_blis1( 58, 1:6 ) = [  464  464  464  3.740e-02  5.343  2.429e-09 ];
data_gemm_blis1( 59, 1:6 ) = [  472  472  472  3.926e-02  5.357  1.363e-09 ];
data_gemm_blis1( 60, 1:6 ) = [  480  480  480  3.863e-02  5.726  9.963e-12 ];
data_gemm_blis1( 61, 1:6 ) = [  488  488  488  4.336e-02  5.361  7.032e-10 ];
data_gemm_blis1( 62, 1:6 ) = [  496  496  496  4.488e-02  5.438  2.350e-09 ];
data_gemm_blis1( 63, 1:6 ) = [  504  504  504  4.694e-02  5.455  2.246e-10 ];
data_gemm_blis1( 64, 1:6 ) = [  512  512  512  5.475e-02  4.903  2.058e-09 ];
data_gemm_blis1( 65, 1:6 ) = [  520  520  520  5.085e-02  5.530  7.259e-11 ];
data_gemm_blis1( 66, 1:6 ) = [  528  528  528  5.271e-02  5.585  2.090e-11 ];
data_gemm_blis1( 67, 1:6 ) = [  536  536  536  5.495e-02  5.605  3.453e-09 ];
data_gemm_blis1( 68, 1:6 ) = [  544  544  544  5.861e-02  5.494  1.273e-10 ];
data_gemm_blis1( 69, 1:6 ) = [  552  552  552  6.076e-02  5.537  6.410e-10 ];
data_gemm_blis1( 70, 1:6 ) = [  560  560  560  6.351e-02  5.531  7.748e-10 ];
data_gemm_blis1( 71, 1:6 ) = [  568  568  568  6.602e-02  5.552  5.202e-10 ];
data_gemm_blis1( 72, 1:6 ) = [  576  576  576  6.823e-02  5.602  3.274e-11 ];
data_gemm_blis1( 73, 1:6 ) = [  584  584  584  7.067e-02  5.637  8.132e-10 ];
data_gemm_blis1( 74, 1:6 ) = [  592  592  592  7.377e-02  5.625  1.328e-10 ];
data_gemm_blis1( 75, 1:6 ) = [  600  600  600  7.240e-02  5.967  1.203e-09 ];
data_gemm_blis1( 76, 1:6 ) = [  608  608  608  7.935e-02  5.665  2.243e-09 ];
data_gemm_blis1( 77, 1:6 ) = [  616  616  616  8.180e-02  5.715  3.431e-09 ];
data_gemm_blis1( 78, 1:6 ) = [  624  624  624  8.509e-02  5.711  1.420e-11 ];
data_gemm_blis1( 79, 1:6 ) = [  632  632  632  8.838e-02  5.713  4.481e-09 ];
data_gemm_blis1( 80, 1:6 ) = [  640  640  640  9.273e-02  5.654  2.439e-09 ];
data_gemm_blis1( 81, 1:6 ) = [  648  648  648  9.372e-02  5.806  3.612e-09 ];
data_gemm_blis1( 82, 1:6 ) = [  656  656  656  9.709e-02  5.815  2.493e-09 ];
data_gemm_blis1( 83, 1:6 ) = [  664  664  664  1.004e-01  5.830  1.500e-09 ];
data_gemm_blis1( 84, 1:6 ) = [  672  672  672  1.030e-01  5.894  1.933e-09 ];
data_gemm_blis1( 85, 1:6 ) = [  680  680  680  1.069e-01  5.881  8.728e-09 ];
data_gemm_blis1( 86, 1:6 ) = [  688  688  688  1.103e-01  5.903  6.203e-10 ];
data_gemm_blis1( 87, 1:6 ) = [  696  696  696  1.142e-01  5.904  6.526e-09 ];
data_gemm_blis1( 88, 1:6 ) = [  704  704  704  1.170e-01  5.963  6.129e-09 ];
data_gemm_blis1( 89, 1:6 ) = [  712  712  712  1.212e-01  5.958  3.883e-10 ];
data_gemm_blis1( 90, 1:6 ) = [  720  720  720  1.198e-01  6.232  3.270e-09 ];
data_gemm_blis1( 91, 1:6 ) = [  728  728  728  1.299e-01  5.942  2.110e-09 ];
data_gemm_blis1( 92, 1:6 ) = [  736  736  736  1.331e-01  5.989  7.353e-09 ];
data_gemm_blis1( 93, 1:6 ) = [  744  744  744  1.372e-01  6.003  7.145e-09 ];
data_gemm_blis1( 94, 1:6 ) = [  752  752  752  1.422e-01  5.981  5.487e-09 ];
data_gemm_blis1( 95, 1:6 ) = [  760  760  760  1.455e-01  6.035  2.683e-09 ];
data_gemm_blis1( 96, 1:6 ) = [  768  768  768  1.607e-01  5.637  5.104e-10 ];
data_gemm_blis1( 97, 1:6 ) = [  776  776  776  1.538e-01  6.077  2.629e-10 ];
data_gemm_blis1( 98, 1:6 ) = [  784  784  784  1.609e-01  5.988  2.343e-09 ];
data_gemm_blis1( 99, 1:6 ) = [  792  792  792  1.645e-01  6.040  1.766e-09 ];
data_gemm_blis1( 100, 1:6 ) = [  800  800  800  1.698e-01  6.029  1.801e-09 ];
data_gemm_blis1( 101, 1:6 ) = [  808  808  808  1.745e-01  6.047  6.851e-10 ];
data_gemm_blis1( 102, 1:6 ) = [  816  816  816  1.792e-01  6.063  1.461e-09 ];
data_gemm_blis1( 103, 1:6 ) = [  824  824  824  1.842e-01  6.074  3.498e-09 ];
data_gemm_blis1( 104, 1:6 ) = [  832  832  832  1.893e-01  6.085  5.614e-09 ];
data_gemm_blis1( 105, 1:6 ) = [  840  840  840  1.866e-01  6.353  9.700e-09 ];
data_gemm_blis1( 106, 1:6 ) = [  848  848  848  2.005e-01  6.081  1.218e-09 ];
data_gemm_blis1( 107, 1:6 ) = [  856  856  856  2.053e-01  6.109  1.597e-09 ];
data_gemm_blis1( 108, 1:6 ) = [  864  864  864  2.110e-01  6.115  1.043e-09 ];
data_gemm_blis1( 109, 1:6 ) = [  872  872  872  2.162e-01  6.134  2.659e-09 ];
data_gemm_blis1( 110, 1:6 ) = [  880  880  880  2.209e-01  6.169  5.720e-09 ];
data_gemm_blis1( 111, 1:6 ) = [  888  888  888  2.264e-01  6.185  4.878e-09 ];
data_gemm_blis1( 112, 1:6 ) = [  896  896  896  2.373e-01  6.063  6.855e-10 ];
data_gemm_blis1( 113, 1:6 ) = [  904  904  904  2.384e-01  6.198  2.625e-09 ];
data_gemm_blis1( 114, 1:6 ) = [  912  912  912  2.446e-01  6.203  8.789e-10 ];
data_gemm_blis1( 115, 1:6 ) = [  920  920  920  2.503e-01  6.223  3.000e-10 ];
data_gemm_blis1( 116, 1:6 ) = [  928  928  928  2.554e-01  6.259  2.106e-09 ];
data_gemm_blis1( 117, 1:6 ) = [  936  936  936  2.627e-01  6.244  8.348e-10 ];
data_gemm_blis1( 118, 1:6 ) = [  944  944  944  2.678e-01  6.283  1.520e-08 ];
data_gemm_blis1( 119, 1:6 ) = [  952  952  952  2.750e-01  6.274  1.277e-09 ];
data_gemm_blis1( 120, 1:6 ) = [  960  960  960  2.721e-01  6.503  6.763e-10 ];
data_gemm_blis1( 121, 1:6 ) = [  968  968  968  2.903e-01  6.248  4.867e-09 ];
data_gemm_blis1( 122, 1:6 ) = [  976  976  976  2.957e-01  6.288  2.287e-09 ];
data_gemm_blis1( 123, 1:6 ) = [  984  984  984  3.035e-01  6.279  5.706e-09 ];
data_gemm_blis1( 124, 1:6 ) = [  992  992  992  3.100e-01  6.297  3.094e-09 ];
data_gemm_blis1( 125, 1:6 ) = [ 1000 1000 1000  3.163e-01  6.323  2.209e-08 ];
data_gemm_blis1( 126, 1:6 ) = [ 1008 1008 1008  3.232e-01  6.338  3.927e-09 ];
data_gemm_blis1( 127, 1:6 ) = [ 1016 1016 1016  3.304e-01  6.349  4.464e-09 ];
data_gemm_blis1( 128, 1:6 ) = [ 1024 1024 1024  3.748e-01  5.730  1.653e-08 ];
data_gemm_blis1( 129, 1:6 ) = [ 1032 1032 1032  3.483e-01  6.311  9.350e-09 ];
data_gemm_blis1( 130, 1:6 ) = [ 1040 1040 1040  3.567e-01  6.308  5.279e-10 ];
data_gemm_blis1( 131, 1:6 ) = [ 1048 1048 1048  3.638e-01  6.327  1.672e-08 ];
data_gemm_blis1( 132, 1:6 ) = [ 1056 1056 1056  3.717e-01  6.336  3.813e-09 ];
data_gemm_blis1( 133, 1:6 ) = [ 1064 1064 1064  3.794e-01  6.350  7.832e-09 ];
data_gemm_blis1( 134, 1:6 ) = [ 1072 1072 1072  3.886e-01  6.340  9.231e-10 ];
data_gemm_blis1( 135, 1:6 ) = [ 1080 1080 1080  3.840e-01  6.562  2.284e-08 ];
data_gemm_blis1( 136, 1:6 ) = [ 1088 1088 1088  4.053e-01  6.356  2.910e-09 ];
data_gemm_blis1( 137, 1:6 ) = [ 1096 1096 1096  4.138e-01  6.364  1.204e-10 ];
data_gemm_blis1( 138, 1:6 ) = [ 1104 1104 1104  4.230e-01  6.362  3.052e-09 ];
data_gemm_blis1( 139, 1:6 ) = [ 1112 1112 1112  4.311e-01  6.380  1.213e-09 ];
data_gemm_blis1( 140, 1:6 ) = [ 1120 1120 1120  4.379e-01  6.417  7.305e-09 ];
data_gemm_blis1( 141, 1:6 ) = [ 1128 1128 1128  4.452e-01  6.447  3.226e-10 ];
data_gemm_blis1( 142, 1:6 ) = [ 1136 1136 1136  4.557e-01  6.433  1.312e-08 ];
data_gemm_blis1( 143, 1:6 ) = [ 1144 1144 1144  4.666e-01  6.418  8.736e-09 ];
data_gemm_blis1( 144, 1:6 ) = [ 1152 1152 1152  4.842e-01  6.315  1.482e-08 ];
data_gemm_blis1( 145, 1:6 ) = [ 1160 1160 1160  4.837e-01  6.454  4.259e-10 ];
data_gemm_blis1( 146, 1:6 ) = [ 1168 1168 1168  4.910e-01  6.490  5.222e-09 ];
data_gemm_blis1( 147, 1:6 ) = [ 1176 1176 1176  5.054e-01  6.436  5.739e-09 ];
data_gemm_blis1( 148, 1:6 ) = [ 1184 1184 1184  5.119e-01  6.485  7.886e-09 ];
data_gemm_blis1( 149, 1:6 ) = [ 1192 1192 1192  5.231e-01  6.476  1.507e-09 ];
data_gemm_blis1( 150, 1:6 ) = [ 1200 1200 1200  5.187e-01  6.662  4.600e-09 ];
data_gemm_blis1( 151, 1:6 ) = [ 1208 1208 1208  5.456e-01  6.462  2.782e-09 ];
data_gemm_blis1( 152, 1:6 ) = [ 1216 1216 1216  5.547e-01  6.483  2.119e-08 ];
data_gemm_blis1( 153, 1:6 ) = [ 1224 1224 1224  5.654e-01  6.487  4.251e-08 ];
data_gemm_blis1( 154, 1:6 ) = [ 1232 1232 1232  5.768e-01  6.484  1.941e-08 ];
data_gemm_blis1( 155, 1:6 ) = [ 1240 1240 1240  5.850e-01  6.518  7.814e-09 ];
data_gemm_blis1( 156, 1:6 ) = [ 1248 1248 1248  5.950e-01  6.533  2.956e-09 ];
data_gemm_blis1( 157, 1:6 ) = [ 1256 1256 1256  6.059e-01  6.540  3.304e-10 ];
data_gemm_blis1( 158, 1:6 ) = [ 1264 1264 1264  6.235e-01  6.478  2.354e-08 ];
data_gemm_blis1( 159, 1:6 ) = [ 1272 1272 1272  6.322e-01  6.511  3.633e-09 ];
data_gemm_blis1( 160, 1:6 ) = [ 1280 1280 1280  6.878e-01  6.098  5.987e-09 ];
data_gemm_blis1( 161, 1:6 ) = [ 1288 1288 1288  6.556e-01  6.518  6.789e-09 ];
data_gemm_blis1( 162, 1:6 ) = [ 1296 1296 1296  6.693e-01  6.505  6.941e-09 ];
data_gemm_blis1( 163, 1:6 ) = [ 1304 1304 1304  6.784e-01  6.537  6.527e-10 ];
data_gemm_blis1( 164, 1:6 ) = [ 1312 1312 1312  6.921e-01  6.527  5.007e-09 ];
data_gemm_blis1( 165, 1:6 ) = [ 1320 1320 1320  6.854e-01  6.711  1.031e-08 ];
data_gemm_blis1( 166, 1:6 ) = [ 1328 1328 1328  7.181e-01  6.523  2.651e-09 ];
data_gemm_blis1( 167, 1:6 ) = [ 1336 1336 1336  7.291e-01  6.542  1.928e-09 ];
data_gemm_blis1( 168, 1:6 ) = [ 1344 1344 1344  7.425e-01  6.539  9.069e-09 ];
data_gemm_blis1( 169, 1:6 ) = [ 1352 1352 1352  7.545e-01  6.551  3.879e-09 ];
data_gemm_blis1( 170, 1:6 ) = [ 1360 1360 1360  7.635e-01  6.590  2.938e-08 ];
data_gemm_blis1( 171, 1:6 ) = [ 1368 1368 1368  7.802e-01  6.563  2.893e-08 ];
data_gemm_blis1( 172, 1:6 ) = [ 1376 1376 1376  7.909e-01  6.588  1.430e-08 ];
data_gemm_blis1( 173, 1:6 ) = [ 1384 1384 1384  8.060e-01  6.578  1.394e-08 ];
data_gemm_blis1( 174, 1:6 ) = [ 1392 1392 1392  8.165e-01  6.607  5.022e-09 ];
data_gemm_blis1( 175, 1:6 ) = [ 1400 1400 1400  8.318e-01  6.598  8.780e-09 ];
data_gemm_blis1( 176, 1:6 ) = [ 1408 1408 1408  8.618e-01  6.478  9.829e-09 ];
data_gemm_blis1( 177, 1:6 ) = [ 1416 1416 1416  8.603e-01  6.600  8.214e-09 ];
data_gemm_blis1( 178, 1:6 ) = [ 1424 1424 1424  8.707e-01  6.633  3.048e-08 ];
data_gemm_blis1( 179, 1:6 ) = [ 1432 1432 1432  8.868e-01  6.623  5.920e-09 ];
data_gemm_blis1( 180, 1:6 ) = [ 1440 1440 1440  8.797e-01  6.789  2.154e-08 ];
data_gemm_blis1( 181, 1:6 ) = [ 1448 1448 1448  9.196e-01  6.603  3.945e-09 ];
data_gemm_blis1( 182, 1:6 ) = [ 1456 1456 1456  9.320e-01  6.624  1.839e-09 ];
data_gemm_blis1( 183, 1:6 ) = [ 1464 1464 1464  9.478e-01  6.621  1.343e-08 ];
data_gemm_blis1( 184, 1:6 ) = [ 1472 1472 1472  9.624e-01  6.628  4.829e-08 ];
data_gemm_blis1( 185, 1:6 ) = [ 1480 1480 1480  9.743e-01  6.655  6.438e-09 ];
data_gemm_blis1( 186, 1:6 ) = [ 1488 1488 1488  9.870e-01  6.676  5.859e-09 ];
data_gemm_blis1( 187, 1:6 ) = [ 1496 1496 1496  1.006e+00  6.653  9.850e-10 ];
data_gemm_blis1( 188, 1:6 ) = [ 1504 1504 1504  1.026e+00  6.630  7.054e-09 ];
data_gemm_blis1( 189, 1:6 ) = [ 1512 1512 1512  1.040e+00  6.645  6.224e-09 ];
data_gemm_blis1( 190, 1:6 ) = [ 1520 1520 1520  1.058e+00  6.638  5.248e-08 ];
data_gemm_blis1( 191, 1:6 ) = [ 1528 1528 1528  1.074e+00  6.642  7.110e-11 ];
data_gemm_blis1( 192, 1:6 ) = [ 1536 1536 1536  1.173e+00  6.179  6.416e-10 ];
data_gemm_blis1( 193, 1:6 ) = [ 1544 1544 1544  1.105e+00  6.660  8.791e-09 ];
data_gemm_blis1( 194, 1:6 ) = [ 1552 1552 1552  1.124e+00  6.653  5.847e-08 ];
data_gemm_blis1( 195, 1:6 ) = [ 1560 1560 1560  1.114e+00  6.815  2.748e-09 ];
data_gemm_blis1( 196, 1:6 ) = [ 1568 1568 1568  1.159e+00  6.655  3.760e-08 ];
data_gemm_blis1( 197, 1:6 ) = [ 1576 1576 1576  1.175e+00  6.666  5.777e-10 ];
data_gemm_blis1( 198, 1:6 ) = [ 1584 1584 1584  1.192e+00  6.668  3.602e-09 ];
data_gemm_blis1( 199, 1:6 ) = [ 1592 1592 1592  1.209e+00  6.675  7.185e-09 ];
data_gemm_blis1( 200, 1:6 ) = [ 1600 1600 1600  1.223e+00  6.700  3.463e-08 ];
data_gemm_blis1( 201, 1:6 ) = [ 1608 1608 1608  1.237e+00  6.722  5.689e-09 ];
data_gemm_blis1( 202, 1:6 ) = [ 1616 1616 1616  1.259e+00  6.704  2.779e-08 ];
data_gemm_blis1( 203, 1:6 ) = [ 1624 1624 1624  1.278e+00  6.704  5.351e-09 ];
data_gemm_blis1( 204, 1:6 ) = [ 1632 1632 1632  1.290e+00  6.737  1.146e-08 ];
data_gemm_blis1( 205, 1:6 ) = [ 1640 1640 1640  1.328e+00  6.643  2.976e-08 ];
data_gemm_blis1( 206, 1:6 ) = [ 1648 1648 1648  1.332e+00  6.721  6.439e-08 ];
data_gemm_blis1( 207, 1:6 ) = [ 1656 1656 1656  1.352e+00  6.717  1.273e-07 ];
data_gemm_blis1( 208, 1:6 ) = [ 1664 1664 1664  1.397e+00  6.596  3.428e-08 ];
data_gemm_blis1( 209, 1:6 ) = [ 1672 1672 1672  1.390e+00  6.724  6.248e-09 ];
data_gemm_blis1( 210, 1:6 ) = [ 1680 1680 1680  1.380e+00  6.874  8.947e-09 ];
data_gemm_blis1( 211, 1:6 ) = [ 1688 1688 1688  1.433e+00  6.712  2.220e-08 ];
data_gemm_blis1( 212, 1:6 ) = [ 1696 1696 1696  1.449e+00  6.734  8.128e-09 ];
data_gemm_blis1( 213, 1:6 ) = [ 1704 1704 1704  1.471e+00  6.729  5.219e-09 ];
data_gemm_blis1( 214, 1:6 ) = [ 1712 1712 1712  1.491e+00  6.732  7.406e-08 ];
data_gemm_blis1( 215, 1:6 ) = [ 1720 1720 1720  1.506e+00  6.757  1.207e-08 ];
data_gemm_blis1( 216, 1:6 ) = [ 1728 1728 1728  1.524e+00  6.771  3.539e-08 ];
data_gemm_blis1( 217, 1:6 ) = [ 1736 1736 1736  1.547e+00  6.763  3.941e-09 ];
data_gemm_blis1( 218, 1:6 ) = [ 1744 1744 1744  1.577e+00  6.725  3.722e-08 ];
data_gemm_blis1( 219, 1:6 ) = [ 1752 1752 1752  1.592e+00  6.755  1.873e-08 ];
data_gemm_blis1( 220, 1:6 ) = [ 1760 1760 1760  1.617e+00  6.742  3.999e-08 ];
data_gemm_blis1( 221, 1:6 ) = [ 1768 1768 1768  1.639e+00  6.744  8.383e-08 ];
data_gemm_blis1( 222, 1:6 ) = [ 1776 1776 1776  1.663e+00  6.737  1.089e-08 ];
data_gemm_blis1( 223, 1:6 ) = [ 1784 1784 1784  1.681e+00  6.756  8.574e-08 ];
data_gemm_blis1( 224, 1:6 ) = [ 1792 1792 1792  1.812e+00  6.351  7.762e-08 ];
data_gemm_blis1( 225, 1:6 ) = [ 1800 1800 1800  1.694e+00  6.886  8.396e-08 ];
data_gemm_blis1( 226, 1:6 ) = [ 1808 1808 1808  1.752e+00  6.745  7.796e-08 ];
data_gemm_blis1( 227, 1:6 ) = [ 1816 1816 1816  1.771e+00  6.765  5.504e-09 ];
data_gemm_blis1( 228, 1:6 ) = [ 1824 1824 1824  1.796e+00  6.756  8.017e-08 ];
data_gemm_blis1( 229, 1:6 ) = [ 1832 1832 1832  1.817e+00  6.767  1.962e-08 ];
data_gemm_blis1( 230, 1:6 ) = [ 1840 1840 1840  1.836e+00  6.784  1.326e-08 ];
data_gemm_blis1( 231, 1:6 ) = [ 1848 1848 1848  1.862e+00  6.779  4.379e-09 ];
data_gemm_blis1( 232, 1:6 ) = [ 1856 1856 1856  1.882e+00  6.793  2.100e-08 ];
data_gemm_blis1( 233, 1:6 ) = [ 1864 1864 1864  1.909e+00  6.784  0.000e+00 ];
data_gemm_blis1( 234, 1:6 ) = [ 1872 1872 1872  1.926e+00  6.812  3.148e-10 ];
data_gemm_blis1( 235, 1:6 ) = [ 1880 1880 1880  1.953e+00  6.806  1.704e-07 ];
data_gemm_blis1( 236, 1:6 ) = [ 1888 1888 1888  1.971e+00  6.828  1.043e-08 ];
data_gemm_blis1( 237, 1:6 ) = [ 1896 1896 1896  2.006e+00  6.796  3.548e-08 ];
data_gemm_blis1( 238, 1:6 ) = [ 1904 1904 1904  2.025e+00  6.818  7.175e-08 ];
data_gemm_blis1( 239, 1:6 ) = [ 1912 1912 1912  2.054e+00  6.807  3.213e-08 ];
data_gemm_blis1( 240, 1:6 ) = [ 1920 1920 1920  2.084e+00  6.792  1.339e-07 ];
data_gemm_blis1( 241, 1:6 ) = [ 1928 1928 1928  2.114e+00  6.782  1.190e-08 ];
data_gemm_blis1( 242, 1:6 ) = [ 1936 1936 1936  2.132e+00  6.806  1.153e-08 ];
data_gemm_blis1( 243, 1:6 ) = [ 1944 1944 1944  2.160e+00  6.803  6.673e-08 ];
data_gemm_blis1( 244, 1:6 ) = [ 1952 1952 1952  2.183e+00  6.815  3.600e-08 ];
data_gemm_blis1( 245, 1:6 ) = [ 1960 1960 1960  2.206e+00  6.827  3.994e-08 ];
data_gemm_blis1( 246, 1:6 ) = [ 1968 1968 1968  2.233e+00  6.826  2.009e-08 ];
data_gemm_blis1( 247, 1:6 ) = [ 1976 1976 1976  2.258e+00  6.833  3.703e-08 ];
data_gemm_blis1( 248, 1:6 ) = [ 1984 1984 1984  2.291e+00  6.816  5.509e-09 ];
data_gemm_blis1( 249, 1:6 ) = [ 1992 1992 1992  2.315e+00  6.829  2.243e-09 ];
data_gemm_blis1( 250, 1:6 ) = [ 2000 2000 2000  2.347e+00  6.818  2.164e-08 ];
data_gemm_blis1( 251, 1:6 ) = [ 2008 2008 2008  2.372e+00  6.826  2.706e-08 ];
data_gemm_blis1( 252, 1:6 ) = [ 2016 2016 2016  2.402e+00  6.821  2.235e-07 ];
data_gemm_blis1( 253, 1:6 ) = [ 2024 2024 2024  2.427e+00  6.832  2.239e-07 ];
data_gemm_blis1( 254, 1:6 ) = [ 2032 2032 2032  2.455e+00  6.836  4.067e-08 ];
data_gemm_blis1( 255, 1:6 ) = [ 2048 2048 2048  3.666e+00  4.686  1.696e-08 ];
