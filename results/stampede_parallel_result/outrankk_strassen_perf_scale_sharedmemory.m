
outrankk_1_stra;

outrankk_9_mulstra_2level_2core;
outrankk_10_mulstra_2level_4core;
outrankk_11_mulstra_2level_8core;
outrankk_12_mulstra_2level_1core;

outrankk_13_stra_1level_1core;

outrankk_14_stra_1level_2core;

outrankk_15_stra_1level_4core;

%output_7_mul2stra_1level;

% ---------------------------------------------------------
% Plotting
% ---------------------------------------------------------
figure;
% hFig = figure(1);
%set(hFig, 'Position', [0 0 160 240])

set( gcf, 'PaperSize', [3 3]);
set( gcf, 'PaperPosition', [0.25 0.25 3 3] );
%set( gcf, 'Position', [0 0 600 400]);
set( gcf, 'Position', [0 0 600*2 400*2]);


hold;

plot( data_gemm_blis12( :, 1 ), data_gemm_blis12( :, 3), '--', 'LineWidth', 2, 'Color', 'g' );
plot( data_gemm_blis12( :, 1 ), data_gemm_blis12( :, 6), '.-', 'LineWidth', 2, 'Color', 'g');
plot( data_gemm_blis13( :, 1 ), data_gemm_blis13( :, 6), '-<', 'LineWidth', 2, 'Color', 'g');


plot( data_gemm_blis9( :, 1 ), data_gemm_blis9( :, 3), '--', 'LineWidth', 2, 'Color',  'c');
plot( data_gemm_blis9( :, 1 ), data_gemm_blis9( :, 6), '.-', 'LineWidth', 2, 'Color',  'c');
plot( data_gemm_blis14( :, 1 ), data_gemm_blis14( :, 6), '-<', 'LineWidth', 2, 'Color',  'c');

plot( data_gemm_blis10( :, 1 ), data_gemm_blis10( :, 3), '--', 'LineWidth', 2, 'Color',  'm');
plot( data_gemm_blis10( :, 1 ), data_gemm_blis10( :, 6), '.-', 'LineWidth', 2, 'Color',  'm');
plot( data_gemm_blis15( :, 1 ), data_gemm_blis15( :, 6), '-<', 'LineWidth', 2, 'Color',  'm');

plot( data_gemm_blis11( :, 1 ), data_gemm_blis11( :, 3), '--', 'LineWidth', 2, 'Color', [0 0.2 1.0] );
plot( data_gemm_blis11( :, 1 ), data_gemm_blis11( :, 6), '.-', 'LineWidth', 2, 'Color', [0 0.2 1.0] );
plot( data_gemm_blis1( :, 1 ), data_gemm_blis1( :, 6), '-<', 'LineWidth', 2, 'Color', [0 0.2 1.0]);




% plot( data_gemm_blis8( :, 1 ), data_gemm_blis1( :, 6), '.-', 'LineWidth', 2, 'Color',  [0 0.2 1.0] );
% plot( data_gemm_blis2( :, 1 ), data_gemm_blis2( :, 6), '.-', 'LineWidth', 2, 'Color', [1 0 0.2] );
% plot( data_gemm_blis3( :, 1 ), data_gemm_blis3( :, 6), '.-', 'LineWidth', 2, 'Color',  'm' );
% plot( data_gemm_blis4( :, 1 ), data_gemm_blis4( :, 6), '.-', 'LineWidth', 2, 'Color',  'g' );
% plot( data_gemm_blis5( :, 1 ), data_gemm_blis5( :, 6), '.-', 'LineWidth', 2, 'Color',  'k' );
% plot( data_gemm_blis6( :, 1 ), data_gemm_blis6( :, 6), '.-', 'LineWidth', 2, 'Color',  [1,0.4,0.6] );

%plot( data_gemm_blis7( :, 1 ), data_gemm_blis7( :, 6), '--', 'LineWidth', 2, 'Color',  'k' );


xlabel( 'm=k=n' );
ylabel( 'Practical GFLOPS (2n^3/time)' );
title( 'Strassen(m=k=n)' );

grid on;
axis square;



grid on;
axis square;
axis( [ 0 8000 0 200 ] );
%axis( [ 0 5000 0 248 ] );

ax = gca;
ax.YTick = [  0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 198.4, 200];
%ax.YTick = [  0, 50, 100, 150, 200, 248];

%ax.XTick = [ 0, 200, 400, 600, 800, 1000];
%ax.XTick = [ 0, 1000, 2000, 3000, 4000 ];
ax.XTick = [ 0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000];
ax.XAxis.Exponent = 3;



set( gca,'FontSize',16 );

legend( 'BLIS DGEMM (1 core)', ...
        'BLIS STRASSEN 2level (1 core)', ...
        'BLIS STRASSEN 1level (1 core)', ...
        'BLIS DGEMM (2 core)', ...
        'BLIS STRASSEN 2level (2 core)', ...
        'BLIS STRASSEN 1level (2 core)', ...
        'BLIS DGEMM (4 core)', ...
        'BLIS STRASSEN 2level (4 core)', ...
        'BLIS STRASSEN 1level (4 core)', ...
        'BLIS DGEMM (8 core)', ...
        'BLIS STRASSEN 2level (8 core)', ...
        'BLIS STRASSEN 1level (8 core)', ...
        'Location','SouthEast');
