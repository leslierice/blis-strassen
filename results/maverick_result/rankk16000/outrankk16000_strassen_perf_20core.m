clear;

outrankk16000_4_1level_20core;
outrankk16000_8_2level_20core;

outrankk16000_12_hybrid_20core;

outrankk16000_16_1levelref_20core;
outrankk16000_20_2levelref_20core;

outrankk16000_24_mkl_20core;

core_num=20;

% ---------------------------------------------------------
% Plotting
% ---------------------------------------------------------
figure;
% hFig = figure(1);
%set(hFig, 'Position', [0 0 160 240])

set( gcf, 'PaperSize', [3 3]);
set( gcf, 'PaperPosition', [0.25 0.25 3 3] );
set( gcf, 'Position', [0 0 600 400]);
%set( gcf, 'Position', [0 0 600*2 400*2]);


hold;

plot( sb_stra_1level( :, 2 ), sb_stra_1level( :, 5), '--', 'LineWidth', 1.3, 'Color',  'k');
plot( sb_mkl_gemm( :, 2 ), sb_mkl_gemm( :, 5), '.-', 'LineWidth', 1.3, 'Color',  'k');

plot( sb_stra_1level( :, 2 ), sb_stra_1level( :, 8), '.-', 'LineWidth', 1.3, 'Color',  [0 0.2 1.0] );
plot( sb_stra_2level( :, 2 ), sb_stra_2level( :, 8), '.-', 'LineWidth', 1.3, 'Color', [1 0 0.2] );

plot( sb_stra2_2level( :, 2 ), sb_stra2_2level( :, 8), '.-', 'LineWidth', 1.3, 'Color', 'm' );

plot( sb_stra_1levelref_par( :, 2 ), sb_stra_1levelref_par( :, 8), '--', 'LineWidth', 1.3, 'Color',  [0 0.2 1.0] );
plot( sb_stra_2levelref_par( :, 2 ), sb_stra_2levelref_par( :, 8), '--', 'LineWidth', 1.3, 'Color', [1 0 0.2] );

xlabel( 'k' );
ylabel( 'Effective GFLOPS (2n^3/time)' );
title( 'm=n=16000, k varies, 20-core' );

grid on;
axis square;
axis( [ 0 16000 0 24.8*core_num ] );
%axis( [ 0 5000 0 248 ] );

ax = gca;
ax.YTick = core_num .* [  0, 5, 10, 15, 20, 24.8, 30, 35];
%ax.YTick = [  0, 50, 100, 150, 200, 248];

%ax.XTick = [ 0, 200, 400, 600, 800, 1000];
%ax.XTick = [ 0, 1000, 2000, 3000, 4000 ];
ax.XTick = [ 0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000, 14000, 15000, 16000 ];
ax.XAxis.Exponent = 3;


set( gca,'FontSize', 12 );

legend( 'BLIS DGEMM', ...
        'MKL DGEMM', ...
        'One-level Strassen', ...
        'Two-level Strassen', ...
        'Two-level Hybrid Strassen', ...
        'One-level Strassen Naive', ...
        'Two-level Strassen Naive', ...
        'Location','SouthEast');
