#!/bin/bash
export KMP_AFFINITY=compact
export OMP_NUM_THREADS=10
export BLIS_JC_NT=1
export BLIS_IC_NT=10
export BLIS_JR_NT=1

k_start=400
k_end=20000
k_blocksize=400


echo "sb_stra_2level=[" >> test_stra_2level_fixk_10core.m
for (( k=k_start; k<=k_end; k+=k_blocksize ))
do
    
    /home/03223/jianyu/Work/Github/blis-strassen/straTest/my_result/test_stra_2level_blis.x $k 1024 $k >> test_stra_2level_fixk_10core.m
done
echo "];" >> test_stra_2level_fixk_10core.m

