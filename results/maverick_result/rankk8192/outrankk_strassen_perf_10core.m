clear;
outrankk_3_1level_10core;
outrankk_7_2level_10core;

outrankk_11_hybrid_10core;

outrankk_15_1levelref_10core;
outrankk_19_2levelref_10core;


core_num=10;

% ---------------------------------------------------------
% Plotting
% ---------------------------------------------------------
figure;
% hFig = figure(1);
%set(hFig, 'Position', [0 0 160 240])

set( gcf, 'PaperSize', [3 3]);
set( gcf, 'PaperPosition', [0.25 0.25 3 3] );
set( gcf, 'Position', [0 0 600 400]);
%set( gcf, 'Position', [0 0 600*2 400*2]);


hold;

plot( sb_stra_1level( :, 2 ), sb_stra_1level( :, 5), '--', 'LineWidth', 1.3, 'Color',  'k');

plot( sb_stra_1level( :, 2 ), sb_stra_1level( :, 8), '.-', 'LineWidth', 1.3, 'Color',  [0 0.2 1.0] );
plot( sb_stra_2level( :, 2 ), sb_stra_2level( :, 8), '.-', 'LineWidth', 1.3, 'Color', [1 0 0.2] );

plot( sb_stra2_2level( :, 2 ), sb_stra2_2level( :, 8), '.-', 'LineWidth', 1.3, 'Color', 'm' );

plot( sb_stra_1levelref( :, 2 ), sb_stra_1levelref( :, 8), '--', 'LineWidth', 1.3, 'Color',  [0 0.2 1.0] );
plot( sb_stra_2levelref( :, 2 ), sb_stra_2levelref( :, 8), '--', 'LineWidth', 1.3, 'Color', [1 0 0.2] );

xlabel( 'k' );
ylabel( 'Effective GFLOPS (2n^3/time)' );
title( 'm=n=8192, k varies, 10-core' );

grid on;
axis square;
axis( [ 0 8000 0 24.8*core_num ] );
%axis( [ 0 5000 0 248 ] );

ax = gca;
ax.YTick = core_num .* [  0, 5, 10, 15, 20, 24.8, 30, 35];
%ax.YTick = [  0, 50, 100, 150, 200, 248];

%ax.XTick = [ 0, 200, 400, 600, 800, 1000];
%ax.XTick = [ 0, 1000, 2000, 3000, 4000 ];
ax.XTick = [ 0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000];
ax.XAxis.Exponent = 3;


set( gca,'FontSize', 12 );

legend( 'Reference DGEMM', ...
        'One-level Strassen', ...
        'Two-level Strassen', ...
         'Two-level Hybrid Strassen', ...
         'One-level Strassen Naive', ...
        'Two-level Strassen Naive', ...
        'Location','SouthEast');
