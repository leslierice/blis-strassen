step_final_st;


stampede_step_final_st;


% ---------------------------------------------------------
% Machine setup
% ---------------------------------------------------------
clockrate = 3.1;
%clockrate = 3.10 * 10;
channels  = 5;
alpha     = 2.2   / channels;
%alpha     = 4   / channels;

%alpha2    = 13.91 / channels;
alpha2    = 8.0   / channels;
alpha3    = 4.0   / channels;

tau       = 1 / ( 8 * clockrate);
epsilon   = 0.5;

% ---------------------------------------------------------
% Problem setup
% ---------------------------------------------------------
%m  = 8192;
%n  = 8192;
k  = 4:4:1028;
m = k;
n = k;
%r  = 2048;

% ---------------------------------------------------------
% Setup the block size
% ---------------------------------------------------------
mc = 96;
nc = 4096;
kc = 256;

% Compute effective flops
%flops_effective  = m * n * ( 2 * k  + 1 );
flops_effective  = m .* n .* ( 2 * k );

% Other instructions
%flops_all        = m * n * ( 2 * k  + 3 + 3 + 1.0 * 12 * ( log2( r ) - 1 ) );
% flops_all        = m * n * ( 2 * k + 3 + 24 ) + epsilon * 24 * m * r * log2( r );

flops_all        = m .* n .* ( 2 * k );


% Compute the floating point operation time
Tf  = flops_all  * tau;


% Compute the memory operation time
TdBc   = 1 * n .* k;
TdAc   = 1 * m .* k  .* ceil( n / nc );
%TdCc   = 1 * ( ceil( k / kc ) -1 ) .* m .* n;
TdCc   = 1 * ( ceil( k / kc ) ) .* m .* n;
%TdB2c  = 2 * n;
%TdA2c  = 2 * ceil( n / nc ) * ( 1 * m );

% We can model L1 spilling problem here.
%TdDc   = epsilon * 2.0 * m * r * log2( r );

%Td     = TdBc  + TdAc  + TdCc  + TdB2c + TdA2c; 
Td     = TdBc  + TdAc  + TdCc;
%Tdvar6 = TdBc  + TdAc  + TdCc  + TdB2c + TdA2c + 1.0 * m * n;
Tdvar6 = Td + 1.0 * m .* n;

Td_mkl = Td + 1 * m .* k + 1 * n .* k + 2 .* m .* n;

Td_mkl = TdBc  + TdAc  + 2 * TdCc;


% ---------------------------------------------------------
% Compute the total time 
% ---------------------------------------------------------
%T      = Tf  + Td     * alpha + TdDc * alpha3;
%T      = Tf  + Tdvar6 * alpha + TdDc * alpha;
%T      = Tf  + Td * alpha;
T      = Tf  + Tdvar6 * alpha;
%T_mkl  = Tf  + Td_mkl * alpha + TdDc * alpha3;
T_mkl  = Tf  + Td_mkl * alpha;


% ---------------------------------------------------------
% Plotting
% ---------------------------------------------------------
figure;
set( gcf, 'PaperSize', [3 3]);
set( gcf, 'PaperPosition', [0.25 0.25 3 3] );
set( gcf, 'Position', [0 0 1200 800]);
%semilogx( k, flops_effective ./ T, '--', 'LineWidth', 1.3, 'Color', [0 0.4 1.0] );
plot( k, flops_effective ./ T, '--', 'LineWidth', 1.3, 'Color', 'b' );
hold on;
plot( k,  flops_effective  ./ T_mkl, '--', 'LineWidth', 1.3, 'Color', 'r' );
%hold on;
plot( stampede_run_step_final_st( :, 1 ), stampede_run_step_final_st( :,4), '.-', 'LineWidth', 2, 'Color',  [0 0.2 1.0] );
%hold on;
plot( stampede_run_step_final_st( :, 1 ), stampede_run_step_final_st( :, 5), '.-', 'LineWidth', 2, 'Color', [1 0 0.2] );

hold on;


xlabel( 'm=n=k' );
ylabel( 'GFLOPS' );
%title( 'Var#6, p=10, m=n=8192, k=2048' );
%title( 'Variant#6, nthd=10, m=n=8192, k=2048' );
title( 'p=1, m=n=k' );
grid on;
%axis square;
axis( [ 0 1028 0 30 ] );
%axis( [ 0 1028 0 248 ] );
ax = gca;
ax.YTick = [ 0, 5 , 10, 15, 20, 23.76, 25, 30 ];
%ax.YTick = [ 0 , 50 , 100, 150, 200, 248 ];
ax.XTick = [ 0, 200, 400, 600, 800, 1000 ];
set( gca,'FontSize',14 );

%legend( 'Modeled GSRNN Variant#6', ...
%        'Modeled MKL+STL', ...
%        'GSRNN Variant#6', ...
%        'MKL+STL', ...
%        'Location','SouthEast');
