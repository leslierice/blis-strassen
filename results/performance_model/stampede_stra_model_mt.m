stampede_output_stra_1level_1;

stampede_step_final_st;


output_15_stra_1level_8core;
output_12_stra_1level_1core;
output_13_stra_1level_2core;
output_14_stra_1level_4core;


% ---------------------------------------------------------
% Machine setup
% ---------------------------------------------------------
clockrate = 3.5 * 8;
%clockrate = 3.10 * 10;
channels  = 5;
%alpha     = 2.2   / channels;
alpha     = 3   / channels;

%alpha2    = 13.91 / channels;
%alpha2    = 8.0   / channels;
%alpha3    = 4.0   / channels;

tau       = 1 / ( 8 * clockrate);

epsilon   = 0.4;

% ---------------------------------------------------------
% Problem setup
% ---------------------------------------------------------
%m  = 8192;
%n  = 8192;
k  = 4:4:20480;
m = k;
n = k;

k_half = 0.5 * k;
m_half = 0.5 * m;
n_half = 0.5 * n;

% ---------------------------------------------------------
% Setup the block size
% ---------------------------------------------------------
mc = 96;
nc = 4096;
kc = 256;

% Compute effective flops
%flops_effective  = m * n * ( 2 * k  + 1 );
flops_effective  = m .* n .* ( 2 * k );

% Other instructions
flops_all_gemm        = m .* n .* ( 2 * k + 1 );
flops_all_stra        = 10 .* m_half .* k_half + 10 .* n_half .* k_half + 7 * m_half .* n_half .* ( 2 * k_half ) + 12 .* m_half .* n_half;

% Compute the floating point operation time
Tf_gemm = flops_all_gemm  * tau;
Tf_stra = flops_all_stra  * tau;


% Compute the memory operation time
TdBc   = 1 * n .* k;
TdAc   = 1 * m .* k  .* ceil( n / nc );
%TdCc   = 1 * ( ceil( k / kc ) -1 ) .* m .* n;
TdCc   = 1 * ( ceil( k / kc ) ) .* m .* n;

TdBc_half   = 1 * n_half .* k_half;
TdAc_half   = 1 * m_half .* k_half  .* ceil( n_half / nc );
%TdCc   = 1 * ( ceil( k / kc ) -1 ) .* m .* n;
TdCc_half   = 1 * ( ceil( k_half / kc ) ) .* m_half .* n_half;


% We can model L1 spilling problem here.
%TdDc   = epsilon * 2.0 * m * r * log2( r );


Td_gemm = TdBc  + TdAc  + TdCc;

Td_stra = 19 * TdBc_half  + 19 * TdAc_half  + 24 * epsilon * TdCc_half; % Between 12 and 24, actually!

%Load A1, Load A2, Store A; Load B1, Load B2, Store B; (Load A, Load B, Store C_tmp, Note here we don't need to load C_tmp); Load C_tmp; Load C1, Load C2, Store C1, Store C2;
% Only load A1, store A1 is in L3 cache, so not counted....
%Td_blis = 2.0 * TdBc  + 2.0 * TdAc  + 2 * TdCc + 2.0 * m .* n;
%Td_blis = 2.0 * TdBc  + 2.0 * TdAc  + 3 * TdCc;
%Td_blis = 3.0 * TdBc  + 3.0 * TdAc  + 3.0 * TdCc;
%Td_blis = 3.0 * TdBc  + 3.0 * TdAc  + 8 * TdCc;
%  (Load A1, Load A2, Load B1, Load B2, Load C1, Load C2, Store C1, Store C2);


% ---------------------------------------------------------
% Compute the total time 
% ---------------------------------------------------------
T_gemm      = Tf_gemm  + Td_gemm * alpha;
T_stra      = Tf_stra  + Td_stra * alpha;


% ---------------------------------------------------------
% Plotting
% ---------------------------------------------------------
figure;
set( gcf, 'PaperSize', [3 3]);
set( gcf, 'PaperPosition', [0.25 0.25 3 3] );
set( gcf, 'Position', [0 0 1200 800]);
%semilogx( k, flops_effective ./ T, '--', 'LineWidth', 1.3, 'Color', [0 0.4 1.0] );
plot( k, flops_effective ./ T_gemm, '--', 'LineWidth', 1.3, 'Color', 'b' );
hold on;
plot( k, flops_effective  ./ T_stra, '--', 'LineWidth', 1.3, 'Color', 'r' );
%hold on;
plot( data_gemm_blis15( :, 1 ), data_gemm_blis15( :,3), '.-', 'LineWidth', 1.3, 'Color',  [0 0.2 1.0] );
%hold on;
plot( data_gemm_blis15( :, 1 ), data_gemm_blis15( :, 6), '.-', 'LineWidth', 1.3, 'Color', [1 0 0.2] );

%plot( stampede_run_step_final_st( :, 1 ), stampede_run_step_final_st( :,4), '.-', 'LineWidth', 2, 'Color',  'm' );
%plot( stampede_run_step_final_st( :, 1 ), stampede_run_step_final_st( :, 5), '.-', 'LineWidth', 2, 'Color', 'g' );


hold on;


xlabel( 'm=n=k' );
ylabel( 'GFLOPS' );
%title( 'Var#6, p=10, m=n=8192, k=2048' );
%title( 'Variant#6, nthd=10, m=n=8192, k=2048' );
title( 'p=1, m=n=k' );
grid on;
%axis square;
axis( [ 0 20480 0 200 ] );
%axis( [ 0 1028 0 248 ] );
ax = gca;
%ax.YTick = [ 0, 5 , 10, 15, 20, 25, 30 ];
ax.YTick = [  0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 192, 200];

%ax.XTick = [ 0, 200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000 ];
ax.XTick = [ 0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000, 14000, 15000, 16000, 17000, 18000, 19000, 20000];
ax.XAxis.Exponent = 3;
set( gca,'FontSize',14 );

%legend( 'Modeled GSRNN Variant#6', ...
%        'Modeled MKL+STL', ...
%        'GSRNN Variant#6', ...
%        'MKL+STL', ...
%        'Location','SouthEast');
