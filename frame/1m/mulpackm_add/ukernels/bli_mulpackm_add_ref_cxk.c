/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"
#include <immintrin.h>

#include <assert.h>

#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname ) \
\
void PASTEMAC(ch,varname)( \
                           conj_t  conja, \
                           dim_t   n, \
                           void*   kappa, \
                           dim_t   len, \
                           void**  a_list, inc_t inca, inc_t lda, \
                           void**  gamma_list, \
                           void*   p,             inc_t ldp, \
                           dim_t   t_id, dim_t num_threads \
                         ) \
{ \
    ctype* restrict kappa_cast = kappa; \
    ctype* restrict acast[STRA_LIST_LEN]; \
    ctype* restrict gammacast[STRA_LIST_LEN]; \
    _Pragma("unroll") \
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) { \
        acast[ ii ] = a_list[ ii ]; \
        gammacast[ ii ] = gamma_list[ ii ]; \
    } \
    ctype* restrict pcast = p; \
\
	if ( PASTEMAC(ch,eq1)( *kappa_cast ) ) \
	{ \
		if ( bli_is_conj( conja ) ) \
		{ \
            printf("NOT SUPPORTED\n"); \
		} \
		else \
		{ \
            dim_t index_inc   = n / num_threads + ( n % num_threads ? 1 : 0 ); \
            dim_t index_start = t_id * index_inc; \
            dim_t index_end   = ( ( t_id + 1 ) * index_inc < n ) ? ( t_id + 1 ) * index_inc : n; \
            for ( dim_t index = index_start; index < index_end; index += 1 ) \
			{ \
\
                ctype* alpha1[STRA_LIST_LEN]; \
                _Pragma("unroll") \
                for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) { \
                    alpha1[ ii ] = &(acast[ii])[index * lda]; \
                } \
				ctype* pi1    = &pcast[index * ldp]; \
\
				PASTEMAC2(ch,ch,copys)( *(alpha1[ 0 ] + 0*inca), *(pi1 + 0) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1[ 0 ] + 1*inca), *(pi1 + 1) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1[ 0 ] + 2*inca), *(pi1 + 2) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1[ 0 ] + 3*inca), *(pi1 + 3) ); \
\
                _Pragma("unroll") \
                for ( dim_t ii = 1; ii < STRA_LIST_LEN; ii++ ) { \
                    if ( alpha1[ ii ] != NULL ) \
                    { \
                        PASTEMAC3(ch,ch,ch,axpys)( *(gammacast[ ii ]), *(alpha1[ ii ] + 0*inca), *(pi1 + 0) ); \
                        PASTEMAC3(ch,ch,ch,axpys)( *(gammacast[ ii ]), *(alpha1[ ii ] + 1*inca), *(pi1 + 1) ); \
                        PASTEMAC3(ch,ch,ch,axpys)( *(gammacast[ ii ]), *(alpha1[ ii ] + 2*inca), *(pi1 + 2) ); \
                        PASTEMAC3(ch,ch,ch,axpys)( *(gammacast[ ii ]), *(alpha1[ ii ] + 3*inca), *(pi1 + 3) ); \
                    } \
                } \
			} \
		} \
	} \
	else \
	{ \
            printf("NOT SUPPORTED\n"); \
	} \
}

GENTFUNC( float,    s, mulpackm_add_ref_4xk )
GENTFUNC( scomplex, c, mulpackm_add_ref_4xk )
GENTFUNC( dcomplex, z, mulpackm_add_ref_4xk )
    /* GENERATE some functions for float/scomplex/dcomple */



#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname ) \
\
void PASTEMAC(ch,varname)( \
                           conj_t  conja, \
                           dim_t   n, \
                           void*   kappa, \
                           dim_t   len, \
                           void**  a_list, inc_t inca, inc_t lda, \
                           void**  gamma_list, \
                           void*   p,             inc_t ldp, \
                           dim_t   t_id, dim_t num_threads \
                         ) \
{ \
    ctype* restrict kappa_cast = kappa; \
    ctype* restrict acast[STRA_LIST_LEN]; \
    ctype* restrict gammacast[STRA_LIST_LEN]; \
    _Pragma("unroll") \
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) { \
        acast[ ii ] = a_list[ ii ]; \
        gammacast[ ii ] = gamma_list[ ii ]; \
    } \
    ctype* restrict pcast = p; \
\
	if ( PASTEMAC(ch,eq1)( *kappa_cast ) ) \
	{ \
		if ( bli_is_conj( conja ) ) \
		{ \
            printf("NOT SUPPORTED\n"); \
		} \
		else \
		{ \
            dim_t index_inc   = n / num_threads + ( n % num_threads ? 1 : 0 ); \
            dim_t index_start = t_id * index_inc; \
            dim_t index_end   = ( ( t_id + 1 ) * index_inc < n ) ? ( t_id + 1 ) * index_inc : n; \
            for ( dim_t index = index_start; index < index_end; index += 1 ) \
			{ \
\
                ctype* alpha1[STRA_LIST_LEN]; \
                _Pragma("unroll") \
                for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) { \
                    alpha1[ ii ] = &(acast[ii])[index * lda]; \
                } \
				ctype* pi1    = &pcast[index * ldp]; \
\
				PASTEMAC(ch,copys)( *(alpha1[ 0 ] + 0*inca), *(pi1 + 0) ); \
				PASTEMAC(ch,copys)( *(alpha1[ 0 ] + 1*inca), *(pi1 + 1) ); \
				PASTEMAC(ch,copys)( *(alpha1[ 0 ] + 2*inca), *(pi1 + 2) ); \
				PASTEMAC(ch,copys)( *(alpha1[ 0 ] + 3*inca), *(pi1 + 3) ); \
				PASTEMAC(ch,copys)( *(alpha1[ 0 ] + 4*inca), *(pi1 + 4) ); \
				PASTEMAC(ch,copys)( *(alpha1[ 0 ] + 5*inca), *(pi1 + 5) ); \
				PASTEMAC(ch,copys)( *(alpha1[ 0 ] + 6*inca), *(pi1 + 6) ); \
				PASTEMAC(ch,copys)( *(alpha1[ 0 ] + 7*inca), *(pi1 + 7) ); \
\
                _Pragma("unroll") \
                for ( dim_t ii = 1; ii < STRA_LIST_LEN; ii++ ) { \
                    if ( alpha1[ ii ] != NULL ) \
                    PASTEMAC3(ch,ch,ch,axpys)( *(gammacast[ ii ]), *(alpha1[ ii ] + 0*inca), *(pi1 + 0) ); \
                    PASTEMAC3(ch,ch,ch,axpys)( *(gammacast[ ii ]), *(alpha1[ ii ] + 1*inca), *(pi1 + 1) ); \
                    PASTEMAC3(ch,ch,ch,axpys)( *(gammacast[ ii ]), *(alpha1[ ii ] + 2*inca), *(pi1 + 2) ); \
                    PASTEMAC3(ch,ch,ch,axpys)( *(gammacast[ ii ]), *(alpha1[ ii ] + 3*inca), *(pi1 + 3) ); \
                    PASTEMAC3(ch,ch,ch,axpys)( *(gammacast[ ii ]), *(alpha1[ ii ] + 4*inca), *(pi1 + 4) ); \
                    PASTEMAC3(ch,ch,ch,axpys)( *(gammacast[ ii ]), *(alpha1[ ii ] + 5*inca), *(pi1 + 5) ); \
                    PASTEMAC3(ch,ch,ch,axpys)( *(gammacast[ ii ]), *(alpha1[ ii ] + 6*inca), *(pi1 + 6) ); \
                    PASTEMAC3(ch,ch,ch,axpys)( *(gammacast[ ii ]), *(alpha1[ ii ] + 7*inca), *(pi1 + 7) ); \
                } \
			} \
		} \
	} \
	else \
	{ \
        printf("NOT SUPPORTED\n"); \
	} \
}

GENTFUNC( float,    s, mulpackm_add_ref_8xk )
GENTFUNC( scomplex, c, mulpackm_add_ref_8xk )
GENTFUNC( dcomplex, z, mulpackm_add_ref_8xk )


void bli_dmulpackm_add_ref_4xk(
                               conj_t  conja,
                               dim_t   n,  
                               void*   kappa,
                               dim_t   len,
                               void**  a_list, inc_t inca, inc_t lda,
                               void**  gamma_list,
                               void*   p,               inc_t ldp,
                               dim_t   t_id, dim_t num_threads
                         )
{
    double* restrict kappa_cast = kappa;
    double* restrict acast[STRA_LIST_LEN]={NULL};
    double* restrict gammacast[STRA_LIST_LEN]={NULL};
    #pragma unroll
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
        acast[ ii ]     = a_list[ ii ];
        //if ( acast[ ii ] == NULL ) {
        //   printf( "acast[ %lu ] is NULL\n", ii );
        //}
        gammacast[ ii ] = gamma_list[ ii ];
    }


    double* restrict pcast = p;

    //printf( "gamma: %lf\n", *((double *)gamma) );
 
    if ( *kappa_cast == 1.0 )
    {   
        if ( bli_is_conj( conja ) ) 
        {   
            printf("mulpackm_add conj area not implemented\n"); \
        }   
        else 
        {   

            __m256d gammaV[STRA_LIST_LEN];
            #pragma unroll
            for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
                gammaV[ ii ] = _mm256_broadcast_sd( gammacast[ ii ] );
                //printf( "gammacast[%lu]=%lf\n", ii, *gammacast[ ii ] );
            }

            dim_t index_inc   = n / num_threads + ( n % num_threads ? 1 : 0 );
            dim_t index_start = t_id * index_inc;
            dim_t index_end   = ( ( t_id + 1 ) * index_inc < n ) ? ( t_id + 1 ) * index_inc : n;

            for ( dim_t index = index_start; index < index_end; index += 1 )
            {
				double* restrict alpha1[STRA_LIST_LEN]={NULL};
                #pragma unroll
                for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
                    if ( acast[ ii ] != NULL ) {
                        alpha1[ ii ] = &(acast[ ii ])[index * lda];
                    } else {
                        alpha1[ ii ] = NULL;
                    }
                }

                double* restrict pi1     = &pcast[index * ldp];

                __m256d av[STRA_LIST_LEN];
                av[ 0 ] = _mm256_set_pd((alpha1[ 0 ])[3*inca + 0*lda],  
                                        (alpha1[ 0 ])[2*inca + 0*lda],  
                                        (alpha1[ 0 ])[1*inca + 0*lda],  
                                        (alpha1[ 0 ])[0*inca + 0*lda]); 

                #pragma unroll
                for ( dim_t ii = 1; ii < STRA_LIST_LEN; ii++ ) {
                    if ( alpha1[ ii ] != NULL ) {  //1. Set to NULL; 2. assign to 0
                        av[ ii ] = _mm256_set_pd((alpha1[ ii ])[3*inca + 0*lda],  
                                                 (alpha1[ ii ])[2*inca + 0*lda],  
                                                 (alpha1[ ii ])[1*inca + 0*lda],  
                                                 (alpha1[ ii ])[0*inca + 0*lda]); 
                        av[ ii ] = _mm256_mul_pd( gammaV[ ii ], av[ ii ] );
                        av[ 0 ]  = _mm256_add_pd( av[ 0 ], av[ ii ] );
                    }
                }

                _mm256_store_pd( pi1 + 0 * ldp, av[ 0 ] );
                //Print gammaV, and check the result!

                //double* aAv_d = (double *)(&aAv);
                //double* aBv_d = (double *)(&aBv);
                //double* gammaV_d = (double *)(&gammaV);
                //printf( "aAv_d: %lf, %lf, %lf, %lf\n", aAv_d[0], aAv_d[1], aAv_d[2], aAv_d[3] );
                //printf( "aBv_d: %lf, %lf, %lf, %lf\n", aBv_d[0], aBv_d[1], aBv_d[2], aBv_d[3] );
                //printf( "gammaV_d: %lf, %lf, %lf, %lf\n", gammaV_d[0], gammaV_d[1], gammaV_d[2], gammaV_d[3] );
            }
        }
    }
    else
    {
        printf("mulpackm_add kappa != 1 not implemented\n");
    }

}



void bli_dmulpackm_add_ref_8xk(
                           conj_t  conja,
                           dim_t   n,
                           void*   kappa,
                           dim_t   len,
                           void**  a_list, inc_t inca, inc_t lda,
                           void**  gamma_list,
                           void*   p,               inc_t ldp,
                           dim_t   t_id, dim_t num_threads
                          )
{
    double* restrict kappa_cast = kappa;
    double* restrict acast[STRA_LIST_LEN];
    double* restrict gammacast[STRA_LIST_LEN];
    #pragma unroll
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
        acast[ ii ]     = a_list[ ii ];
        gammacast[ ii ] = gamma_list[ ii ];
    }

    double* restrict pcast = p;

    if ( *kappa_cast == 1.0 )
    {
        if ( bli_is_conj( conja ) )
        {
            printf("mulpackm_add conj area not implemented\n"); \
        }
        else
        {
            __m256d gammaV[STRA_LIST_LEN];
            #pragma unroll
            for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
                gammaV[ ii ] = _mm256_broadcast_sd( gammacast[ ii ] );
                //printf( "gammacast[%lu]=%lf\n", ii, *gammacast[ ii ] );
            }

            dim_t index_inc   = n / num_threads + ( n % num_threads ? 1 : 0 );
            dim_t index_start = t_id * index_inc;
            dim_t index_end   = ( ( t_id + 1 ) * index_inc < n ) ? ( t_id + 1 ) * index_inc : n;

            for ( dim_t index = index_start; index < index_end; index += 1 )
            {
                double* restrict alpha1[STRA_LIST_LEN]={NULL};
                #pragma unroll
                for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
                    if ( acast[ ii ] != NULL ) {
                        alpha1[ ii ] = &(acast[ ii ])[index * lda];
                    } else {
                        alpha1[ ii ] = NULL;
                    }
                }

				double* pi1    = &pcast[index * ldp];

                __m256d av1[STRA_LIST_LEN];
                __m256d av2[STRA_LIST_LEN];
                av1[ 0 ] = _mm256_set_pd((alpha1[ 0 ])[3*inca + 0*lda],  
                                         (alpha1[ 0 ])[2*inca + 0*lda],  
                                         (alpha1[ 0 ])[1*inca + 0*lda],  
                                         (alpha1[ 0 ])[0*inca + 0*lda]); 

                av2[ 0 ] = _mm256_set_pd((alpha1[ 0 ])[7*inca + 0*lda],  
                                         (alpha1[ 0 ])[6*inca + 0*lda],  
                                         (alpha1[ 0 ])[5*inca + 0*lda],  
                                         (alpha1[ 0 ])[4*inca + 0*lda]); 

                #pragma unroll
                for ( dim_t ii = 1; ii < STRA_LIST_LEN; ii++ ) {
                    if ( alpha1[ ii ] != NULL ) {  //1. Set to NULL; 2. assign to 0
                        av1[ ii ] = _mm256_set_pd((alpha1[ ii ])[3*inca + 0*lda],  
                                                  (alpha1[ ii ])[2*inca + 0*lda],  
                                                  (alpha1[ ii ])[1*inca + 0*lda],  
                                                  (alpha1[ ii ])[0*inca + 0*lda]); 
                        av2[ ii ] = _mm256_set_pd((alpha1[ ii ])[7*inca + 0*lda],  
                                                  (alpha1[ ii ])[6*inca + 0*lda],  
                                                  (alpha1[ ii ])[5*inca + 0*lda],  
                                                  (alpha1[ ii ])[4*inca + 0*lda]); 

                        av1[ ii ] = _mm256_mul_pd( gammaV[ ii ], av1[ ii ] );
                        av2[ ii ] = _mm256_mul_pd( gammaV[ ii ], av2[ ii ] );
                        av1[ 0 ]  = _mm256_add_pd( av1[ 0 ], av1[ ii ] );
                        av2[ 0 ]  = _mm256_add_pd( av2[ 0 ], av2[ ii ] );
                    }
                }


                //Store back separately, Or aBv1+aBv2 -> aBv1?

                _mm256_store_pd( pi1 + 0 + 0 * ldp, av1[ 0 ] );
                _mm256_store_pd( pi1 + 4 + 0 * ldp, av2[ 0 ] );

            }
        }
        
    }
    else
    { \
        printf("mulpackm_add kappa != 1 not implemented\n"); \
    }

}

