/*

   BLIS
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"

#define FUNCPTR_T packm_add_fp

typedef void (*FUNCPTR_T) ( \
                           struc_t struccA, \
                           struc_t struccB, \
                           doff_t  diagoffc, \
                           diag_t  diagc, \
                           uplo_t  uploc, \
                           trans_t transcA, \
                           trans_t transcB, \
                           pack_t  schema, \
                           bool_t  invdiag, \
                           bool_t  revifup, \
                           bool_t  reviflo, \
                           dim_t   m, \
                           dim_t   n, \
                           dim_t   m_max, \
                           dim_t   n_max, \
                           void*   kappa, \
                           void*   cA, inc_t rs_cA, inc_t cs_cA, \
                           void*   gamma, \
                           void*   cB, inc_t rs_cB, inc_t cs_cB, \
                           void*   p, inc_t rs_p, inc_t cs_p, \
                                      dim_t pd_p, inc_t ps_p, \
                           packm_thrinfo_t* thread \
                         );

static FUNCPTR_T GENARRAY(ftypes, packm_add_blk_var1);

void bli_packm_add_blk_var1( obj_t*   cA,
                             obj_t*   gamma,
                             obj_t*   cB,
                             obj_t*   p,
                             packm_thrinfo_t* t )
{
	num_t     dt_cp      = bli_obj_datatype( *cA );

	struc_t   struccA    = bli_obj_struc( *cA );
    struc_t   struccB    = bli_obj_struc( *cB );
	doff_t    diagoffc   = bli_obj_diag_offset( *cA );
	diag_t    diagc      = bli_obj_diag( *cA );
	uplo_t    uploc      = bli_obj_uplo( *cA );
	trans_t   transcA    = bli_obj_conjtrans_status( *cA );
    trans_t   transcB    = bli_obj_conjtrans_status( *cB );
	pack_t    schema     = bli_obj_pack_schema( *p );
	bool_t    invdiag    = bli_obj_has_inverted_diag( *p );
	bool_t    revifup    = bli_obj_is_pack_rev_if_upper( *p );
	bool_t    reviflo    = bli_obj_is_pack_rev_if_lower( *p );

	dim_t     m_p        = bli_obj_length( *p );
	dim_t     n_p        = bli_obj_width( *p );
	dim_t     m_max_p    = bli_obj_padded_length( *p );
	dim_t     n_max_p    = bli_obj_padded_width( *p );

	void*     buf_cA      = bli_obj_buffer_at_off( *cA );
	inc_t     rs_cA       = bli_obj_row_stride( *cA );
	inc_t     cs_cA       = bli_obj_col_stride( *cA );

	void*     buf_cB      = bli_obj_buffer_at_off( *cB );
	inc_t     rs_cB       = bli_obj_row_stride( *cB );
	inc_t     cs_cB       = bli_obj_col_stride( *cB );

	void*     buf_p      = bli_obj_buffer_at_off( *p );
	inc_t     rs_p       = bli_obj_row_stride( *p );
	inc_t     cs_p       = bli_obj_col_stride( *p );
	dim_t     pd_p       = bli_obj_panel_dim( *p );
	inc_t     ps_p       = bli_obj_panel_stride( *p );


    /* Ugly implementation! Only works for double precision */
    //void*     buf_gamma  = bli_obj_buffer_for_const(bli_obj_datatype( *p ), *gamma );
    void*     buf_gamma  = bli_obj_buffer_for_const(BLIS_DOUBLE, *gamma );
    //void*     buf_gamma  = bli_obj_buffer_at_off( *gamma );
    //printf( "buf_gamma:%lf\n", *( (double *)(buf_gamma) ) );

    void*     buf_kappa;

	FUNCPTR_T f;

	// This variant assumes that the micro-kernel will always apply the
	// alpha scalar of the higher-level operation. Thus, we use BLIS_ONE
	// for kappa so that the underlying packm implementation does not
	// scale during packing.
	buf_kappa = bli_obj_buffer_for_const( dt_cp, BLIS_ONE );

	// Index into the type combination array to extract the correct
	// function pointer.
	f = ftypes[dt_cp];

	// Invoke the function.
	f( struccA,
       struccB,
	   diagoffc,
	   diagc,
	   uploc,
	   transcA,
       transcB,
	   schema,
	   invdiag,
	   revifup,
	   reviflo,
	   m_p,
	   n_p,
	   m_max_p,
	   n_max_p,
	   buf_kappa,
	   buf_cA, rs_cA, cs_cA,
       buf_gamma,
	   buf_cB, rs_cB, cs_cB,
       buf_p, rs_p, cs_p,
	          pd_p, ps_p,
	   t );

}


#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname ) \
\
void PASTEMAC(ch,varname)( \
                           struc_t struccA, \
                           struc_t struccB, \
                           doff_t  diagoffc, \
                           diag_t  diagc, \
                           uplo_t  uploc, \
                           trans_t transcA, \
                           trans_t transcB, \
                           pack_t  schema, \
                           bool_t  invdiag, \
                           bool_t  revifup, \
                           bool_t  reviflo, \
                           dim_t   m, \
                           dim_t   n, \
                           dim_t   m_max, \
                           dim_t   n_max, \
                           void*   kappa, \
                           void*   cA, inc_t rs_cA, inc_t cs_cA, \
                           void*   gamma, \
                           void*   cB, inc_t rs_cB, inc_t cs_cB, \
                           void*   p, inc_t rs_p, inc_t cs_p, \
                                      dim_t pd_p, inc_t ps_p, \
                           packm_thrinfo_t* thread \
                         ) \
{ \
	ctype* restrict kappa_cast = kappa; \
	ctype* restrict cA_cast     = cA; \
	ctype* restrict gamma_cast     = gamma; \
	ctype* restrict cB_cast     = cB; \
	ctype* restrict p_cast     = p; \
\
	dim_t           iter_dim; \
	dim_t           panel_len; \
	dim_t           panel_dim; \
	inc_t           vs_cA, vs_cB; \
	inc_t           ldcA, ldcB; \
	inc_t           ldp; \
	conj_t          conjc; \
	bool_t          row_stored; \
	/* bool_t          col_stored; */ \
\
	/* doff_t          diagoffp_i; \ */ \
\
    /* rs_cA == rs_cB, cs_cA == cs_cB */ \
\
	/* Extract the conjugation bit from the transposition argument. */ \
	conjc = bli_extract_conj( transcA ); \
\
	/* If c needs a transposition, induce it so that we can more simply
	   express the remaining parameters and code. #Jianyu#: Do we need this here? */ \
	if ( bli_does_trans( transcA ) ) \
	{ \
		bli_swap_incs( rs_cA, cs_cA ); \
		bli_negate_diag_offset( diagoffc ); \
		bli_toggle_uplo( uploc ); \
		bli_toggle_trans( transcA ); \
	} \
    if ( bli_does_trans( transcB ) ) \
    { \
        bli_swap_incs( rs_cB, cs_cB ); \
        bli_negate_diag_offset( diagoffc ); \
        bli_toggle_uplo( uploc ); \
        bli_toggle_trans( transcB ); \
    } \
\
	/* Create flags to incidate row or column storage. Note that the
	   schema bit that encodes row or column is describing the form of
	   micro-panel, not the storage in the micro-panel. Hence the
	   mismatch in "row" and "column" semantics. */ \
	row_stored = bli_is_col_packed( schema ); \
	/* col_stored = bli_is_row_packed( schema ); */ \
\
	/* If the row storage flag indicates row storage, then we are packing
	   to column panels; otherwise, if the strides indicate column storage,
	   we are packing to row panels. */ \
	if ( row_stored ) \
	{ \
		/* Prepare to pack to row-stored column panels. */ \
		iter_dim       = n; \
		panel_len  = m; \
		panel_dim  = pd_p; \
		ldcA           = rs_cA; \
		vs_cA          = cs_cA; \
        ldcB           = rs_cB; \
        vs_cB          = cs_cB; \
		ldp            = rs_p; \
	} \
	else /* if ( col_stored ) */ \
	{ \
		/* Prepare to pack to column-stored row panels. */ \
		iter_dim       = m; \
		panel_len = n; \
        panel_dim = pd_p; \
		ldcA           = cs_cA; \
		vs_cA          = rs_cA; \
        ldcB           = cs_cB; \
        vs_cB          = rs_cB; \
		ldp            = cs_p; \
	} \
\
	/* Compute the total number of iterations we'll need. */ \
	dim_t num_iter = iter_dim / panel_dim + ( iter_dim % panel_dim ? 1 : 0 ); \
\
    dim_t start_index = 0; \
    dim_t index_inc   = 1; \
\
    dim_t i; \
	for ( i = start_index; i < num_iter; i += index_inc ) \
	{ \
		ctype* restrict cA_begin     = cA_cast   + i * panel_dim * vs_cA;\
		ctype* restrict cB_begin     = cB_cast   + i * panel_dim * vs_cB;\
        ctype* restrict p_begin     = p_cast   + i * ps_p; \
\
        PASTEMAC(ch,packm_add_cxk)( \
                                  conjc, \
                                  struccA, \
                                  struccB, \
                                  panel_dim, \
                                  panel_len, \
                                  i*panel_dim, \
                                  kappa_cast, \
                                  cA_begin, vs_cA, ldcA, \
                                  gamma_cast, \
                                  cB_begin, vs_cB, ldcB, \
                                  /*p_begin, pd_p,*/ \
                                  p_begin, ldp, \
                                  thread ); \
	} \
}
INSERT_GENTFUNC_BASIC0( packm_add_blk_var1 )





    /*
     *
    ctype* restrict p_use; \
    ctype* restrict c_use; \

    //Below start_index, index_inc line
    if( leftMul ){ \
        start_index = thread_id( thread ); \
        index_inc   = thread_num_threads( thread ); \
    } \

    */
