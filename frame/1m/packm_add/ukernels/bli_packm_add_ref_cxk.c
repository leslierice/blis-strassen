/*

   BLIS
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"
#include <immintrin.h>


#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname ) \
\
void PASTEMAC(ch,varname)( \
                           conj_t  conja, \
                           struc_t struccA, \
                           struc_t struccB, \
                           dim_t   n, \
                           dim_t   panel_off, \
                           void*   kappa, \
                           void*   aA, inc_t incaA, inc_t ldaA, \
                           void*   gamma, \
                           void*   aB, inc_t incaB, inc_t ldaB, \
                           void*   p,             inc_t ldp, \
                           dim_t   t_id, dim_t num_threads \
                         ) \
{ \
    ctype* restrict kappa_cast = kappa; \
    ctype* restrict aAcast = aA; \
    ctype* restrict aBcast = aB; \
    ctype* restrict pcast = p; \
    ctype* restrict gammacast = gamma; \
\
	if ( PASTEMAC(ch,eq1)( *kappa_cast ) ) \
	{ \
		if ( bli_is_conj( conja ) ) \
		{ \
            printf("NOT SUPPORTED\n"); \
		} \
		else \
		{ \
            dim_t index_inc   = n / num_threads + ( n % num_threads ? 1 : 0 ); \
            dim_t index_start = t_id * index_inc; \
            dim_t index_end   = ( ( t_id + 1 ) * index_inc < n ) ? ( t_id + 1 ) * index_inc : n; \
 \
            for ( dim_t index = index_start; index < index_end; index += 1 ) \
            { \
\
				ctype* alphaA1 = &aAcast[index * ldaA]; \
				ctype* alphaB1 = &aBcast[index * ldaB]; \
				ctype* pi1    = &pcast[index * ldp]; \
\
				PASTEMAC2(ch,ch,copys)( *(alphaA1 + 0*incaA), *(pi1 + 0) ); \
				PASTEMAC2(ch,ch,copys)( *(alphaA1 + 1*incaA), *(pi1 + 1) ); \
				PASTEMAC2(ch,ch,copys)( *(alphaA1 + 2*incaA), *(pi1 + 2) ); \
				PASTEMAC2(ch,ch,copys)( *(alphaA1 + 3*incaA), *(pi1 + 3) ); \
\
				PASTEMAC3(ch,ch,ch,axpys)( *(gammacast), *(alphaB1 + 0*incaB), *(pi1 + 0) ); \
				PASTEMAC3(ch,ch,ch,axpys)( *(gammacast), *(alphaB1 + 1*incaB), *(pi1 + 1) ); \
				PASTEMAC3(ch,ch,ch,axpys)( *(gammacast), *(alphaB1 + 2*incaB), *(pi1 + 2) ); \
				PASTEMAC3(ch,ch,ch,axpys)( *(gammacast), *(alphaB1 + 3*incaB), *(pi1 + 3) ); \
			} \
		} \
	} \
	else \
	{ \
            printf("NOT SUPPORTED\n"); \
	} \
}

GENTFUNC( float,    s, packm_add_ref_4xk )
GENTFUNC( scomplex, c, packm_add_ref_4xk )
GENTFUNC( dcomplex, z, packm_add_ref_4xk )
    /* GENERATE some functions for float/scomplex/dcomple */



#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname ) \
\
void PASTEMAC(ch,varname)( \
                           conj_t  conja, \
                           struc_t struccA, \
                           struc_t struccB, \
                           dim_t   n, \
                           dim_t   panel_off, \
                           void*   kappa, \
                           void*   aA, inc_t incaA, inc_t ldaA, \
                           void*   gamma, \
                           void*   aB, inc_t incaB, inc_t ldaB, \
                           void*   p,             inc_t ldp, \
                           dim_t   t_id, dim_t num_threads \
                         ) \
{ \
    ctype* restrict kappa_cast = kappa; \
    ctype* restrict aAcast = aA; \
    ctype* restrict aBcast = aB; \
    ctype* restrict pcast = p; \
    ctype* restrict gammacast = gamma; \
\
	if ( PASTEMAC(ch,eq1)( *kappa_cast ) ) \
	{ \
		if ( bli_is_conj( conja ) ) \
		{ \
            printf("NOT SUPPORTED\n"); \
		} \
		else \
		{ \
            dim_t index_inc   = n / num_threads + ( n % num_threads ? 1 : 0 ); \
            dim_t index_start = t_id * index_inc; \
            dim_t index_end   = ( ( t_id + 1 ) * index_inc < n ) ? ( t_id + 1 ) * index_inc : n; \
 \
            for ( dim_t index = index_start; index < index_end; index += 1 ) \
            { \
				ctype* alphaA1 = &aAcast[index * ldaA]; \
				ctype* alphaB1 = &aBcast[index * ldaB]; \
				ctype* pi1    = &pcast[index * ldp]; \
\
				PASTEMAC(ch,copys)( *(alphaA1 + 0*incaA), *(pi1 + 0) ); \
				PASTEMAC(ch,copys)( *(alphaA1 + 1*incaA), *(pi1 + 1) ); \
				PASTEMAC(ch,copys)( *(alphaA1 + 2*incaA), *(pi1 + 2) ); \
				PASTEMAC(ch,copys)( *(alphaA1 + 3*incaA), *(pi1 + 3) ); \
				PASTEMAC(ch,copys)( *(alphaA1 + 4*incaA), *(pi1 + 4) ); \
				PASTEMAC(ch,copys)( *(alphaA1 + 5*incaA), *(pi1 + 5) ); \
				PASTEMAC(ch,copys)( *(alphaA1 + 6*incaA), *(pi1 + 6) ); \
				PASTEMAC(ch,copys)( *(alphaA1 + 7*incaA), *(pi1 + 7) ); \
\
				PASTEMAC3(ch,ch,ch,axpys)( *(gammacast), *(alphaB1 + 0*incaB), *(pi1 + 0) ); \
				PASTEMAC3(ch,ch,ch,axpys)( *(gammacast), *(alphaB1 + 1*incaB), *(pi1 + 1) ); \
				PASTEMAC3(ch,ch,ch,axpys)( *(gammacast), *(alphaB1 + 2*incaB), *(pi1 + 2) ); \
				PASTEMAC3(ch,ch,ch,axpys)( *(gammacast), *(alphaB1 + 3*incaB), *(pi1 + 3) ); \
				PASTEMAC3(ch,ch,ch,axpys)( *(gammacast), *(alphaB1 + 4*incaB), *(pi1 + 4) ); \
				PASTEMAC3(ch,ch,ch,axpys)( *(gammacast), *(alphaB1 + 5*incaB), *(pi1 + 5) ); \
				PASTEMAC3(ch,ch,ch,axpys)( *(gammacast), *(alphaB1 + 6*incaB), *(pi1 + 6) ); \
				PASTEMAC3(ch,ch,ch,axpys)( *(gammacast), *(alphaB1 + 7*incaB), *(pi1 + 7) ); \
			} \
		} \
	} \
	else \
	{ \
        printf("NOT SUPPORTED\n"); \
	} \
}

GENTFUNC( float,    s, packm_add_ref_8xk )
GENTFUNC( scomplex, c, packm_add_ref_8xk )
GENTFUNC( dcomplex, z, packm_add_ref_8xk )


void bli_dpackm_add_ref_4xk(
                           conj_t  conja,
                           struc_t struccA,
                           struc_t struccB,
                           dim_t   n,
                           dim_t   panel_off,
                           void*   kappa,
                           void*   aA, inc_t incaA, inc_t ldaA,
                           void*   gamma,
                           void*   aB, inc_t incaB, inc_t ldaB,
                           void*   p,               inc_t ldp,
                           dim_t   t_id, dim_t num_threads
                         )
{
    double* restrict kappa_cast = kappa;
    double* restrict aAcast = aA;
    double* restrict aBcast = aB;
    double* restrict pcast = p;
    double* restrict gammacast = gamma;


    //printf( "gamma: %lf\n", *((double *)gamma) );

    if ( *kappa_cast == 1.0 )
    {
        if ( bli_is_conj( conja ) )
        {
            printf("packm_add conj area not implemented\n"); \
        }
        else
        {
            #ifdef REFC
            //double* alphaA1 = (double *)aAcast;
            //double* alphaB1 = (double *)aBcast;
            //double* pi1     = (double *)pcast;
            dim_t index_inc   = n / num_threads + ( n % num_threads ? 1 : 0 );
            dim_t index_start = t_id * index_inc;
            dim_t index_end   = ( ( t_id + 1 ) * index_inc < n ) ? ( t_id + 1 ) * index_inc : n;

            for ( dim_t index = index_start; index < index_end; index += 1 )
            {
                double* restrict alphaA1 = &aAcast[index * ldaA];
                double* restrict alphaB1 = &aBcast[index * ldaA];
                double* restrict pi1     = &pcast[index * ldp];
                #pragma unroll
                //#pragma vector aligned nontemporal
                for ( int kk = 0; kk < 4; kk ++ ) {
                    //pi1[kk] = alphaA1[kk*incaA] + alphaB1[kk*incaB];
                    pi1[kk] = alphaA1[kk*incaA] + *gammacast * alphaB1[kk*incaA];
                }
                //alphaA1 += ldaA;
                //alphaB1 += ldaA;
                //pi1     += ldp;
            }
            #else
            __m256d gammaV = _mm256_broadcast_sd( gammacast );

            dim_t index_inc   = n / num_threads + ( n % num_threads ? 1 : 0 );
            dim_t index_start = t_id * index_inc;
            dim_t index_end   = ( ( t_id + 1 ) * index_inc < n ) ? ( t_id + 1 ) * index_inc : n;

            for ( dim_t index = index_start; index < index_end; index += 1 )
            {
				double* restrict alphaA1 = &aAcast[index * ldaA];
				double* restrict alphaB1 = &aBcast[index * ldaB];
				double* restrict pi1     = &pcast[index * ldp];

                __m256d aAv = _mm256_set_pd(alphaA1[3*incaA + 0*ldaA],
                                            alphaA1[2*incaA + 0*ldaA],
                                            alphaA1[1*incaA + 0*ldaA],
                                            alphaA1[0*incaA + 0*ldaA]);

                __m256d aBv = _mm256_set_pd(alphaB1[3*incaB + 0*ldaB],
                                            alphaB1[2*incaB + 0*ldaB],
                                            alphaB1[1*incaB + 0*ldaB],
                                            alphaB1[0*incaB + 0*ldaB]);

                aBv = _mm256_mul_pd( gammaV, aBv );
                aAv = _mm256_add_pd( aAv, aBv );

                _mm256_store_pd( pi1 + 0 * ldp, aAv );
                //Print gammaV, and check the result!

                //double* aAv_d = (double *)(&aAv);
                //double* aBv_d = (double *)(&aBv);
                //double* gammaV_d = (double *)(&gammaV);
                //printf( "aAv_d: %lf, %lf, %lf, %lf\n", aAv_d[0], aAv_d[1], aAv_d[2], aAv_d[3] );
                //printf( "aBv_d: %lf, %lf, %lf, %lf\n", aBv_d[0], aBv_d[1], aBv_d[2], aBv_d[3] );
                //printf( "gammaV_d: %lf, %lf, %lf, %lf\n", gammaV_d[0], gammaV_d[1], gammaV_d[2], gammaV_d[3] );
            }
            #endif
        }
    }
    else
    {
        printf("packm_add kappa != 1 not implemented\n");
    }

}



void bli_dpackm_add_ref_8xk(
                           conj_t  conja,
                           struc_t struccA,
                           struc_t struccB,
                           dim_t   n,
                           dim_t   panel_off,
                           void*   kappa,
                           void*   aA, inc_t incaA, inc_t ldaA,
                           void*   gamma,
                           void*   aB, inc_t incaB, inc_t ldaB,
                           void*   p,               inc_t ldp,
                           dim_t   t_id, dim_t num_threads
                          )
{
    double* restrict kappa_cast = kappa;
    double* restrict aAcast = aA;
    double* restrict aBcast = aB;
    double* restrict pcast = p;
    double* restrict gammacast = gamma;

    if ( *kappa_cast == 1.0 )
    {
        if ( bli_is_conj( conja ) )
        {
            printf("packm_add conj area not implemented\n"); \
        }
        else
        {
            #ifdef REFC
            //double* alphaA1 = (double *)aAcast;
            //double* alphaB1 = (double *)aBcast;
            //double* pi1     = (double *)pcast;
            dim_t index_inc   = n / num_threads + ( n % num_threads ? 1 : 0 );
            dim_t index_start = t_id * index_inc;
            dim_t index_end   = ( ( t_id + 1 ) * index_inc < n ) ? ( t_id + 1 ) * index_inc : n;

            for ( dim_t index = index_start; index < index_end; index += 1 )
            {
                double* restrict alphaA1 = &aAcast[index * ldaA];
                double* restrict alphaB1 = &aBcast[index * ldaA];
                double* restrict pi1     = &pcast[index * ldp];
                #pragma unroll
                //#pragma vector aligned nontemporal
                for ( int kk = 0; kk < 8; kk ++ ) {
                    //pi1[kk] = alphaA1[kk*incaA] + alphaB1[kk*incaB];
                    pi1[kk] = alphaA1[kk*incaA] + *gammacast * alphaB1[kk*incaA];
                }
                //alphaA1 += ldaA;
                //alphaB1 += ldaA;
                //pi1     += ldp;
            }
            #else
            __m256d gammaV = _mm256_broadcast_sd( gammacast );

            dim_t index_inc   = n / num_threads + ( n % num_threads ? 1 : 0 );
            dim_t index_start = t_id * index_inc;
            dim_t index_end   = ( ( t_id + 1 ) * index_inc < n ) ? ( t_id + 1 ) * index_inc : n;

            for ( dim_t index = index_start; index < index_end; index += 1 )
            {
				double* alphaA1;
                double* alphaB1;
                __m256d aAv1, aAv2, aBv1, aBv2;
				double* pi1    = &pcast[index * ldp];

                if ( bli_is_symmetric( struccA ) && ( index - panel_off > 0 ) ) {
                    alphaA1 = &aAcast[ index - panel_off + panel_off * ldaA];
                    if ( index - panel_off == 1 ) {
                        aAv1 = _mm256_set_pd(alphaA1[2*incaA + 1*ldaA],
                                                     alphaA1[1*incaA + 1*ldaA],
                                                     alphaA1[0*incaA + 1*ldaA],
                                                     alphaA1[0*incaA + 0*ldaA]);
                        aAv2 = _mm256_set_pd(alphaA1[6*incaA + 1*ldaA],
                                                     alphaA1[5*incaA + 1*ldaA],
                                                     alphaA1[4*incaA + 1*ldaA],
                                                     alphaA1[3*incaA + 1*ldaA]);
                    } else if ( index - panel_off == 2 ) {
                        aAv1 = _mm256_set_pd(alphaA1[1*incaA + 2*ldaA],
                                                     alphaA1[0*incaA + 2*ldaA],
                                                     alphaA1[0*incaA + 1*ldaA],
                                                     alphaA1[0*incaA + 0*ldaA]);
                        aAv2 = _mm256_set_pd(alphaA1[5*incaA + 2*ldaA],
                                                     alphaA1[4*incaA + 2*ldaA],
                                                     alphaA1[3*incaA + 2*ldaA],
                                                     alphaA1[2*incaA + 2*ldaA]);
                    } else if ( index - panel_off == 3 ) {
                        aAv1 = _mm256_set_pd(alphaA1[0*incaA + 3*ldaA],
                                                     alphaA1[0*incaA + 2*ldaA],
                                                     alphaA1[0*incaA + 1*ldaA],
                                                     alphaA1[0*incaA + 0*ldaA]);
                        aAv2 = _mm256_set_pd(alphaA1[4*incaA + 3*ldaA],
                                                     alphaA1[3*incaA + 3*ldaA],
                                                     alphaA1[2*incaA + 3*ldaA],
                                                     alphaA1[1*incaA + 3*ldaA]);
                    } else if ( index - panel_off == 4 ) {
                        aAv1 = _mm256_set_pd(alphaA1[0*incaA + 3*ldaA],
                                                     alphaA1[0*incaA + 2*ldaA],
                                                     alphaA1[0*incaA + 1*ldaA],
                                                     alphaA1[0*incaA + 0*ldaA]);
                        aAv2 = _mm256_set_pd(alphaA1[3*incaA + 4*ldaA],
                                                     alphaA1[2*incaA + 4*ldaA],
                                                     alphaA1[1*incaA + 4*ldaA],
                                                     alphaA1[0*incaA + 4*ldaA]);
                    } else if ( index - panel_off == 5 ) {
                        aAv1 = _mm256_set_pd(alphaA1[0*incaA + 3*ldaA],
                                                     alphaA1[0*incaA + 2*ldaA],
                                                     alphaA1[0*incaA + 1*ldaA],
                                                     alphaA1[0*incaA + 0*ldaA]);
                        aAv2 = _mm256_set_pd(alphaA1[2*incaA + 5*ldaA],
                                                     alphaA1[1*incaA + 5*ldaA],
                                                     alphaA1[0*incaA + 5*ldaA],
                                                     alphaA1[0*incaA + 4*ldaA]);
                    } else if ( index - panel_off == 6 ) {
                        aAv1 = _mm256_set_pd(alphaA1[0*incaA + 3*ldaA],
                                                     alphaA1[0*incaA + 2*ldaA],
                                                     alphaA1[0*incaA + 1*ldaA],
                                                     alphaA1[0*incaA + 0*ldaA]);
                        aAv2 = _mm256_set_pd(alphaA1[1*incaA + 6*ldaA],
                                                     alphaA1[0*incaA + 6*ldaA],
                                                     alphaA1[0*incaA + 5*ldaA],
                                                     alphaA1[0*incaA + 4*ldaA]);
                    } else {
                        aAv1 = _mm256_set_pd(alphaA1[0*incaA + 3*ldaA],
                                                     alphaA1[0*incaA + 2*ldaA],
                                                     alphaA1[0*incaA + 1*ldaA],
                                                     alphaA1[0*incaA + 0*ldaA]);
                        aAv2 = _mm256_set_pd(alphaA1[0*incaA + 7*ldaA],
                                                     alphaA1[0*incaA + 6*ldaA],
                                                     alphaA1[0*incaA + 5*ldaA],
                                                     alphaA1[0*incaA + 4*ldaA]);
                    }
                } else {
                    alphaA1 = &aAcast[index * ldaA];
                    aAv1 = _mm256_set_pd(alphaA1[3*incaA + 0*ldaA],
                                                 alphaA1[2*incaA + 0*ldaA],
                                                 alphaA1[1*incaA + 0*ldaA],
                                                 alphaA1[0*incaA + 0*ldaA]);
                    aAv2 = _mm256_set_pd(alphaA1[7*incaA + 0*ldaA],
                                                 alphaA1[6*incaA + 0*ldaA],
                                                 alphaA1[5*incaA + 0*ldaA],
                                                 alphaA1[4*incaA + 0*ldaA]);
                }
                if ( bli_is_symmetric( struccB ) && ( index - panel_off > 0 ) ) {
                    alphaB1 = &aBcast[ index - panel_off + panel_off * ldaB];
                    if ( index - panel_off == 1 ) {
                        aBv1 = _mm256_set_pd(alphaB1[2*incaB + 1*ldaB],
                                                     alphaB1[1*incaB + 1*ldaB],
                                                     alphaB1[0*incaB + 1*ldaB],
                                                     alphaB1[0*incaB + 0*ldaB]);
                        aBv2 = _mm256_set_pd(alphaB1[6*incaB + 1*ldaB],
                                                     alphaB1[5*incaB + 1*ldaB],
                                                     alphaB1[4*incaB + 1*ldaB],
                                                     alphaB1[3*incaB + 1*ldaB]);
                    } else if ( index - panel_off == 2 ) {
                        aBv1 = _mm256_set_pd(alphaB1[1*incaB + 2*ldaB],
                                                     alphaB1[0*incaB + 2*ldaB],
                                                     alphaB1[0*incaB + 1*ldaB],
                                                     alphaB1[0*incaB + 0*ldaB]);
                        aBv2 = _mm256_set_pd(alphaB1[5*incaB + 2*ldaB],
                                                     alphaB1[4*incaB + 2*ldaB],
                                                     alphaB1[3*incaB + 2*ldaB],
                                                     alphaB1[2*incaB + 2*ldaB]);
                    } else if ( index - panel_off == 3 ) {
                        aBv1 = _mm256_set_pd(alphaB1[0*incaB + 3*ldaB],
                                                     alphaB1[0*incaB + 2*ldaB],
                                                     alphaB1[0*incaB + 1*ldaB],
                                                     alphaB1[0*incaB + 0*ldaB]);
                        aBv2 = _mm256_set_pd(alphaB1[4*incaB + 3*ldaB],
                                                     alphaB1[3*incaB + 3*ldaB],
                                                     alphaB1[2*incaB + 3*ldaB],
                                                     alphaB1[1*incaB + 3*ldaB]);
                    } else if ( index - panel_off == 4 ) {
                        aBv1 = _mm256_set_pd(alphaB1[0*incaB + 3*ldaB],
                                                     alphaB1[0*incaB + 2*ldaB],
                                                     alphaB1[0*incaB + 1*ldaB],
                                                     alphaB1[0*incaB + 0*ldaB]);
                        aBv2 = _mm256_set_pd(alphaB1[3*incaB + 4*ldaB],
                                                     alphaB1[2*incaB + 4*ldaB],
                                                     alphaB1[1*incaB + 4*ldaB],
                                                     alphaB1[0*incaB + 4*ldaB]);
                    } else if ( index - panel_off == 5 ) {
                        aBv1 = _mm256_set_pd(alphaB1[0*incaB + 3*ldaB],
                                                     alphaB1[0*incaB + 2*ldaB],
                                                     alphaB1[0*incaB + 1*ldaB],
                                                     alphaB1[0*incaB + 0*ldaB]);
                        aBv2 = _mm256_set_pd(alphaB1[2*incaB + 5*ldaB],
                                                     alphaB1[1*incaB + 5*ldaB],
                                                     alphaB1[0*incaB + 5*ldaB],
                                                     alphaB1[0*incaB + 4*ldaB]);
                    } else if ( index - panel_off == 6 ) {
                        aBv1 = _mm256_set_pd(alphaB1[0*incaB + 3*ldaB],
                                                     alphaB1[0*incaB + 2*ldaB],
                                                     alphaB1[0*incaB + 1*ldaB],
                                                     alphaB1[0*incaB + 0*ldaB]);
                        aBv2 = _mm256_set_pd(alphaB1[1*incaB + 6*ldaB],
                                                     alphaB1[0*incaB + 6*ldaB],
                                                     alphaB1[0*incaB + 5*ldaB],
                                                     alphaB1[0*incaB + 4*ldaB]);
                    } else {
                        aBv1 = _mm256_set_pd(alphaB1[0*incaB + 3*ldaB],
                                                     alphaB1[0*incaB + 2*ldaB],
                                                     alphaB1[0*incaB + 1*ldaB],
                                                     alphaB1[0*incaB + 0*ldaB]);
                        aBv2 = _mm256_set_pd(alphaB1[0*incaB + 7*ldaB],
                                                     alphaB1[0*incaB + 6*ldaB],
                                                     alphaB1[0*incaB + 5*ldaB],
                                                     alphaB1[0*incaB + 4*ldaB]);
                    }
                } else {
                    alphaB1 = &aBcast[index * ldaB];
                    aBv1 = _mm256_set_pd(alphaB1[3*incaB + 0*ldaB],
                                                 alphaB1[2*incaB + 0*ldaB],
                                                 alphaB1[1*incaB + 0*ldaB],
                                                 alphaB1[0*incaB + 0*ldaB]);
                    aBv2 = _mm256_set_pd(alphaB1[7*incaB + 0*ldaB],
                                                 alphaB1[6*incaB + 0*ldaB],
                                                 alphaB1[5*incaB + 0*ldaB],
                                                 alphaB1[4*incaB + 0*ldaB]);
                }

                aBv1 = _mm256_mul_pd( gammaV, aBv1 );
                aBv2 = _mm256_mul_pd( gammaV, aBv2 );

                aAv1 = _mm256_add_pd( aAv1, aBv1 );
                aAv2 = _mm256_add_pd( aAv2, aBv2 );

                //Store back separately, Or aBv1+aBv2 -> aBv1?

                _mm256_store_pd( pi1 + 0 + 0 * ldp, aAv1 );
                _mm256_store_pd( pi1 + 4 + 0 * ldp, aAv2 );

            }
            #endif
        }

    }
    else
    { \
        printf("packm_add kappa != 1 not implemented\n"); \
    }

}
