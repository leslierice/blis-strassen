/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"

#define FUNCPTR_T mulpackm_add2_fp

typedef void (*FUNCPTR_T) ( \
                           struc_t strucc, \
                           doff_t  diagoffc, \
                           diag_t  diagc, \
                           uplo_t  uploc, \
                           trans_t transc, \
                           pack_t  schema, \
                           bool_t  invdiag, \
                           bool_t  revifup, \
                           bool_t  reviflo, \
                           dim_t   m, \
                           dim_t   n, \
                           dim_t   m_max, \
                           dim_t   n_max, \
                           void*   kappa, \
                           dim_t   len,
                           void**  c_list, inc_t rs_c, inc_t cs_c, \
                           void**  gamma_list, \
                           void*   p, inc_t rs_p, inc_t cs_p, \
                                      dim_t pd_p, inc_t ps_p, \
                           packm_thrinfo_t* thread \
                         );

static FUNCPTR_T GENARRAY(ftypes, mulpackm_add2_blk_var1);

void bli_mulpackm_add2_blk_var1( dim_t    len,
                                obj_t**  c_list,
                                obj_t**  gamma_list,
                                obj_t*   p,
                                packm_thrinfo_t* t )
{
	num_t     dt_cp      = bli_obj_datatype( *c_list[ 0 ] );

	struc_t   strucc     = bli_obj_struc( *c_list[ 0 ] );
	doff_t    diagoffc   = bli_obj_diag_offset( *c_list[ 0 ] );
	diag_t    diagc      = bli_obj_diag( *c_list[ 0 ] );
	uplo_t    uploc      = bli_obj_uplo( *c_list[ 0 ] );
	trans_t   transc     = bli_obj_conjtrans_status( *c_list[ 0 ] );
	pack_t    schema     = bli_obj_pack_schema( *p );
	bool_t    invdiag    = bli_obj_has_inverted_diag( *p );
	bool_t    revifup    = bli_obj_is_pack_rev_if_upper( *p );
	bool_t    reviflo    = bli_obj_is_pack_rev_if_lower( *p );

	dim_t     m_p        = bli_obj_length( *p );
	dim_t     n_p        = bli_obj_width( *p );
	dim_t     m_max_p    = bli_obj_padded_length( *p );
	dim_t     n_max_p    = bli_obj_padded_width( *p );

    void*     buf_c[STRA_LIST_LEN]={NULL};
    #pragma unroll
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
        //printf( "ii: %d\n" , ii );
        //if ( bli_obj_buffer( *c_list[ ii ] ) != NULL ) {
        //    printf( "ii: %d not NULL\n", ii );
        //    bli_printm( " c_list[  ]", c_list[ ii ], "%4.1f", "" ); 
        //}
        if ( bli_obj_buffer( *c_list[ ii ] ) != NULL ) 
            buf_c[ ii ] = bli_obj_buffer_at_off( *c_list[ ii ] );
        else
            buf_c[ ii ] = NULL;
        //if ( buf_c[ ii ] != NULL ) {
        //    printf( "bli_mulpackm_add2_blk_var1:buf_c[%lu]:%lf\n", ii, *(double *)buf_c[ ii ] );
        //} else {
        //    printf( "bli_mulpackm_add2_blk_var1:buf_c[%lu] is NULL!\n", ii );
        //}
    }
	inc_t     rs_c       = bli_obj_row_stride( *c_list[ 0 ] );
	inc_t     cs_c       = bli_obj_col_stride( *c_list[ 0 ] );


	void*     buf_p      = bli_obj_buffer_at_off( *p );
	inc_t     rs_p       = bli_obj_row_stride( *p );
	inc_t     cs_p       = bli_obj_col_stride( *p );
	dim_t     pd_p       = bli_obj_panel_dim( *p );
	inc_t     ps_p       = bli_obj_panel_stride( *p );


    /* Ugly implementation! Only works for double precision */
    //void*     buf_gamma  = bli_obj_buffer_for_const(bli_obj_datatype( *p ), *gamma );
    void*     buf_gamma[STRA_LIST_LEN];
    #pragma unroll
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
        buf_gamma[ ii ] = bli_obj_buffer_for_const(BLIS_DOUBLE, *gamma_list[ ii ] );
    }
    //void*     buf_gamma  = bli_obj_buffer_at_off( *gamma );
    //printf( "buf_gamma:%lf\n", *( (double *)(buf_gamma) ) );

    void*     buf_kappa;

	FUNCPTR_T f;

	// This variant assumes that the micro-kernel will always apply the
	// alpha scalar of the higher-level operation. Thus, we use BLIS_ONE
	// for kappa so that the underlying packm implementation does not
	// scale during packing.
	buf_kappa = bli_obj_buffer_for_const( dt_cp, BLIS_ONE );

	// Index into the type combination array to extract the correct
	// function pointer.
	f = ftypes[dt_cp];

	// Invoke the function.
	f( strucc,
	   diagoffc,
	   diagc,
	   uploc,
	   transc,
	   schema,
	   invdiag,
	   revifup,
	   reviflo,
	   m_p,
	   n_p,
	   m_max_p,
	   n_max_p,
	   buf_kappa,
       len,
	   buf_c, rs_c, cs_c,
       buf_gamma,
       buf_p, rs_p, cs_p,
	          pd_p, ps_p,
	   t );

}


#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname ) \
\
void PASTEMAC(ch,varname)( \
                           struc_t strucc, \
                           doff_t  diagoffc, \
                           diag_t  diagc, \
                           uplo_t  uploc, \
                           trans_t transc, \
                           pack_t  schema, \
                           bool_t  invdiag, \
                           bool_t  revifup, \
                           bool_t  reviflo, \
                           dim_t   m, \
                           dim_t   n, \
                           dim_t   m_max, \
                           dim_t   n_max, \
                           void*   kappa, \
                           dim_t   len, \
                           void**  c_list, inc_t rs_c, inc_t cs_c, \
                           void**  gamma_list, \
                           void*   p, inc_t rs_p, inc_t cs_p, \
                                      dim_t pd_p, inc_t ps_p, \
                           packm_thrinfo_t* thread \
                         ) \
{ \
	ctype* restrict kappa_cast = kappa; \
    ctype* restrict c_cast[STRA_LIST_LEN]={NULL}; \
	ctype* restrict gamma_cast[STRA_LIST_LEN]; \
    /* ctype* c_cast[STRA_LIST_LEN]; \
	ctype* gamma_cast[STRA_LIST_LEN]; */ \
    _Pragma("unroll") \
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) { \
        c_cast[ ii ]     = c_list[ ii ]; \
        /*if ( c_list[ ii ] == NULL ) \
           printf( "mulpackm_add2_blk_var1: c_list[ %lu ] is NULL\n", ii );  \
        else \
           printf( "mulpackm_add2_blk_var1: c_list[ %lu ] != NULL\n", ii );*/  \
        gamma_cast[ ii ] = gamma_list[ ii ]; \
    } \
	ctype* restrict p_cast     = p; \
\
	dim_t           iter_dim; \
	dim_t           panel_len; \
	dim_t           panel_dim; \
	inc_t           vs_c; \
	inc_t           ldc; \
	inc_t           ldp; \
	conj_t          conjc; \
	bool_t          row_stored; \
	/* bool_t          col_stored; */ \
\
	/* doff_t          diagoffp_i; \ */ \
\
    /* rs_cA == rs_cB, cs_cA == cs_cB */ \
\
	/* Extract the conjugation bit from the transposition argument. */ \
	conjc = bli_extract_conj( transc ); \
\
	/* If c needs a transposition, induce it so that we can more simply
	   express the remaining parameters and code. #Jianyu#: Do we need this here? */ \
	if ( bli_does_trans( transc ) ) \
	{ \
		bli_swap_incs( rs_c, cs_c ); \
		bli_negate_diag_offset( diagoffc ); \
		bli_toggle_uplo( uploc ); \
		bli_toggle_trans( transc ); \
	} \
\
	/* Create flags to incidate row or column storage. Note that the
	   schema bit that encodes row or column is describing the form of
	   micro-panel, not the storage in the micro-panel. Hence the
	   mismatch in "row" and "column" semantics. */ \
	row_stored = bli_is_col_packed( schema ); \
	/* col_stored = bli_is_row_packed( schema ); */ \
\
	/* If the row storage flag indicates row storage, then we are packing
	   to column panels; otherwise, if the strides indicate column storage,
	   we are packing to row panels. */ \
	if ( row_stored ) \
	{ \
		/* Prepare to pack to row-stored column panels. */ \
		iter_dim       = n; \
		panel_len  = m; \
		panel_dim  = pd_p; \
		ldc            = rs_c; \
		vs_c           = cs_c; \
		ldp            = rs_p; \
	} \
	else /* if ( col_stored ) */ \
	{ \
		/* Prepare to pack to column-stored row panels. */ \
		iter_dim       = m; \
		panel_len = n; \
        panel_dim = pd_p; \
		ldc            = cs_c; \
		vs_c           = rs_c; \
		ldp            = cs_p; \
	} \
\
	/* Compute the total number of iterations we'll need. */ \
	dim_t num_iter = iter_dim / panel_dim + ( iter_dim % panel_dim ? 1 : 0 ); \
\
    dim_t num_threads = thread_num_threads( thread ); \
    dim_t thread_id   = thread_id( thread ); \
\
    dim_t index_inc   = num_iter / num_threads + ( num_iter % num_threads ? 1 : 0 ); \
    dim_t index_start = ( thread_id ) * index_inc; \
    dim_t temp_upper_bound = ( thread_id + 1 ) * index_inc; \
    dim_t index_end   = temp_upper_bound < num_iter ? temp_upper_bound : num_iter; \
\
    dim_t i; \
\
	for ( i = index_start; i < index_end; i += 1 )  \
	{ \
        ctype* restrict c_begin[STRA_LIST_LEN]={NULL}; \
        _Pragma("unroll") \
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) { \
            if ( c_cast[ ii ] != NULL ) \
                c_begin[ ii ] = c_cast[ ii ] + i * panel_dim * vs_c; \
            else \
                c_begin[ ii ] = NULL; \
        } \
        ctype* restrict p_begin     = p_cast   + i * ps_p;  \
\
        PASTEMAC(ch,mulpackm_add2_cxk)( \
                                  conjc, \
                                  panel_dim, \
                                  panel_len, \
                                  kappa_cast, \
                                  len, \
                                  (void **)c_begin, vs_c, ldc, \
                                  (void **)gamma_cast, \
                                  /*p_begin, pd_p,*/ \
                                  p_begin, ldp ); \
	} \
}
INSERT_GENTFUNC_BASIC0( mulpackm_add2_blk_var1 )


