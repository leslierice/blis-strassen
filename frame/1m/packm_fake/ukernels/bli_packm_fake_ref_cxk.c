/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"
#include <immintrin.h>
#define DISABLE


//#define REFC

#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname ) \
\
void PASTEMAC(ch,varname)( \
                           conj_t  conja, \
                           dim_t   n, \
                           void*   kappa, \
                           void*   a, inc_t inca, inc_t lda, \
                           void*   p,             inc_t ldp, \
                           dim_t   t_id, dim_t num_threads \
                         ) \
{ \
    ctype* restrict kappa_cast = kappa; \
    ctype* restrict acast = a; \
    ctype* restrict pcast = p; \
\
	if ( PASTEMAC(ch,eq1)( *kappa_cast ) ) \
	{ \
		if ( bli_is_conj( conja ) ) \
		{ \
            printf("NOT SUPPORTED\n"); \
		} \
		else \
		{ \
			for ( int index = t_id; index < n; index += num_threads ) \
			{ \
\
				ctype* alpha1 = &acast[index * lda]; \
				ctype* pi1    = &pcast[index * ldp]; \
\
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 0*inca), *(pi1 + 0) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 1*inca), *(pi1 + 1) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 2*inca), *(pi1 + 2) ); \
				PASTEMAC2(ch,ch,copys)( *(alpha1 + 3*inca), *(pi1 + 3) ); \
\
			} \
		} \
	} \
	else \
	{ \
            printf("NOT SUPPORTED\n"); \
	} \
}

GENTFUNC( float,    s, packm_fake_ref_4xk )
GENTFUNC( scomplex, c, packm_fake_ref_4xk )
GENTFUNC( dcomplex, z, packm_fake_ref_4xk )
    /* GENERATE some functions for float/scomplex/dcomple */



#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname ) \
\
void PASTEMAC(ch,varname)( \
                           conj_t  conja, \
                           dim_t   n, \
                           void*   kappa, \
                           void*   a, inc_t inca, inc_t lda, \
                           void*   p,             inc_t ldp, \
                           dim_t   t_id, dim_t num_threads \
                         ) \
{ \
    ctype* restrict kappa_cast = kappa; \
    ctype* restrict acast = a; \
    ctype* restrict pcast = p; \
\
	if ( PASTEMAC(ch,eq1)( *kappa_cast ) ) \
	{ \
		if ( bli_is_conj( conja ) ) \
		{ \
            printf("NOT SUPPORTED\n"); \
		} \
		else \
		{ \
			for ( int index = t_id; index < n; index += num_threads ) \
			{ \
				ctype* alpha1 = &acast[index * lda]; \
				ctype* pi1    = &pcast[index * ldp]; \
\
				PASTEMAC(ch,copys)( *(alpha1 + 0*inca), *(pi1 + 0) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 1*inca), *(pi1 + 1) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 2*inca), *(pi1 + 2) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 3*inca), *(pi1 + 3) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 4*inca), *(pi1 + 4) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 5*inca), *(pi1 + 5) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 6*inca), *(pi1 + 6) ); \
				PASTEMAC(ch,copys)( *(alpha1 + 7*inca), *(pi1 + 7) ); \
\
			} \
		} \
	} \
	else \
	{ \
        printf("NOT SUPPORTED\n"); \
	} \
}

GENTFUNC( float,    s, packm_fake_ref_8xk )
GENTFUNC( scomplex, c, packm_fake_ref_8xk )
GENTFUNC( dcomplex, z, packm_fake_ref_8xk )


//int is_aligned(void *p, int N)
//{
//    printf( "p: %#010x\n", (int)p );
//        return (int)p % N == 0;
//}


void bli_dpackm_fake_ref_4xk(
                           conj_t  conja,
                           dim_t   n,  
                           void*   kappa,
                           void*   a, inc_t inca, inc_t lda,
                           void*   p,             inc_t ldp,
                           dim_t   t_id, dim_t num_threads
                         )
{
    double* restrict kappa_cast = kappa;
    double* restrict acast = a;
    double* restrict pcast = p;


 
    if ( *kappa_cast == 1.0 )
    {   
        if ( bli_is_conj( conja ) ) 
        {   
            printf("packm_fake conj area not implemented\n"); \
        }   
        else 
        {   

            #ifdef REFC
            //double* alpha1 = (double *)acast;
            //double* pi1     = (double *)pcast;
            for ( int index = t_id; index < n; index += num_threads ) {
                double* restrict alpha1 = &acast[index * lda];
                double* restrict pi1     = &pcast[index * ldp];
                //#pragma unroll
                #pragma vector aligned nontemporal
                for ( int kk = 0; kk < 4; kk ++ ) {
                    pi1[kk] = alpha1[kk*inca];
                }
                //alpha1 += lda;
                //pi1    += ldp;
            }
            #else
            for ( int index = t_id; index < n; index += num_threads )
            {
				double* restrict alpha1 = &acast[index * lda];
				double* restrict pi1     = &pcast[index * ldp];

                __m256d av = _mm256_set_pd( alpha1[3*inca + 0*lda],  
                                            alpha1[2*inca + 0*lda],  
                                            alpha1[1*inca + 0*lda],  
                                            alpha1[0*inca + 0*lda]); 


                _mm256_store_pd( pi1 + 0 * ldp, av );

                //double* av_d = (double *)(&av);
                //printf( "av_d: %lf, %lf, %lf, %lf\n", av_d[0], av_d[1], av_d[2], av_d[3] );
            }
            #endif
        }
    }
    else
    {
        printf("packm_fake kappa != 1 not implemented\n");
    }

}



void bli_dpackm_fake_ref_8xk(
                           conj_t  conja,
                           dim_t   n,
                           void*   kappa,
                           void*   a,   inc_t inca, inc_t lda,
                           void*   p,               inc_t ldp,
                           dim_t   t_id, dim_t num_threads
                          )
{
    double* restrict kappa_cast = kappa;
    double* restrict acast = a;
    double* restrict pcast = p;

    if ( *kappa_cast == 1.0 )
    {
        if ( bli_is_conj( conja ) )
        {
            printf("packm_fake conj area not implemented\n"); \
        }
        else
        {

            #ifdef REFC
            //double* alpha1 = (double *)acast;
            //double* pi1     = (double *)pcast;
            for ( int index = t_id; index < n; index += num_threads ) {
                double* restrict alpha1 = &acast[index * lda];
                double* restrict pi1     = &pcast[index * ldp];
                //#pragma unroll
                #pragma vector aligned nontemporal
                for ( int kk = 0; kk < 8; kk ++ ) {
                    pi1[kk] = alpha1[kk*inca];
                }
                //alpha1 += lda;
                //pi1    += ldp;
            }
            #else
            for ( int index = t_id; index < n; index += num_threads )
            {
				double* alpha1 = &acast[index * lda];
				double* pi1    = &pcast[index * ldp];
            
                __m256d av1 = _mm256_set_pd( alpha1[3*inca + 0*lda],  
                                             alpha1[2*inca + 0*lda],  
                                             alpha1[1*inca + 0*lda],  
                                             alpha1[0*inca + 0*lda]); 

                __m256d av2 = _mm256_set_pd( alpha1[7*inca + 0*lda],  
                                             alpha1[6*inca + 0*lda],  
                                             alpha1[5*inca + 0*lda],  
                                             alpha1[4*inca + 0*lda]); 

                _mm256_store_pd( pi1 + 0 + 0 * ldp, av1 );
                _mm256_store_pd( pi1 + 4 + 0 * ldp, av2 );

            }
            #endif
        }
        
    }
    else
    { \
        printf("packm_fake kappa != 1 not implemented\n"); \
    }

}




