
#include "blis.h"

void bli_straprim_ref( obj_t*  alpha1, obj_t*  alpha2,
                       obj_t*  aA, obj_t*  gamma, obj_t*  aB,
                       obj_t*  bA, obj_t*  delta, obj_t*  bB,
                       obj_t*  beta1, obj_t*  beta2,
                       obj_t*  cA, obj_t*  cB ) {

	num_t dt = bli_obj_datatype( *cA );
    dim_t m  = bli_obj_length( *cA );
    dim_t n  = bli_obj_width( *cA );
    dim_t k  = bli_obj_width( *aA );

    obj_t a, b, c_tmp;
    bli_obj_create( dt, m, k, 0, 0, &a );
    bli_obj_create( dt, k, n, 0, 0, &b );
    bli_obj_create( dt, m, n, 0, 0, &c_tmp );

    //dtime = bli_clock();
    //bli_straprim_ref( &alpha1, &alpha2,
    //                  &aA, &gamma, aB, 
    //                  &bA, &delta, &bB, 
    //                  &beta1, &beta2,
    //                  &cA_strassen, &cB_strassen );

    //cA = beta1*cA+alpha1*(aA+gamma*aB)(bA+delta*bB)
    //cB = beta2*cB+alpha2*(aA+gamma*aB)(bA+delta*bB) 
    //a:=aA
    //a:=a+gamma*aB
    //b:=bA
    //b:=b+delta*bB
    //c_tmp := a * b
    //cA:=beta1*cA
    //cB:=beta2*cB
    //cA:=cA+alpha1*c_tmp
    //cB:=cB+alpha1*c_tmp

    bli_copym( aA, &a );

    if ( bli_obj_buffer( *aB ) != NULL ) {
        bli_axpym ( gamma, aB, &a );
    }

    bli_copym( bA, &b );

    if ( bli_obj_buffer( *bB ) != NULL ) {
        bli_axpym ( delta, bB, &b );
    }

    bli_gemm( &BLIS_ONE, &a, &b, &BLIS_ZERO, &c_tmp );

    //bli_printm( "cA", &cA, "%4.1f", "" );
    //bli_printm( "beta1", &beta1, "%4.1f", "" );

    //bli_scalm( beta1, cA );
    bli_axpym( alpha1, &c_tmp, cA );


    if ( bli_obj_buffer( *cB ) != NULL ) {
        //bli_scalm( beta2, cB );
        bli_axpym( alpha2, &c_tmp, cB );
    }
    //dtime_conventional = bli_clock_min_diff( dtime_conventional, dtime );

    bli_obj_free( &a );
    bli_obj_free( &b );
    bli_obj_free( &c_tmp );
}
