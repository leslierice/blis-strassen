
obj_t* obj_const_mul( obj_t* alpha, double b );


//void straprim_wrapper( double cur_alpha1, double cur_alpha2,
//                       double cur_gamma1, double cur_gamma2,
//                       double cur_delta1, double cur_delta2,
//                       int submat_id1, int submat_id2
//                     )
#define STRAPRIM_WRAPPER( CUR_ALPHA1, CUR_ALPHA2, \
                          CUR_GAMMA1, CUR_GAMMA2, \
                          CUR_DELTA1, CUR_DELTA2, \
                          A_SUBMAT_ID1, A_SUBMAT_ID2, \
                          B_SUBMAT_ID1, B_SUBMAT_ID2, \
                          C_SUBMAT_ID1, C_SUBMAT_ID2, \
                          STRAPRIM_FUNC \
                        ) \
{ \
    for ( dim_t i = 0; i < len; i++ ) { \
        /* alpha_next_list[ i ] = obj_const_mul( alpha_list[ i ], CUR_ALPHA1 ); */ \
        alpha_next_list[ 2 * i + 0 ] = obj_const_mul( alpha_list[ i ], CUR_ALPHA1 ); \
    } \
    for ( dim_t i = 0; i < len; i++ ) { \
        /* alpha_next_list[ len + i ] = obj_const_mul( alpha_list[ i ], CUR_ALPHA2 ); */ \
        alpha_next_list[ 2 * i + 1 ] = obj_const_mul( alpha_list[ i ], CUR_ALPHA2 ); \
    } \
    for ( dim_t i = 0; i < len; i++ ) { \
        gamma_next_list[ 2 * i + 0 ] = obj_const_mul( gamma_list[ i ], CUR_GAMMA1 ); \
    } \
    for ( dim_t i = 0; i < len; i++ ) { \
        gamma_next_list[ 2 * i + 1 ] = obj_const_mul( gamma_list[ i ], CUR_GAMMA2 ); \
    } \
    for ( dim_t i = 0; i < len; i++ ) { \
        delta_next_list[ 2 * i + 0 ] = obj_const_mul( delta_list[ i ], CUR_DELTA1 ); \
    } \
    for ( dim_t i = 0; i < len; i++ ) { \
        delta_next_list[ 2 * i + 1 ] = obj_const_mul( delta_list[ i ], CUR_DELTA2 ); \
    } \
    for ( dim_t i = 0; i < 2*len; i++ ) { \
        beta_next_list[ i ] = &BLIS_ONE; \
    } \
    for ( dim_t i = 0; i < len; i++ ) { \
        if ( (A_SUBMAT_ID1) == 0 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART00, a_list[i], temp); \
            a_next_list[ 2 * i + 0 ] = temp; \
        } else if ( (A_SUBMAT_ID1) == 1 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART01, a_list[i], temp); \
            a_next_list[ 2 * i + 0 ] = temp; \
        } else if ( (A_SUBMAT_ID1) == 2 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART10, a_list[i], temp); \
            a_next_list[ 2 * i + 0 ] = temp; \
        } else if ( (A_SUBMAT_ID1) == 3 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART11, a_list[i], temp); \
            a_next_list[ 2 * i + 0 ] = temp; \
        } else { /*if ( (A_SUBMAT_ID1) == -1 ) */ \
            a_next_list[ 2 * i + 0 ] = &a_null; \
        } \
        if ( (A_SUBMAT_ID2) == 0 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART00, a_list[i], temp); \
            a_next_list[ 2 * i + 1 ] = temp; \
        } else if ( (A_SUBMAT_ID2) == 1 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART01, a_list[i], temp); \
            a_next_list[ 2 * i + 1 ] = temp; \
        } else if ( (A_SUBMAT_ID2) == 2 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART10, a_list[i], temp); \
            a_next_list[ 2 * i + 1 ] = temp; \
        } else if ( (A_SUBMAT_ID2) == 3 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART11, a_list[i], temp); \
            a_next_list[ 2 * i + 1 ] = temp; \
        } else { /*if ( (A_SUBMAT_ID2) == -1 ) */ \
            a_next_list[ 2 * i + 1 ] = &a_null; \
        } \
    } \
    for ( dim_t i = 0; i < len; i++ ) { \
        if ( (B_SUBMAT_ID1) == 0 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART00, b_list[i], temp); \
            b_next_list[ 2 * i + 0 ] = temp; \
        } else if ( (B_SUBMAT_ID1) == 1 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART01, b_list[i], temp); \
            b_next_list[ 2 * i + 0 ] = temp; \
        } else if ( (B_SUBMAT_ID1) == 2 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART10, b_list[i], temp); \
            b_next_list[ 2 * i + 0 ] = temp; \
        } else if ( (B_SUBMAT_ID1) == 3 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART11, b_list[i], temp); \
            b_next_list[ 2 * i + 0 ] = temp; \
        } else { /*if ( (B_SUBMAT_ID1) == -1 ) */ \
            b_next_list[ 2 * i + 0 ] = &b_null; \
        } \
        if ( (B_SUBMAT_ID2) == 0 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART00, b_list[i], temp); \
            b_next_list[ 2 * i + 1 ] = temp; \
        } else if ( (B_SUBMAT_ID2) == 1 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART01, b_list[i], temp); \
            b_next_list[ 2 * i + 1 ] = temp; \
        } else if ( (B_SUBMAT_ID2) == 2 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART10, b_list[i], temp); \
            b_next_list[ 2 * i + 1 ] = temp; \
        } else if ( (B_SUBMAT_ID2) == 3 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART11, b_list[i], temp); \
            b_next_list[ 2 * i + 1 ] = temp; \
        } else { /*if ( (B_SUBMAT_ID2) == -1 ) */ \
            b_next_list[ 2 * i + 1 ] = &b_null; \
        } \
    } \
    /*for ( dim_t i = 0; i < len; i++ ) { \
        if ( (C_SUBMAT_ID1) == 0 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART00, c_list[i], temp); \
            c_next_list[ 0 * len + i ] = temp; \
        } else if ( (C_SUBMAT_ID1) == 1 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART01, c_list[i], temp); \
            c_next_list[ 0 * len + i ] = temp; \
        } else if ( (C_SUBMAT_ID1) == 2 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART10, c_list[i], temp); \
            c_next_list[ 0 * len + i ] = temp; \
        } else if ( (C_SUBMAT_ID1) == 3 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART11, c_list[i], temp); \
            c_next_list[ 0 * len + i ] = temp; \
        } else {  \
            c_next_list[ 0 * len + i ] = &c_null; \
        } \
        if ( (C_SUBMAT_ID2) == 0 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART00, c_list[i], temp); \
            c_next_list[ 1 * len + i ] = temp; \
        } else if ( (C_SUBMAT_ID2) == 1 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART01, c_list[i], temp); \
            c_next_list[ 1 * len + i ] = temp; \
        } else if ( (C_SUBMAT_ID2) == 2 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART10, c_list[i], temp); \
            c_next_list[ 1 * len + i ] = temp; \
        } else if ( (C_SUBMAT_ID2) == 3 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART11, c_list[i], temp); \
            c_next_list[ 1 * len + i ] = temp; \
        } else {  \
            c_next_list[ 1 * len + i ] = &c_null; \
        } \
    } */ \
    for ( dim_t i = 0; i < len; i++ ) { \
        if ( (C_SUBMAT_ID1) == 0 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART00, c_list[i], temp); \
            c_next_list[ 2 * i + 0 ] = temp; \
        } else if ( (C_SUBMAT_ID1) == 1 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART01, c_list[i], temp); \
            c_next_list[ 2 * i + 0 ] = temp; \
        } else if ( (C_SUBMAT_ID1) == 2 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART10, c_list[i], temp); \
            c_next_list[ 2 * i + 0 ] = temp; \
        } else if ( (C_SUBMAT_ID1) == 3 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART11, c_list[i], temp); \
            c_next_list[ 2 * i + 0 ] = temp; \
        } else { /*if ( (C_SUBMAT_ID1) == -1 ) */ \
            c_next_list[ 2 * i + 0 ] = &c_null; \
        } \
        if ( (C_SUBMAT_ID2) == 0 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART00, c_list[i], temp); \
            c_next_list[ 2 * i + 1 ] = temp; \
        } else if ( (C_SUBMAT_ID2) == 1 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART01, c_list[i], temp); \
            c_next_list[ 2 * i + 1 ] = temp; \
        } else if ( (C_SUBMAT_ID2) == 2 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART10, c_list[i], temp); \
            c_next_list[ 2 * i + 1 ] = temp; \
        } else if ( (C_SUBMAT_ID2) == 3 ) { \
            obj_t *temp = (obj_t *)malloc(sizeof(obj_t)); \
            bli_acquire_mpart_half( BLIS_SUBPART11, c_list[i], temp); \
            c_next_list[ 2 * i + 1 ] = temp; \
        } else { /*if ( (C_SUBMAT_ID2) == -1 ) */ \
            c_next_list[ 2 * i + 1 ] = &c_null; \
        } \
    } \
    STRAPRIM_FUNC( next_len, \
                  alpha_next_list, \
                  a_next_list, gamma_next_list, \
                  b_next_list,     delta_next_list, \
                  beta_next_list, \
                  c_next_list); \
}


#define STRAPRIM_WRAPPER_2( CUR_ALPHA1, CUR_ALPHA2, \
                            CUR_GAMMA1, CUR_GAMMA2, \
                            CUR_DELTA1, CUR_DELTA2, \
                            CUR_BETA1,  CUR_BETA2, \
                            A_SUBMAT1, A_SUBMAT2, \
                            B_SUBMAT1, B_SUBMAT2, \
                            C_SUBMAT1, C_SUBMAT2, \
                            STRAPRIM_FUNC \
                        ) \
{ \
    alpha_list[ 0 ] = &(CUR_ALPHA1); \
    alpha_list[ 1 ] = &(CUR_ALPHA2); \
    gamma_list[ 0 ] = &(CUR_GAMMA1); \
    gamma_list[ 1 ] = &(CUR_GAMMA2); \
    delta_list[ 0 ] = &(CUR_DELTA1); \
    delta_list[ 1 ] = &(CUR_DELTA2); \
    beta_list[ 0 ] = &(CUR_BETA1); \
    beta_list[ 1 ] = &(CUR_BETA2); \
\
    a_list[ 0 ] = &(A_SUBMAT1); \
    a_list[ 1 ] = &(A_SUBMAT2); \
    b_list[ 0 ] = &(B_SUBMAT1); \
    b_list[ 1 ] = &(B_SUBMAT2); \
    c_list[ 0 ] = &(C_SUBMAT1); \
    c_list[ 1 ] = &(C_SUBMAT2); \
    STRAPRIM_FUNC( 2, \
                  alpha_list, \
                  a_list, gamma_list, \
                  b_list,     delta_list, \
                  beta_list, \
                  c_list); \
}


