
        printf( "C(%lu, %lu) += %lf * ( A(%lu, %lu) ",
                bli_obj_row_off( *c_list[ 0 ] )/bli_obj_length( *c_list[ 0 ] ), 
                bli_obj_col_off( *c_list[ 0 ] )/bli_obj_width( *c_list[ 0 ] ), 
                *(double *)bli_obj_buffer_for_const( BLIS_DOUBLE, *alpha_list[ 0 ] ),
                bli_obj_row_off( *a_list[ 0 ] )/bli_obj_length( *a_list[ 0 ] ), 
                bli_obj_col_off( *a_list[ 0 ] )/bli_obj_width( *a_list[ 0 ] )

              );

        for ( dim_t i = 1; i < len; i++ ) {
            if( bli_obj_buffer( *a_list[ i ] ) != NULL ) {
                if ( *(double *)bli_obj_buffer_for_const( BLIS_DOUBLE, *gamma_list[ i ] ) == 1.0 ) {
                    printf( "+ A(%lu, %lu) ",
                        bli_obj_row_off( *a_list[ i ] )/bli_obj_length( *a_list[ i ] ), 
                        bli_obj_col_off( *a_list[ i ] )/bli_obj_width( *a_list[ i ] )
                      );
                }
                else if ( *(double *)bli_obj_buffer_for_const( BLIS_DOUBLE, *gamma_list[ i ] ) == -1.0 ) {
                    printf( "- A(%lu, %lu) ",
                        bli_obj_row_off( *a_list[ i ] )/bli_obj_length( *a_list[ i ] ), 
                        bli_obj_col_off( *a_list[ i ] )/bli_obj_width( *a_list[ i ] )
                      );
                }
                else if ( *(double *)bli_obj_buffer_for_const( BLIS_DOUBLE, *gamma_list[ i ] ) == 0.0 ) {
                }
                else {
                    exit( 0 );
                }
            }
        }

        printf( " ) * ( B(%lu, %lu) ",
                bli_obj_row_off( *b_list[ 0 ] )/bli_obj_length( *b_list[ 0 ] ), 
                bli_obj_col_off( *b_list[ 0 ] )/bli_obj_width( *b_list[ 0 ] )
              );
        for ( dim_t i = 1; i < len; i++ ) {
            if( bli_obj_buffer( *b_list[ i ] ) != NULL ) {
                //printf( "i: %lu\t", i );
                if ( *(double *)bli_obj_buffer_for_const( BLIS_DOUBLE, *delta_list[ i ] ) == 1.0 ) {
                    printf( "+ B(%lu, %lu) ",
                            bli_obj_row_off( *b_list[ i ] )/bli_obj_length( *b_list[ i ] ), 
                            bli_obj_col_off( *b_list[ i ] )/bli_obj_width( *b_list[ i ] )
                          );
                }
                else if ( *(double *)bli_obj_buffer_for_const( BLIS_DOUBLE, *delta_list[ i ] ) == -1.0 ) {
                    printf( "- B(%lu, %lu) ",
                            bli_obj_row_off( *b_list[ i ] )/bli_obj_length( *b_list[ i ] ), 
                            bli_obj_col_off( *b_list[ i ] )/bli_obj_width( *b_list[ i ] )
                          );
                }
                else if ( *(double *)bli_obj_buffer_for_const( BLIS_DOUBLE, *delta_list[ i ] ) == 0.0 ) {
                }
                else {
                    exit( 0 );
                }
            }
        }
        printf( " )\n" );

        for ( dim_t i = 1; i < len; i++ ) {
            if( bli_obj_buffer( *c_list[ i ] ) != NULL ) {
                //printf( "i: %lu\n", i );
                //char mat_name[80], num[80]="\0";
                //strcpy( mat_name, "c_list[ " );
                ////itoa( (int)i, num, 10 );
                //strcat( mat_name, num );
                //strcat( mat_name, " ]" );
                //bli_printm( mat_name, c_list[ i ], "%4.1f", "" );
                ////printf( "m: %lu, n:%lu, offm: %lu, offn: %lu\n", bli_obj_length( *c_list[ i ] ),
                ////        bli_obj_width( *c_list[ i ] ), bli_obj_row_off( *c_list[ i ] ),
                ////        bli_obj_col_off( *c_list[ i ] ) );
                //bli_printm( "alpha[ ]", alpha_list[ i ], "%4.1f", "" );

                printf( "C(%lu, %lu) += %lf * ( A(%lu, %lu) ",
                        bli_obj_row_off( *c_list[ i ] )/bli_obj_length( *c_list[ i ] ), 
                        bli_obj_col_off( *c_list[ i ] )/bli_obj_width( *c_list[ i ] ), 
                        *(double *)bli_obj_buffer_for_const( BLIS_DOUBLE, *alpha_list[ i ] ),
                        bli_obj_row_off( *a_list[ 0 ] )/bli_obj_length( *a_list[ 0 ] ), 
                        bli_obj_col_off( *a_list[ 0 ] )/bli_obj_width( *a_list[ 0 ] )

                      );

                for ( dim_t i = 1; i < len; i++ ) {
                    if( bli_obj_buffer( *a_list[ i ] ) != NULL ) {
                        printf( "+ A(%lu, %lu) ",
                                bli_obj_row_off( *a_list[ i ] )/bli_obj_length( *a_list[ i ] ), 
                                bli_obj_col_off( *a_list[ i ] )/bli_obj_width( *a_list[ i ] )
                              );
                    }
                }

                printf( " ) * ( B(%lu, %lu) ",
                        bli_obj_row_off( *b_list[ 0 ] )/bli_obj_length( *b_list[ 0 ] ), 
                        bli_obj_col_off( *b_list[ 0 ] )/bli_obj_width( *b_list[ 0 ] )
                      );
                for ( dim_t i = 1; i < len; i++ ) {
                    if( bli_obj_buffer( *b_list[ i ] ) != NULL ) {
                        printf( "+ B(%lu, %lu) ",
                                bli_obj_row_off( *b_list[ i ] )/bli_obj_length( *b_list[ i ] ), 
                                bli_obj_col_off( *b_list[ i ] )/bli_obj_width( *b_list[ i ] )
                              );
                    }
                }
                printf( " )\n" );



            }
        }
        printf( "\n" );



