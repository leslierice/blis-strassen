
#include "blis.h"

#include "assert.h"

void bli_stra_1level( obj_t* a, obj_t* b, obj_t* c )
{
    //printf( "Enter bli_stra_1level\n" ); fflush(stdout);
    dim_t m = bli_obj_length( *c );
    dim_t n = bli_obj_width( *c );
    dim_t k = bli_obj_width( *a );

    //Pretty sure we can handle the following cases,
    //but for now, let's make sure we're working with square even dimensioned matrices.
    assert( m % 2 == 0);
    assert( n % 2 == 0);
    assert( k % 2 == 0);
    //assert( m == n);
    //assert( m == k);

    //Partition
    obj_t a00, a01;
    obj_t a10, a11;
    bli_acquire_mpart_half( BLIS_SUBPART00, a, &a00);
    bli_acquire_mpart_half( BLIS_SUBPART01, a, &a01);
    bli_acquire_mpart_half( BLIS_SUBPART10, a, &a10);
    bli_acquire_mpart_half( BLIS_SUBPART11, a, &a11);

    obj_t b00, b01;
    obj_t b10, b11;
    bli_acquire_mpart_half( BLIS_SUBPART00, b, &b00);
    bli_acquire_mpart_half( BLIS_SUBPART01, b, &b01);
    bli_acquire_mpart_half( BLIS_SUBPART10, b, &b10);
    bli_acquire_mpart_half( BLIS_SUBPART11, b, &b11);

    obj_t c00, c01;
    obj_t c10, c11;
    bli_acquire_mpart_half( BLIS_SUBPART00, c, &c00);
    bli_acquire_mpart_half( BLIS_SUBPART01, c, &c01);
    bli_acquire_mpart_half( BLIS_SUBPART10, c, &c10);
    bli_acquire_mpart_half( BLIS_SUBPART11, c, &c11);

    obj_t a_null, b_null, c_null, scalar_null;
    bli_obj_create_without_buffer( bli_obj_datatype( a00 ), bli_obj_length( a00 ), bli_obj_width( a00 ), &a_null );
    bli_obj_create_without_buffer( bli_obj_datatype( b00 ), bli_obj_length( b00 ), bli_obj_width( b00 ), &b_null );
    bli_obj_create_without_buffer( bli_obj_datatype( c00 ), bli_obj_length( c00 ), bli_obj_width( c00 ), &c_null );
    //bli_obj_create_without_buffer( bli_obj_datatype( c00 ), 1, 1, scalar_null );
    scalar_null = BLIS_ZERO;


    ////num_t dt = bli_obj_datatype( c00 );
    //num_t dt = BLIS_DOUBLE;
    //obj_t c00_ref, c11_ref;
    //bli_obj_create( dt, m/2, n/2, 0, 0, &c00_ref );
    //bli_obj_create( dt, m/2, n/2, 0, 0, &c11_ref );
    //bli_copym( &c00, &c00_ref );
    //bli_copym( &c11, &c11_ref );


    //Now do the 7 straprim thingers

    //printf("-------Enter straprim\n");

    // M1
    // c00 = 0*c00+1*(a00+a11)(b00+b11)
    // c11 = 0*c11+1*(a00+a11)(b00+b11)
    bli_straprim( &BLIS_ONE, &BLIS_ONE,
                  &a00, &BLIS_ONE, &a11,
                  &b00, &BLIS_ONE, &b11,
                  &BLIS_ONE, &BLIS_ONE,
                  &c00, &c11 );
    //bli_straprim( &BLIS_ONE, &BLIS_ONE,
    //              &a00, &BLIS_ONE, &a11,
    //              &b00, &BLIS_ONE, &b11,
    //              &BLIS_ZERO, &BLIS_ZERO,
    //              &c00, &c11 );


    //printf("-------M1\n");
    //bli_printm( "c00", &c00, "%4.6lf", "" );
    //bli_printm( "c", c, "%4.6lf", "" );
    //bli_printm( "c11", &c11, "%4.1f", "" );

    //exit( 0 );

    //bli_straprim_ref( &BLIS_ONE, &BLIS_ONE,
    //                  &a00, &BLIS_ONE, &a11,
    //                  &b00, &BLIS_ONE, &b11,
    //                  &BLIS_ZERO, &BLIS_ZERO,
    //                  &c00_ref, &c11_ref );

    //bli_printm( "c00_ref", &c00_ref, "%4.6lf", "" );
    //bli_printm( "c11_ref", &c11_ref, "%4.1f", "" );

    //inc_t ldc      = bli_obj_col_stride( c00 );
    //inc_t ldc_ref  = bli_obj_col_stride( c00_ref );
    //computeError( ldc, ldc_ref, m/2, n/2, bli_obj_buffer( c00 ), bli_obj_buffer( c00_ref ) );


    // M2
    // c10 = 0*c10+1*(a10+a11)b00
    // c11 = 1*c11-1*(a10+a11)b00
    //bli_straprim( &BLIS_ONE, &BLIS_MINUS_ONE,
    //              &a10, &BLIS_ONE, &a11,
    //              &b00, &scalar_null, &b_null,
    //              &BLIS_ZERO, &BLIS_ONE,
    //              &c10, &c11 );
    bli_straprim( &BLIS_ONE, &BLIS_MINUS_ONE,
                  &a10, &BLIS_ONE, &a11,
                  &b00, &scalar_null, &b_null,
                  &BLIS_ONE, &BLIS_ONE,
                  &c10, &c11 );

    //printf("-------M2\n");
    //bli_printm( "c", c, "%4.6lf", "" );
    //exit( 0 );

    // M3
    // c01 = 0*c01+1*a00(b01-b11)
    // c11 = 1*c11+1*a00(b01-b11)
    //bli_straprim( &BLIS_ONE, &BLIS_ONE,
    //              &a00, &scalar_null, &a_null,
    //              &b01, &BLIS_MINUS_ONE, &b11,
    //              &BLIS_ZERO, &BLIS_ONE,
    //              &c01, &c11 );
    bli_straprim( &BLIS_ONE, &BLIS_ONE,
                  &a00, &BLIS_ZERO, &a00,
                  &b01, &BLIS_MINUS_ONE, &b11,
                  &BLIS_ONE, &BLIS_ONE,
                  &c01, &c11 );

    //printf("-------M3\n");
    //bli_printm( "c", c, "%4.6lf", "" );



    // M4
    // c00 = 1*c00+1*a11(b10-b00)
    // c10 = 1*c10+1*a11(b10-b00)
    bli_straprim( &BLIS_ONE, &BLIS_ONE,
                  &a11, &BLIS_ZERO, &a00,
                  &b10, &BLIS_MINUS_ONE, &b00,
                  &BLIS_ONE, &BLIS_ONE,
                  &c00, &c10 );

    //printf("-------M4\n");
    //bli_printm( "c", c, "%4.6lf", "" );




    // M5
    // c00 = 1*c00-1*(a00+a01)b11
    // c01 = 1*c01+1*(a00+a01)b11
    bli_straprim( &BLIS_MINUS_ONE, &BLIS_ONE,
                  &a00, &BLIS_ONE, &a01,
                  &b11, &scalar_null, &b_null,
                  &BLIS_ONE, &BLIS_ONE,
                  &c00, &c01 );

    //printf("-------M5\n");
    //bli_printm( "c", c, "%4.6lf", "" );



    // M6
    // c11 = 1*c11+(a10-a00)(b00+b01)
    bli_straprim( &BLIS_ONE, &scalar_null,
                  &a10, &BLIS_MINUS_ONE, &a00,
                  &b00, &BLIS_ONE, &b01,
                  &BLIS_ONE, &scalar_null,
                  &c11, &c_null );                     //if (c_null->is_dummy==1) do nothing for cB

    //printf("-------M6\n");
    //bli_printm( "c", c, "%4.6lf", "" );




    // M7
    // c00 = 1*c00+(a01-a11)(b10+b11)
    bli_straprim( &BLIS_ONE, &scalar_null,
                  &a01, &BLIS_MINUS_ONE, &a11,
                  &b10, &BLIS_ONE, &b11,
                  &BLIS_ONE, &scalar_null,
                  &c00, &c_null );

    //printf("-------M7\n");
    //bli_printm( "c", c, "%4.6lf", "" );



    bli_obj_free( &a_null );
    bli_obj_free( &b_null );
    bli_obj_free( &c_null );
}
