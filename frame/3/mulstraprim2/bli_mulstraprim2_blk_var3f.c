/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"

void bli_mulstraprim2_blk_var3f( dim_t   len,
                                obj_t**  alpha_list, 
                                obj_t**  a_list, obj_t**  gamma_list,
                                obj_t**  b_list, obj_t**  delta_list,
                                obj_t**  beta_list, 
                                obj_t**  c_list, 
                                gemm_t* cntl,
                                gemm_thrinfo_t* thread )
{
    obj_t  c_pack_s[STRA_LIST_LEN];
    obj_t  a1_pack_s[STRA_LIST_LEN], b1_pack_s[STRA_LIST_LEN];

    obj_t  a1[STRA_LIST_LEN], b1[STRA_LIST_LEN];
    //obj_t* a1A_pack = NULL, *a1B_pack = NULL;
    //obj_t* b1A_pack = NULL, *b1B_pack = NULL;
    //obj_t* cA_pack = NULL,  *cB_pack = NULL;
    obj_t* a1_pack[STRA_LIST_LEN] = {NULL};
    obj_t* b1_pack[STRA_LIST_LEN] = {NULL};
    obj_t* c_pack[STRA_LIST_LEN]  = {NULL};


	dim_t  i;
	dim_t  b_alg;
	dim_t  k_trans;

    if( thread_am_ochief( thread ) ) {
        // Initialize object for packing C
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_obj_init_pack( &c_pack_s[ ii ] );
            bli_packm_init( c_list[ ii ], &c_pack_s[ ii ],
                    cntl_sub_packm_c( cntl ) );

        }

        // Scale C by beta (if instructed).
        bli_scalm_int( &BLIS_ONE,
                c_list[ 0 ],
                cntl_sub_scalm( cntl ) );

        #pragma unroll
        for ( dim_t ii = 1; ii < STRA_LIST_LEN; ii++ ) {
            if ( bli_obj_buffer( *c_list[ ii ] ) != NULL ) {
                bli_scalm_int( &BLIS_ONE,
                        c_list[ ii ],
                        cntl_sub_scalm( cntl ) );
            }
        }
    }

    #pragma unroll
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
        c_pack[ ii ] = thread_obroadcast( thread, &c_pack_s[ ii ] );
    }

    // Initialize pack objects for A and B that are passed into packm_init().
    if( thread_am_ichief( thread ) ){
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_obj_init_pack( &a1_pack_s[ ii ] );
            bli_obj_init_pack( &b1_pack_s[ ii ] );
        }
    }
    #pragma unroll
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
        a1_pack[ ii ] = thread_ibroadcast( thread, &a1_pack_s[ ii ] );
        b1_pack[ ii ] = thread_ibroadcast( thread, &b1_pack_s[ ii ] );
    }

	// Pack C (if instructed).
    #pragma unroll
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
        bli_packm_int( c_list[ ii ], c_pack[ ii ],
	               cntl_sub_packm_c( cntl ),
                   gemm_thread_sub_opackm( thread ) );
    }

	// Query dimension in partitioning direction.
	k_trans = bli_obj_width_after_trans( *a_list[ 0 ] );

	// Partition along the k dimension.
	for ( i = 0; i < k_trans; i += b_alg )
	{
		// Determine the current algorithmic blocksize.
		// NOTE: We call a gemm/hemm/symm-specific function to determine
		// the kc blocksize so that we can implement the "nudging" of kc
		// to be a multiple of mr or nr, as needed.
		b_alg = bli_gemm_determine_kc_f( i, k_trans, a_list[ 0 ], b_list[ 0 ],
		                                 cntl_blocksize( cntl ) );

		// Acquire partitions for A1 and B1.
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_acquire_mpart_l2r( BLIS_SUBPART1,
                    i, b_alg, a_list[ ii ], &a1[ ii ] );
            bli_acquire_mpart_t2b( BLIS_SUBPART1,
                    i, b_alg, b_list[ ii ], &b1[ ii ] );
        }

		// Initialize objects for packing A1 and B1.
        if( thread_am_ichief( thread ) ) {
            #pragma unroll
            for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
                bli_packm_init( &a1[ ii ], a1_pack[ ii ],
                        cntl_sub_packm_a( cntl ) );
                bli_packm_init( &b1[ ii ], b1_pack[ ii ],
                        cntl_sub_packm_b( cntl ) );
            }
        }
        thread_ibarrier( thread );

		// Pack A1 (if instructed).
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_packm_int( &a1[ ii ], a1_pack[ ii ],
                    cntl_sub_packm_a( cntl ),
                    gemm_thread_sub_ipackm( thread ) );
        }

		// Pack B1 (if instructed).
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_packm_int( &b1[ ii ], b1_pack[ ii ],
		               cntl_sub_packm_b( cntl ),
                       gemm_thread_sub_ipackm( thread ) );
        }

		// Perform mulstraprim2 subproblem.
        bli_mulstraprim2_int( len,
                             alpha_list,
                             a1_pack, gamma_list,
                             b1_pack, delta_list,
                             beta_list,
                             c_pack,
                             cntl_sub_gemm( cntl ),
                             gemm_thread_sub_gemm( thread ) );
		
		// This variant executes multiple rank-k updates. Therefore, if the
		// internal beta scalar on matrix C is non-zero, we must use it
		// only for the first iteration (and then BLIS_ONE for all others).
		// And since c_pack is a local obj_t, we can simply overwrite the
		// internal beta scalar with BLIS_ONE once it has been used in the
		// first iteration.
        thread_ibarrier( thread );
		if ( i == 0 && thread_am_ichief( thread ) ) {
            #pragma unroll
            for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
                bli_obj_scalar_reset( c_pack[ ii ] );
            }
        }

	}

    thread_obarrier( thread );

	// Unpack C (if C was packed).
    #pragma unroll
    for ( dim_t i = 0; i < STRA_LIST_LEN; i++ ) {
        bli_unpackm_int( c_pack[ i ], c_list[ i ],
                cntl_sub_unpackm_c( cntl ),
                gemm_thread_sub_opackm( thread ) );
    }


	// If any packing buffers were acquired within packm, release them back
	// to the memory manager.
    if( thread_am_ochief( thread ) ) {
        #pragma unroll
        for ( dim_t i = 0; i < STRA_LIST_LEN; i++ ) {
            bli_packm_release( c_pack[ i ], cntl_sub_packm_c( cntl ) );
        }
    }
    if( thread_am_ichief( thread ) ){
        #pragma unroll
        for ( dim_t i = 0; i < STRA_LIST_LEN; i++ ) {
            bli_packm_release( a1_pack[ i ], cntl_sub_packm_a( cntl ) );
            bli_packm_release( b1_pack[ i ], cntl_sub_packm_b( cntl ) );
        }

    }
}

