/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"

void bli_mulstraprim2_blk_var1f( dim_t   len,
                                obj_t**  alpha_list, 
                                obj_t**  a_list, obj_t**  gamma_list,
                                obj_t**  b_list, obj_t**  delta_list,
                                obj_t**  beta_list, 
                                obj_t**  c_list,
                                gemm_t* cntl,
                                gemm_thrinfo_t* thread
                                //extraloop_t* loop
                           )
{

    //err_t e_val;
    
    //The s is for "lives on the stack"
    obj_t b_pack_s;
    obj_t a1_pack_s, c1_pack_s[STRA_LIST_LEN];

    obj_t a1[STRA_LIST_LEN], c1[STRA_LIST_LEN];
    obj_t* a1_pack  = NULL;
    obj_t* b_pack   = NULL;
    //obj_t* c1A_pack = NULL;
    //obj_t* c1B_pack = NULL;
    obj_t* c1_pack[STRA_LIST_LEN] = {NULL};

	dim_t i;
	dim_t b_alg;

    //obj_t bb;
    //obj_t *b = &bb;
    //bli_obj_create( bli_obj_datatype( *bA ), bli_obj_length( *bA ), bli_obj_width( *bA ), 0, 0, b );
    ////bA+delta*bB -> b -> b_pack
    //bli_copym( bA, b );

    ////e_val = bli_check_matrix_object( bB );
    ////if ( e_val != BLIS_EXPECTED_NONNULL_OBJECT_BUFFER )
    //if ( bli_obj_buffer( *bB ) != NULL ) {
    //    bli_axpym( delta, bB, b);
    //}
    ////if ( bB->is_dummy != 1 ) {
    ////    bli_axpym( delta, bB, b);
    ////}

    if( thread_am_ochief( thread ) ) {
	    // Initialize object for packing B.
	    bli_obj_init_pack( &b_pack_s );
	    bli_packm_init( b_list[ 0 ], &b_pack_s,
	                    cntl_sub_packm_b( cntl ) );

        // Scale C by beta (if instructed).
        // Since scalm doesn't support multithreading yet, must be done by chief thread (ew)
        bli_scalm_int( &BLIS_ONE,
                       c_list[ 0 ],
                       cntl_sub_scalm( cntl ) );
         
        #pragma unroll
        for ( dim_t ii = 1; ii < STRA_LIST_LEN; ii++ ) {
            if ( bli_obj_buffer( *c_list[ ii ] ) != NULL ) {
                bli_scalm_int( &BLIS_ONE,
                        c_list[ ii ],
                        cntl_sub_scalm( cntl ) );
            }
        }

    }
    b_pack = thread_obroadcast( thread, &b_pack_s );


	// Initialize objects passed into bli_packm_init for A and C
    if( thread_am_ichief( thread ) ) {
        bli_obj_init_pack( &a1_pack_s );

        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_obj_init_pack( &c1_pack_s[ ii ] );
        }
    }
    a1_pack = thread_ibroadcast( thread, &a1_pack_s );
    #pragma unroll
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
        c1_pack[ ii ] = thread_ibroadcast( thread, &c1_pack_s[ ii ] );
    }

    //
	// Pack B (if instructed).
	//bli_packm_int( b, b_pack,
	//               cntl_sub_packm_b( cntl ),
    //               gemm_thread_sub_opackm( thread ) );

    /////////////////////////////////////////////////////////////////////////
    //if ( bli_obj_buffer( *bB ) == NULL ) {
    //    bli_packm_int( bA, b_pack,
    //                   cntl_sub_packm_b( cntl ),
    //                   gemm_thread_sub_opackm( thread ) );
    //} else {
    //    bli_packm_add2_int( bA, delta, bB, b_pack,
    //                       cntl_sub_packm_b( cntl ),
    //                       gemm_thread_sub_opackm( thread ) );
    //}


    //bli_printm( "b_list[0]", b_list[0], "%4.6lf", "" );
    //if ( bli_obj_buffer( *b_list[ 1 ] ) != NULL ) {
    //    bli_printm( "b_list[1]", b_list[1], "%4.6lf", "" );
    //}

    //#pragma unroll
    //for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
    //    printf( "ii:%d\n", ii );
    //    if ( bli_obj_buffer( *b_list[ ii ] ) != NULL ) {
    //        bli_printm( "b_list", b_list[ ii ], "%4.1lf", "" );
    //    }
    //}

    //b is obj_t * , or obj_t array...
    //b_pack = b_list .* delta_list;
    bli_mulpackm_add2_int( len, b_list, delta_list, b_pack,
                           cntl_sub_packm_b( cntl ),
                           gemm_thread_sub_opackm( thread ) );

    //bli_printm( "b_pack", b_pack, "%4.6lf", "" );

    dim_t my_start, my_end;
    // Get range from aA (no need to get again from aB)
    bli_get_range_t2b( thread, a_list[ 0 ],
                       bli_blksz_get_mult_for_obj( a_list[ 0 ], cntl_blocksize( cntl ) ),
                       &my_start, &my_end );

	// Partition along the m dimension.
	for ( i = my_start; i < my_end; i += b_alg )
	{
		// Determine the current algorithmic blocksize.
		// NOTE: Use of a (for execution datatype) is intentional!
		// This causes the right blocksize to be used if c and a are
		// complex and b is real.
		b_alg = bli_determine_blocksize_f( i, my_end, a_list[ 0 ],
		                                   cntl_blocksize( cntl ) );

		// Acquire partitions for a1A, a1B and c1A, c1B.
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_acquire_mpart_t2b( BLIS_SUBPART1,
                    i, b_alg, a_list[ ii ], &a1[ ii ] );
            bli_acquire_mpart_t2b( BLIS_SUBPART1,
                    i, b_alg, c_list[ ii ], &c1[ ii ] );
        }
		
        // Initialize objects for packing a1A, a1B and c1A, c1B.
        if( thread_am_ichief( thread ) ) {
            //NEED to change this routine later to one routine with the following footprint:
            //bli_packm_axpy_init( &a1A, gamma, &a1B a1_pack,
            //                cntl_sub_packm_a( cntl ) );
            //a1A+gamma*a1B -> a1_pack

            bli_packm_init( &a1[ 0 ], a1_pack,
                    cntl_sub_packm_a( cntl ) );


            // Judging whether c1[ 1 ], c1[ 2 ] is NULL or not???? Not executed actually....
            #pragma unroll
            for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
                //if ( bli_obj_buffer( c1[ i ] ) != NULL ) {
                    bli_packm_init( &c1[ ii ], c1_pack[ ii ],
                            cntl_sub_packm_c( cntl ) );
                //}
            }

        }
        thread_ibarrier( thread );

        ////a1A+gamma*a1B -> a1_pack
		//// Pack A1 (if instructed).
		//bli_packm_int( &a1, a1_pack,
		//               cntl_sub_packm_a( cntl ),
        //               gemm_thread_sub_ipackm( thread ) );

        //printf( "Before packm_add\n" );

        ////////////////////////////////////////////////////////////////
        //if ( bli_obj_buffer( a1B ) == NULL ) {
        //    bli_packm_int( &a1A, a1_pack,
        //            cntl_sub_packm_a( cntl ),
        //            gemm_thread_sub_ipackm( thread ) );
        //} else {
        //    bli_packm_add2_int( &a1A, gamma, &a1B, a1_pack,
        //            cntl_sub_packm_a( cntl ),
        //            gemm_thread_sub_ipackm( thread ) );
        //    
        //}

        //a1 is obj_t * , or obj_t array...return a1_pack, a pointer obj_t *
        //a1_pack = a1 .* gamma_list;

        obj_t* a1_p[STRA_LIST_LEN];
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            a1_p[ ii ] = &a1[ ii ];
        }
        bli_mulpackm_add_int( len, a1_p, gamma_list, a1_pack,
                cntl_sub_packm_a( cntl ),
                gemm_thread_sub_ipackm( thread ) );


        //bli_printm( "a1_pack", a1_pack, "%4.6lf", "" );


        //c1A->c1A_pack ?
        //c1B->c1B_pack ?
		// Pack C1 (if instructed).
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            //if ( bli_obj_buffer( c1[ i ] ) != NULL ) {
                bli_packm_int( &c1[ ii ], c1_pack[ ii ],
                        cntl_sub_packm_c( cntl ),
                        gemm_thread_sub_ipackm( thread ) );
            //}
        }


		// Perform gemm subproblem: c_tmp = a1_pack * b_pack
        //bli_gemm_int( &BLIS_ONE,
        //            a1_pack, 
        //            b_pack,
        //            &BLIS_ZERO,
        //            c1A_pack,
        //            cntl_sub_gemm( cntl ),
        //            gemm_thread_sub_gemm( thread ) );

        //////////////////////////////////////////////////////////////////////
        //if ( bli_obj_buffer( c1B ) == NULL ) {
        //    bli_gemm_int( alpha1,
        //            a1_pack, 
        //            b_pack,
        //            beta1,
        //            c1A_pack,
        //            cntl_sub_gemm( cntl ),
        //            gemm_thread_sub_gemm( thread ) );
        //} else {
        //    //bli_obj_scalar_apply_scalar( &BLIS_ZERO, c1_pack );
        //    bli_mulstraprim2_ker_var2( alpha1, alpha2,
        //            a1_pack,
        //            b_pack,
        //            beta1,  beta2,
        //            c1A_pack,
        //            c1B_pack,
        //            cntl_sub_gemm( cntl ),
        //            gemm_thread_sub_gemm( thread ) );
        //}

        //bli_mulstraprim2_ker_var2( len,
        //                          alpha_list,
        //                          a1_pack,
        //                          b_pack,
        //                          beta_list,
        //                          c1_pack,
        //                          cntl_sub_gemm( cntl ),
        //                          gemm_thread_sub_gemm( thread ) );

        //bli_mulstraprim2_ker_var2( len,
        //                          alpha_list,
        //                          a1_pack,
        //                          b_pack,
        //                          beta_list,
        //                          c1_pack,
        //                          cntl_sub_gemm( cntl ),
        //                          gemm_thread_sub_gemm( thread ) );


        bli_gemm_int( &BLIS_ONE,
                      a1_pack,
                      b_pack,
                      &BLIS_ONE,
                      c1_pack[0],
                      cntl_sub_gemm( cntl ),
                      gemm_thread_sub_gemm( thread ) );



        //bli_printm( "c1_pack[ 0 ]", c1_pack[0], "%4.6lf", "" );
        //bli_printm( "c1_pack[ 1 ]", c1_pack[1], "%4.6lf", "" );


		// Perform gemm subproblem: c_tmp = a1_pack * b_pack

        // The following should only be computed in the last iteration of rank-k update!
        // NEED:
        // Perform: cA := beta1*cA+alpha1*c_tmp
        //          cB := beta2*cB+alpha2*c_tmp
       
        thread_ibarrier( thread );

		// Unpack C1 (if C1 was packed).
        // Currently must be done by 1 thread
        
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            //if ( bli_obj_buffer( c1[ i ] ) != NULL ) {
                bli_unpackm_int( c1_pack[ ii ], &c1[ ii ],
                        cntl_sub_unpackm_c( cntl ),
                        gemm_thread_sub_ipackm( thread ) );
            //}
        }

    }

	// If any packing buffers were acquired within packm, release them back
	// to the memory manager.
    thread_obarrier( thread );
    if( thread_am_ochief( thread ) )
	    bli_packm_release( b_pack, cntl_sub_packm_b( cntl ) );
    if( thread_am_ichief( thread ) ){
        bli_packm_release( a1_pack, cntl_sub_packm_a( cntl ) );
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_packm_release( c1_pack[ ii ], cntl_sub_packm_c( cntl ) );
        }
    }


    //bli_obj_free( &bb );
}

