/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"

void dstraprim_ukernel_ref( dim_t k,
                       double* restrict alpha1, double* restrict alpha2,
                       double* restrict a,
                       double* restrict b,
                       double* restrict beta1,  double* restrict beta2,
                       double* restrict cA, inc_t rs_cA, inc_t cs_cA,
                       double* restrict cB, inc_t rs_cB, inc_t cs_cB,
                       auxinfo_t* data )
{
    //printf("a[0]:%lf", a[0]);
    //printf("b[0]:%lf", b[0]);
    //printf("cA[0]:%lf", cA[0]);
    //printf("cB[0]:%lf", cB[0]);
    //printf("alpha1[0]:%lf", alpha1[0]);
    //printf("alpha2[0]:%lf", alpha2[0]);
    //printf("beta1[0]:%lf", beta1[0]);
    //printf("beta2[0]:%lf", beta2[0]);
    //printf("\n");

    const dim_t mr = BLIS_DEFAULT_MR_D;
    const dim_t nr = BLIS_DEFAULT_NR_D;

    //Need to have a temporary buffer so we can perform checksum...
    double AB[BLIS_DEFAULT_MR_D * BLIS_DEFAULT_NR_D];
    double ZERO = 0.0;
    double ONE  = 1.0;


    memset(AB, 0, sizeof(double)*BLIS_DEFAULT_MR_D * BLIS_DEFAULT_NR_D);

    //Call microkernel
    //BLIS_DGEMM_UKERNEL2( k,
    BLIS_DGEMM_UKERNEL( k,
                        &ONE, 
                        a, 
                        b,
                        &ZERO,
                        AB, 1, BLIS_DEFAULT_MR_D,
                        data );


    for( int i = 0; i < mr; i++ ){
        for( int j = 0; j < nr; j++ ){
            cA[ i * rs_cA + j * cs_cA ] = *beta1 * cA[ i * rs_cA + j * cs_cA ] + *alpha1 * AB[i*1 + j*BLIS_DEFAULT_MR_D];
            //cA[ i * rs_cA + j * cs_cA ] += *alpha1 * AB[i*1 + j*BLIS_DEFAULT_MR_D];
            //printf( "%lf ", cA[i*1 + j*BLIS_DEFAULT_MR_D] );
        }
        //printf("\n");
    }

    if (cB != NULL ) {
        for( int i = 0; i < mr; i++ ){
            for( int j = 0; j < nr; j++ ){
                cB[ i * rs_cB + j * cs_cB ] = *beta2 * cB[ i * rs_cB + j * cs_cB ] + *alpha2 * AB[i*1 + j*BLIS_DEFAULT_MR_D];
                //cB[ i * rs_cB + j * cs_cB ] += *alpha2 * AB[i*1 + j*BLIS_DEFAULT_MR_D];
                //printf( "%lf ", cA[i*1 + j*BLIS_DEFAULT_MR_D] );
            }
            //printf("\n");
        }
    }
    //else {
    //    printf( "cB == NULL!\n" );
    //}



}

void sstraprim_ukernel_ref( dim_t k,
                       float* restrict alpha1, float* restrict alpha2,
                       float* restrict a,
                       float* restrict b,
                       float* restrict beta1,  float* restrict beta2,
                       float* restrict cA, inc_t rs_cA, inc_t cs_cA,
                       float* restrict cB, inc_t rs_cB, inc_t cs_cB,
                       auxinfo_t* data )
{
}

void cstraprim_ukernel_ref( dim_t k,
                       scomplex* restrict alpha1, scomplex* restrict alpha2,
                       scomplex* restrict a,
                       scomplex* restrict b,
                       scomplex* restrict beta1,  scomplex* restrict beta2,
                       scomplex* restrict cA, inc_t rs_cA, inc_t cs_cA,
                       scomplex* restrict cB, inc_t rs_cB, inc_t cs_cB,
                       auxinfo_t* data )
{
}

void zstraprim_ukernel_ref( dim_t k,
                       dcomplex* restrict alpha1, dcomplex* restrict alpha2,
                       dcomplex* restrict a,
                       dcomplex* restrict b,
                       dcomplex* restrict beta1,  dcomplex* restrict beta2,
                       dcomplex* restrict cA, inc_t rs_cA, inc_t cs_cA,
                       dcomplex* restrict cB, inc_t rs_cB, inc_t cs_cB,
                       auxinfo_t* data )
{
}
