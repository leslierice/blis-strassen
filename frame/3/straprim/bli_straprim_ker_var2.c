/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"

#define FUNCPTR_T straprim_fp

typedef void (*FUNCPTR_T)(
                           pack_t  schema_a,
                           pack_t  schema_b,
                           dim_t   m,
                           dim_t   n,
                           dim_t   k,
                           void*   alpha1, void*   alpha2,
                           void*   a, inc_t cs_a, inc_t is_a,
                                      dim_t pd_a, inc_t ps_a,
                           void*   b, inc_t rs_b, inc_t is_b,
                                      dim_t pd_b, inc_t ps_b,
                           void*   beta1, void*   beta2,
                           void*   cA, inc_t rs_cA, inc_t cs_cA,
                           void*   cB, inc_t rs_cB, inc_t cs_cB,
                           void*   straprim_ukr,
                           gemm_thrinfo_t* thread
                         );

static FUNCPTR_T GENARRAY(ftypes,straprim_ker_var2);


void bli_straprim_ker_var2( obj_t*  alpha1, obj_t*  alpha2,
                            obj_t*  a,
                            obj_t*  b,
                            obj_t*  beta1, obj_t*  beta2,
                            obj_t*  cA,
                            obj_t*  cB,
                            gemm_t* cntl,
                            gemm_thrinfo_t* thread )
{


    /*bli_printm( "a", a, "%4.6lf", "" );
    bli_printm( "b", b, "%4.6lf", "" );
    bli_printm( "cA", cA, "%4.6lf", "" );
    bli_printm( "cB", cB, "%4.6lf", "" );
    bli_printm( "alpha1", alpha1, "%4.6lf", "" );
    bli_printm( "alpha2", alpha2, "%4.6lf", "" );
    bli_printm( "beta1", beta1, "%4.6lf", "" );
    bli_printm( "beta2", beta2, "%4.6lf", "" ); */


	num_t     dt_exec   = bli_obj_execution_datatype( *cA );

	pack_t    schema_a  = bli_obj_pack_schema( *a );
	pack_t    schema_b  = bli_obj_pack_schema( *b );

	dim_t     m         = bli_obj_length( *cA );
	dim_t     n         = bli_obj_width( *cA );
	dim_t     k         = bli_obj_width( *a );

	void*     buf_a     = bli_obj_buffer_at_off( *a );
	inc_t     cs_a      = bli_obj_col_stride( *a );
	inc_t     is_a      = bli_obj_imag_stride( *a );
	dim_t     pd_a      = bli_obj_panel_dim( *a );
	inc_t     ps_a      = bli_obj_panel_stride( *a );

	void*     buf_b     = bli_obj_buffer_at_off( *b );
	inc_t     rs_b      = bli_obj_row_stride( *b );
	inc_t     is_b      = bli_obj_imag_stride( *b );
	dim_t     pd_b      = bli_obj_panel_dim( *b );
	inc_t     ps_b      = bli_obj_panel_stride( *b );

	void*     buf_cA     = bli_obj_buffer_at_off( *cA );
	inc_t     rs_cA      = bli_obj_row_stride( *cA );
	inc_t     cs_cA      = bli_obj_col_stride( *cA );

    void*     buf_cB     = bli_obj_buffer_at_off( *cB );
	inc_t     rs_cB      = bli_obj_row_stride( *cB );
	inc_t     cs_cB      = bli_obj_col_stride( *cB );

	obj_t     scalar_a;
	obj_t     scalar_b;

    //void*     buf_alpha1  = bli_obj_buffer_for_const(bli_obj_datatype( *a ), *alpha1 );
	//void*     buf_alpha1 = bli_obj_buffer_at_off( *alpha1 );
	//void*     buf_alpha2 = bli_obj_buffer_at_off( *alpha2 );
	//void*     buf_beta1  = bli_obj_buffer_at_off( *beta1 );
	//void*     buf_beta2  = bli_obj_buffer_at_off( *beta2 );

    void*     buf_alpha1 = bli_obj_buffer_for_const(BLIS_DOUBLE, *alpha1 );
    void*     buf_alpha2 = bli_obj_buffer_for_const(BLIS_DOUBLE, *alpha2 );
    void*     buf_beta1  = bli_obj_buffer_for_const(BLIS_DOUBLE, *beta1 );
    void*     buf_beta2  = bli_obj_buffer_for_const(BLIS_DOUBLE, *beta2 );



	FUNCPTR_T f;

	func_t*   straprim_ukrs = bli_func_obj_create( BLIS_SSTRAPRIM_UKR, BLIS_SGEMM_UKERNEL_PREFERS_CONTIG_ROWS,
                                                   BLIS_DSTRAPRIM_UKR, BLIS_DGEMM_UKERNEL_PREFERS_CONTIG_ROWS,
                                                   BLIS_CSTRAPRIM_UKR, BLIS_CGEMM_UKERNEL_PREFERS_CONTIG_ROWS,
                                                   BLIS_ZSTRAPRIM_UKR, BLIS_ZGEMM_UKERNEL_PREFERS_CONTIG_ROWS
                                                 );
	void*     straprim_ukr;


	//// Detach and multiply the scalars attached to A and B.
	//bli_obj_scalar_detach( a, &scalar_a );
	//bli_obj_scalar_detach( b, &scalar_b );
	//bli_mulsc( &scalar_a, &scalar_b );

	//// Grab the addresses of the internal scalar buffers for the scalar
	//// merged above and the scalar attached to C.
	//buf_alpha1 = bli_obj_internal_scalar_buffer( scalar_b );
	//buf_beta1  = bli_obj_internal_scalar_buffer( *cA );
	//buf_beta2  = bli_obj_internal_scalar_buffer( *cB );


	// Index into the type combination array to extract the correct
	// function pointer.
	f = ftypes[dt_exec];

	// Extract from the control tree node the func_t object containing
	// the straprim micro-kernel function addresses, and then query the
	// function address corresponding to the current datatype.
//	straprim_ukrs = cntl_gemm_ukrs( cntl );
	straprim_ukr  = bli_func_obj_query( dt_exec, straprim_ukrs );

	// Invoke the function.
	f( schema_a,
	   schema_b,
	   m,
	   n,
	   k,
	   buf_alpha1, buf_alpha2,
	   buf_a, cs_a, is_a,
	          pd_a, ps_a,
	   buf_b, rs_b, is_b,
	          pd_b, ps_b,
	   buf_beta1,  buf_beta2,
	   buf_cA, rs_cA, cs_cA,
	   buf_cB, rs_cB, cs_cB,
	   straprim_ukr,
	   thread );
}


#undef  GENTFUNC
#define GENTFUNC( ctype, ch, varname, ukrtype ) \
\
void PASTEMAC(ch,varname)( \
                           pack_t  schema_a, \
                           pack_t  schema_b, \
                           dim_t   m, \
                           dim_t   n, \
                           dim_t   k, \
                           void*   alpha1, void* alpha2, \
                           void*   a, inc_t cs_a, inc_t is_a, \
                                      dim_t pd_a, inc_t ps_a, \
                           void*   b, inc_t rs_b, inc_t is_b, \
                                      dim_t pd_b, inc_t ps_b, \
                           void*   beta1, void*   beta2, \
                           void*   cA, inc_t rs_cA, inc_t cs_cA, \
                           void*   cB, inc_t rs_cB, inc_t cs_cB, \
                           void*   straprim_ukr,  \
                           gemm_thrinfo_t* thread \
                         ) \
{ \
	/* Cast the micro-kernel address to its function pointer type. */ \
	PASTECH(ch,ukrtype) straprim_ukr_cast = straprim_ukr; \
\
	/* Temporary C buffer for edge cases. */ \
	ctype           ctA[ PASTEMAC(ch,maxmr) * \
	                    PASTEMAC(ch,maxnr) ] \
	                    __attribute__((aligned(BLIS_STACK_BUF_ALIGN_SIZE))); \
	const inc_t     rs_ctA      = 1; \
	const inc_t     cs_ctA      = PASTEMAC(ch,maxmr); \
    ctype           ctB[ PASTEMAC(ch,maxmr) * \
	                    PASTEMAC(ch,maxnr) ] \
	                    __attribute__((aligned(BLIS_STACK_BUF_ALIGN_SIZE))); \
	const inc_t     rs_ctB      = 1; \
	const inc_t     cs_ctB      = PASTEMAC(ch,maxmr); \
\
	/* Alias some constants to simpler names. */ \
	const dim_t     MR         = pd_a; \
	const dim_t     NR         = pd_b; \
	/*const dim_t     PACKMR     = cs_a;*/ \
	/*const dim_t     PACKNR     = rs_b;*/ \
\
	ctype* restrict zero       = PASTEMAC(ch,0); \
	ctype* restrict one        = PASTEMAC(ch,1); \
	ctype* restrict a_cast     = a; \
	ctype* restrict b_cast     = b; \
	ctype* restrict cA_cast     = cA; \
	ctype* restrict cB_cast     = cB; \
	ctype* restrict alpha1_cast = alpha1; \
	ctype* restrict alpha2_cast = alpha2; \
	ctype* restrict beta1_cast  = beta1; \
	ctype* restrict beta2_cast  = beta2; \
	ctype* restrict b1; \
	ctype* restrict c1A; \
	ctype* restrict c1B=NULL; \
\
    /*printf("a_cast[0]:%lf", a_cast[0]); \
    printf("b_cast[0]:%lf", b_cast[0]); \
    printf("cA_cast[0]:%lf", cA_cast[0]); \
    printf("cB_cast[0]:%lf", cB_cast[0]); \
    printf("alpha1_cast[0]:%lf", alpha1_cast[0]); \
    printf("alpha2_cast[0]:%lf", alpha2_cast[0]); \
    printf("beta1_cast[0]:%lf", beta1_cast[0]); \
    printf("beta2_cast[0]:%lf", beta2_cast[0]); */\
\
	dim_t           m_iter, m_left; \
	dim_t           n_iter, n_left; \
	dim_t           i, j; \
	dim_t           m_cur; \
	dim_t           n_cur; \
	inc_t           rstep_a; \
	inc_t           cstep_b; \
	inc_t           rstep_c, cstep_c; \
	auxinfo_t       aux; \
\
	/*
	   Assumptions/assertions:
	     rs_a == 1
	     cs_a == PACKMR
	     pd_a == MR
	     ps_a == stride to next micro-panel of A
	     rs_b == PACKNR
	     cs_b == 1
	     pd_b == NR
	     ps_b == stride to next micro-panel of B
	     rs_c == (no assumptions)
	     cs_c == (no assumptions)
	*/ \
\
	/* If any dimension is zero, return immediately. */ \
	if ( bli_zero_dim3( m, n, k ) ) return; \
\
	/* Clear the temporary C buffer in case it has any infs or NaNs. */ \
	PASTEMAC(ch,set0s_mxn)( MR, NR, \
	                        ctA, rs_ctA, cs_ctA ); \
    PASTEMAC(ch,set0s_mxn)( MR, NR, \
	                        ctB, rs_ctB, cs_ctB ); \
\
	/* Compute number of primary and leftover components of the m and n
	   dimensions. */ \
	n_iter = n / NR; \
	n_left = n % NR; \
\
	m_iter = m / MR; \
	m_left = m % MR; \
\
	if ( n_left ) ++n_iter; \
	if ( m_left ) ++m_iter; \
\
	/* Determine some increments used to step through A, B, and C. */ \
	rstep_a = ps_a; \
\
	cstep_b = ps_b; \
\
	rstep_c = rs_cA * MR; \
	cstep_c = cs_cA * NR; \
\
	/* Save the pack schemas of A and B to the auxinfo_t object. */ \
	bli_auxinfo_set_schema_a( schema_a, aux ); \
	bli_auxinfo_set_schema_b( schema_b, aux ); \
\
	/* Save the imaginary stride of A and B to the auxinfo_t object. */ \
	bli_auxinfo_set_is_a( is_a, aux ); \
	bli_auxinfo_set_is_b( is_b, aux ); \
\
	gemm_thrinfo_t* caucus = gemm_thread_sub_gemm( thread ); \
	dim_t jr_num_threads = thread_n_way( thread ); \
	dim_t jr_thread_id   = thread_work_id( thread ); \
	dim_t ir_num_threads = thread_n_way( caucus ); \
	dim_t ir_thread_id   = thread_work_id( caucus ); \
\
	/* Loop over the n dimension (NR columns at a time). */ \
	for ( j = jr_thread_id; j < n_iter; j += jr_num_threads ) \
	{ \
		ctype* restrict a1; \
		ctype* restrict c11A; \
		ctype* restrict c11B=NULL; \
		ctype* restrict b2; \
\
		b1 = b_cast + j * cstep_b; \
		c1A = cA_cast + j * cstep_c; \
        if ( cB_cast != NULL ) { \
            c1B = cB_cast + j * cstep_c; \
        }\
\
		n_cur = ( bli_is_not_edge_f( j, n_iter, n_left ) ? NR : n_left ); \
\
		/* Initialize our next panel of B to be the current panel of B. */ \
		b2 = b1; \
\
		/* Loop over the m dimension (MR rows at a time). */ \
		for ( i = ir_thread_id; i < m_iter; i += ir_num_threads ) \
		{ \
			ctype* restrict a2; \
\
			a1  = a_cast + i * rstep_a; \
			c11A = c1A     + i * rstep_c; \
            if ( c1B != NULL ) { \
                c11B = c1B     + i * rstep_c; \
            }\
\
			m_cur = ( bli_is_not_edge_f( i, m_iter, m_left ) ? MR : m_left ); \
\
			/* Compute the addresses of the next panels of A and B. */ \
			a2 = gemm_get_next_a_micropanel( caucus, a1, rstep_a ); \
			if ( bli_is_last_iter( i, m_iter, ir_thread_id, ir_num_threads ) ) \
			{ \
				a2 = a_cast; \
				b2 = gemm_get_next_b_micropanel( thread, b1, cstep_b ); \
				if ( bli_is_last_iter( j, n_iter, jr_thread_id, jr_num_threads ) ) \
					b2 = b_cast; \
			} \
\
			/* Save addresses of next panels of A and B to the auxinfo_t
			   object. */ \
			bli_auxinfo_set_next_a( a2, aux ); \
			bli_auxinfo_set_next_b( b2, aux ); \
\
			/* Handle interior and edge cases separately. */ \
			if ( m_cur == MR && n_cur == NR ) \
			{ \
				/* Invoke the straprim micro-kernel. */ \
				straprim_ukr_cast( k, \
				               alpha1_cast, alpha2_cast, \
				               a1, \
				               b1, \
				               beta1_cast, beta2_cast, \
				               c11A, rs_cA, cs_cA, \
				               c11B, rs_cB, cs_cB, \
				               &aux ); \
			} \
			else \
			{ \
                /*printf("Invoke straprim kernel: Edge cases!\n");*/ \
/* Clear the temporary C buffer in case it has any infs or NaNs. */ \
	PASTEMAC(ch,set0s_mxn)( MR, NR, \
	                        ctA, rs_ctA, cs_ctA ); \
    PASTEMAC(ch,set0s_mxn)( MR, NR, \
	                        ctB, rs_ctB, cs_ctB ); \
				/* Invoke the straprim micro-kernel. */ \
				straprim_ukr_cast( k, \
				               alpha1_cast, alpha2_cast, \
				               a1, \
				               b1, \
				               zero, zero, \
				               /*one, one, */ \
				               ctA, rs_ctA, cs_ctA, \
				               ctB, rs_ctB, cs_ctB, \
				               &aux ); \
                /*straprim_ukr_cast( k, \
				               alpha1_cast, alpha2_cast, \
				               a1, \
				               b1, \
				               zero, zero, \
				               ctA, rs_ctA, cs_ctA, \
				               ctB, rs_ctB, cs_ctB, \
				               &aux ); */ \
\
                /* printf( "m_cur:%lu,n_cur:%lu\n", m_cur, n_cur ); \
                printf( "ctA[0]:%lf, beta1:%lf, ctB[0]:%lf, beta2:%lf\n", ctA[0], *beta1_cast, ctB[0], beta2_cast[0] ); */ \
                /*bli_printm( "ctA", ctA, "%4.6lf", "" ); \
                bli_printm( "beta1", beta1, "%4.6lf", "" ); \
                bli_printm( "ctB", ctB, "%4.6lf", "" ); \
                bli_printm( "beta2", beta2, "%4.6lf", "" );*/ \
				/* Scale the bottom edge of C and add the result from above. */ \
				PASTEMAC(ch,xpbys_mxn)( m_cur, n_cur, \
				                        ctA,  rs_ctA, cs_ctA, \
				                        beta1_cast, \
				                        c11A, rs_cA,  cs_cA \
                                        ); \
                if ( c11B != NULL ) { \
                PASTEMAC(ch,xpbys_mxn)( m_cur, n_cur, \
				                        ctB,  rs_ctB, cs_ctB, \
				                        beta2_cast, \
				                        c11B, rs_cB,  cs_cB \
                                        ); \
                } \
			} \
		} \
	} \
\
/*PASTEMAC(ch,fprintm)( stdout, "straprim_ker_var2: b1", k, NR, b1, NR, 1, "%4.1f", "" ); \
PASTEMAC(ch,fprintm)( stdout, "straprim_ker_var2: a1", MR, k, a1, 1, MR, "%4.1f", "" );*/ \
}

INSERT_GENTFUNC_BASIC( straprim_ker_var2, straprim_ukr_t )

