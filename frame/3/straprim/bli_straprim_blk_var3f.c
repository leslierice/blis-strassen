/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"

void bli_straprim_blk_var3f( obj_t*  alpha1, obj_t*  alpha2,
                             obj_t*  aA, obj_t* gamma, obj_t*  aB,
                             obj_t*  bA, obj_t* delta, obj_t*  bB,
                             obj_t*  beta1, obj_t*  beta2,
                             obj_t*  cA, obj_t*  cB,
                             gemm_t* cntl,
                             gemm_thrinfo_t* thread )
{
    obj_t  cA_pack_s, cB_pack_s;
    obj_t  a1A_pack_s, a1B_pack_s, b1A_pack_s, b1B_pack_s;

    obj_t  a1A, a1B, b1A, b1B;
    obj_t* a1A_pack = NULL, *a1B_pack = NULL;
    obj_t* b1A_pack = NULL, *b1B_pack = NULL;
    obj_t* cA_pack = NULL,  *cB_pack = NULL;

	dim_t  i;
	dim_t  b_alg;
	dim_t  k_trans;

    if( thread_am_ochief( thread ) ){
        // Initialize object for packing C
	    bli_obj_init_pack( &cA_pack_s );
	    bli_obj_init_pack( &cB_pack_s );
        bli_packm_init( cA, &cA_pack_s,
                        cntl_sub_packm_c( cntl ) );
        bli_packm_init( cB, &cB_pack_s,
                        cntl_sub_packm_c( cntl ) );

        // Scale C by beta (if instructed).
        bli_scalm_int( &BLIS_ONE,
                       cA,
                       cntl_sub_scalm( cntl ) );
        if ( bli_obj_buffer( *cB ) != NULL ) {
            bli_scalm_int( &BLIS_ONE,
                    cB,
                    cntl_sub_scalm( cntl ) );
        }
    }
    cA_pack = thread_obroadcast( thread, &cA_pack_s );
    cB_pack = thread_obroadcast( thread, &cB_pack_s );

    // Initialize pack objects for A and B that are passed into packm_init().
    if( thread_am_ichief( thread ) ){
        bli_obj_init_pack( &a1A_pack_s );
        bli_obj_init_pack( &a1B_pack_s );
        bli_obj_init_pack( &b1A_pack_s );
        bli_obj_init_pack( &b1B_pack_s );
    }
    a1A_pack = thread_ibroadcast( thread, &a1A_pack_s );
    a1B_pack = thread_ibroadcast( thread, &a1B_pack_s );
    b1A_pack = thread_ibroadcast( thread, &b1A_pack_s );
    b1B_pack = thread_ibroadcast( thread, &b1B_pack_s );

	// Pack C (if instructed).
	bli_packm_int( cA, cA_pack,
	               cntl_sub_packm_c( cntl ),
                   gemm_thread_sub_opackm( thread ) );
	bli_packm_int( cB, cB_pack,
	               cntl_sub_packm_c( cntl ),
                   gemm_thread_sub_opackm( thread ) );

	// Query dimension in partitioning direction.
	k_trans = bli_obj_width_after_trans( *aA );

	// Partition along the k dimension.
	for ( i = 0; i < k_trans; i += b_alg )
	{
		// Determine the current algorithmic blocksize.
		// NOTE: We call a gemm/hemm/symm-specific function to determine
		// the kc blocksize so that we can implement the "nudging" of kc
		// to be a multiple of mr or nr, as needed.
		b_alg = bli_gemm_determine_kc_f( i, k_trans, aA, bA,
		                                 cntl_blocksize( cntl ) );

		// Acquire partitions for A1 and B1.
		bli_acquire_mpart_l2r( BLIS_SUBPART1,
		                       i, b_alg, aA, &a1A );
		bli_acquire_mpart_l2r( BLIS_SUBPART1,
		                       i, b_alg, aB, &a1B );
		bli_acquire_mpart_t2b( BLIS_SUBPART1,
		                       i, b_alg, bA, &b1A );
		bli_acquire_mpart_t2b( BLIS_SUBPART1,
		                       i, b_alg, bB, &b1B );

		// Initialize objects for packing A1 and B1.
        if( thread_am_ichief( thread ) ) {
            bli_packm_init( &a1A, a1A_pack,
                            cntl_sub_packm_a( cntl ) );
            bli_packm_init( &a1B, a1B_pack,
                            cntl_sub_packm_a( cntl ) );
            bli_packm_init( &b1A, b1A_pack,
                            cntl_sub_packm_b( cntl ) );
            bli_packm_init( &b1B, b1B_pack,
                            cntl_sub_packm_b( cntl ) );
        }
        thread_ibarrier( thread );

		// Pack A1 (if instructed).
		bli_packm_int( &a1A, a1A_pack,
		               cntl_sub_packm_a( cntl ),
                       gemm_thread_sub_ipackm( thread ) );
		bli_packm_int( &a1B, a1B_pack,
		               cntl_sub_packm_a( cntl ),
                       gemm_thread_sub_ipackm( thread ) );

		// Pack B1 (if instructed).
		bli_packm_int( &b1A, b1A_pack,
		               cntl_sub_packm_b( cntl ),
                       gemm_thread_sub_ipackm( thread ) );
		bli_packm_int( &b1B, b1B_pack,
		               cntl_sub_packm_b( cntl ),
                       gemm_thread_sub_ipackm( thread ) );

		// Perform straprim subproblem.
		bli_straprim_int( alpha1, alpha2,
		                  a1A_pack, gamma, a1B_pack,
		                  b1A_pack, delta, b1B_pack,
		                  beta1, beta2,
		                  cA_pack, cB_pack,
		                  cntl_sub_gemm( cntl ),
                          gemm_thread_sub_gemm( thread) );

		// This variant executes multiple rank-k updates. Therefore, if the
		// internal beta scalar on matrix C is non-zero, we must use it
		// only for the first iteration (and then BLIS_ONE for all others).
		// And since c_pack is a local obj_t, we can simply overwrite the
		// internal beta scalar with BLIS_ONE once it has been used in the
		// first iteration.
        thread_ibarrier( thread );
		if ( i == 0 && thread_am_ichief( thread ) ) {
            bli_obj_scalar_reset( cA_pack );
            bli_obj_scalar_reset( cB_pack );
        }

	}

    thread_obarrier( thread );

	// Unpack C (if C was packed).
    bli_unpackm_int( cA_pack, cA,
                     cntl_sub_unpackm_c( cntl ),
                     gemm_thread_sub_opackm( thread ) );
    bli_unpackm_int( cB_pack, cB,
                     cntl_sub_unpackm_c( cntl ),
                     gemm_thread_sub_opackm( thread ) );

	// If any packing buffers were acquired within packm, release them back
	// to the memory manager.
    if( thread_am_ochief( thread ) ) {
        bli_packm_release( cA_pack, cntl_sub_packm_c( cntl ) );
        bli_packm_release( cB_pack, cntl_sub_packm_c( cntl ) );
    }
    if( thread_am_ichief( thread ) ){
        bli_packm_release( a1A_pack, cntl_sub_packm_a( cntl ) );
        bli_packm_release( a1B_pack, cntl_sub_packm_a( cntl ) );
        bli_packm_release( b1A_pack, cntl_sub_packm_b( cntl ) );
        bli_packm_release( b1B_pack, cntl_sub_packm_b( cntl ) );
    }
}

