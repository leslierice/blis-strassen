/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"

extern gemm_t* gemm_cntl;

//cA = beta1*cA+alpha1*(aA+gamma*aB)(bA+delta*bB)
//cB = beta2*cB+alpha2*(aA+gamma*aB)(bA+delta*bB)
void bli_straprim2( obj_t*  alpha1, obj_t*  alpha2,
                   obj_t*  aA, obj_t*  gamma, obj_t*  aB,
                   obj_t*  bA, obj_t*  delta, obj_t*  bB,
                   obj_t*  beta1, obj_t*  beta2,
                   obj_t*  cA, obj_t*  cB )
{
	//num_t dt = bli_obj_datatype( *c1 );

	// If an induced method is available (ie: implemented and enabled),
	// call it instead.
	//if ( bli_strprimind_has_avail( dt ) )
	//{
	//	strprim_fp_t func = bli_strprimind_get_avail( dt );

	//	return func( alpha, a, b, beta, c );
	//}

    obj_t c_tmp, c_null;

    bli_obj_create( bli_obj_datatype( *cA ), bli_obj_length( *cA ), bli_obj_width( *cA ), 0, 0, &c_tmp );
    bli_setm( &BLIS_ZERO, &c_tmp );

    bli_obj_create_without_buffer( bli_obj_datatype( *cA ), bli_obj_length( *cA ), bli_obj_width( *cA ), &c_null );



	bli_straprim2_front( alpha1, alpha2,
                       aA, gamma, aB,
                       bA, delta, bB,
                       beta1, beta2,
                       //cA, cB,
                       &c_tmp, &c_null,
	                   gemm_cntl );


    mkl_axpym( alpha1, &c_tmp, cA );
    if ( bli_obj_buffer( *cB )  != NULL ) {
        mkl_axpym( alpha2, &c_tmp, cB );
    }

    bli_obj_free( &c_tmp );
    bli_obj_free( &c_null );

}


/*
#undef  GENTFUNC
#define GENTFUNC( ctype, ch, opname ) \
\
void PASTEMAC(ch,opname)( \
                          trans_t transa, \
                          trans_t transb, \
                          dim_t   m, \
                          dim_t   n, \
                          dim_t   k, \
                          ctype*  alpha1, \
                          ctype*  alpha2, \
                          ctype*  a1, inc_t rs_a1, inc_t cs_a1, \
                          ctype*  gamma, \
                          ctype*  a2, inc_t rs_a2, inc_t cs_a2, \
                          ctype*  b1, inc_t rs_b1, inc_t cs_b1, \
                          ctype*  delta, \
                          ctype*  b2, inc_t rs_b2, inc_t cs_b2, \
                          ctype*  beta1, \
                          ctype*  beta2, \
                          ctype*  c1, inc_t rs_c1, inc_t cs_c1,  \
                          ctype*  c2, inc_t rs_c2, inc_t cs_c2  \
                        ) \
{ \
	const num_t dt = PASTEMAC(ch,type); \
\
	obj_t       alphao, ao, bo, betao, co; \
\
	dim_t       m_a, n_a; \
	dim_t       m_b, n_b; \
\
	bli_set_dims_with_trans( transa, m, k, m_a, n_a ); \
	bli_set_dims_with_trans( transb, k, n, m_b, n_b ); \
\
	bli_obj_create_1x1_with_attached_buffer( dt, alpha1, &alphao ); \
	bli_obj_create_1x1_with_attached_buffer( dt, alpha2, &alphao ); \
	bli_obj_create_1x1_with_attached_buffer( dt, beta,  &betao  ); \
\
	bli_obj_create_with_attached_buffer( dt, m_a, n_a, a, rs_a, cs_a, &ao ); \
	bli_obj_create_with_attached_buffer( dt, m_b, n_b, b, rs_b, cs_b, &bo ); \
	bli_obj_create_with_attached_buffer( dt, m,   n,   c, rs_c, cs_c, &co ); \
\
	bli_obj_set_conjtrans( transa, ao ); \
	bli_obj_set_conjtrans( transb, bo ); \
\
	PASTEMAC0(opname)( &alphao, \
	                   &ao, \
	                   &bo, \
	                   &betao, \
	                   &co ); \
}

INSERT_GENTFUNC_BASIC0( straprim2 )

    */
