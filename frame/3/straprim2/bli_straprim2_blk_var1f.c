/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"

void bli_straprim2_blk_var1f( obj_t*  alpha1, obj_t*  alpha2,
                             obj_t*  aA, obj_t* gamma, obj_t*  aB,
                             obj_t*  bA, obj_t* delta, obj_t*  bB,
                             obj_t*  beta1, obj_t*  beta2,
                             obj_t*  cA, obj_t*  cB,
                             gemm_t* cntl,
                             gemm_thrinfo_t* thread
                             //extraloop_t* loop
                           )
{
    //err_t e_val;
    
    //The s is for "lives on the stack"
    obj_t b_pack_s;
    obj_t a1_pack_s, c1A_pack_s, c1B_pack_s;

    obj_t a1A, a1B, c1A, c1B;
    obj_t* a1_pack  = NULL;
    obj_t* b_pack   = NULL;
    //obj_t* c1_pack = NULL;
    obj_t* c1A_pack = NULL;
    obj_t* c1B_pack = NULL;

	dim_t i;
	dim_t b_alg;

    //obj_t bb;
    //obj_t *b = &bb;
    //bli_obj_create( bli_obj_datatype( *bA ), bli_obj_length( *bA ), bli_obj_width( *bA ), 0, 0, b );
    ////bA+delta*bB -> b -> b_pack
    //bli_copym( bA, b );

    ////e_val = bli_check_matrix_object( bB );
    ////if ( e_val != BLIS_EXPECTED_NONNULL_OBJECT_BUFFER )
    //if ( bli_obj_buffer( *bB ) != NULL ) {
    //    bli_axpym( delta, bB, b);
    //}
    ////if ( bB->is_dummy != 1 ) {
    ////    bli_axpym( delta, bB, b);
    ////}

    if( thread_am_ochief( thread ) ) {
	    // Initialize object for packing B.
	    bli_obj_init_pack( &b_pack_s );
	    bli_packm_init( bA, &b_pack_s,
	                    cntl_sub_packm_b( cntl ) );

        // Scale C by beta (if instructed).
        // Since scalm doesn't support multithreading yet, must be done by chief thread (ew)
        bli_scalm_int( &BLIS_ONE,
                       cA,
                       cntl_sub_scalm( cntl ) );

        if ( bli_obj_buffer( *cB ) != NULL ) {
            bli_scalm_int( &BLIS_ONE,
                    cB,
                    cntl_sub_scalm( cntl ) );
        }
    }
    b_pack = thread_obroadcast( thread, &b_pack_s );

	// Initialize objects passed into bli_packm_init for A and C
    if( thread_am_ichief( thread ) ) {
        bli_obj_init_pack( &a1_pack_s );
        bli_obj_init_pack( &c1A_pack_s );
        bli_obj_init_pack( &c1B_pack_s );
    }
    a1_pack = thread_ibroadcast( thread, &a1_pack_s );
    c1A_pack = thread_ibroadcast( thread, &c1A_pack_s );
    c1B_pack = thread_ibroadcast( thread, &c1B_pack_s );

    //
	// Pack B (if instructed).
	//bli_packm_int( b, b_pack,
	//               cntl_sub_packm_b( cntl ),
    //               gemm_thread_sub_opackm( thread ) );

    if ( bli_obj_buffer( *bB ) == NULL ) {
        bli_packm_int( bA, b_pack,
                       cntl_sub_packm_b( cntl ),
                       gemm_thread_sub_opackm( thread ) );
    } else {
        bli_packm_add2_int( bA, delta, bB, b_pack,
                           cntl_sub_packm_b( cntl ),
                           gemm_thread_sub_opackm( thread ) );

    }
    //bli_printm( "b_pack", b_pack, "%4.6lf", "" );

    dim_t my_start, my_end;
    // Get range from aA (no need to get again from aB)
    bli_get_range_t2b( thread, aA,
                       bli_blksz_get_mult_for_obj( aA, cntl_blocksize( cntl ) ),
                       &my_start, &my_end );

	// Partition along the m dimension.
	for ( i = my_start; i < my_end; i += b_alg )
	{
		// Determine the current algorithmic blocksize.
		// NOTE: Use of a (for execution datatype) is intentional!
		// This causes the right blocksize to be used if c and a are
		// complex and b is real.
		b_alg = bli_determine_blocksize_f( i, my_end, aA,
		                                   cntl_blocksize( cntl ) );

		// Acquire partitions for a1A, a1B and c1A, c1B.
		bli_acquire_mpart_t2b( BLIS_SUBPART1,
		                       i, b_alg, aA, &a1A );
        bli_acquire_mpart_t2b( BLIS_SUBPART1,
		                       i, b_alg, aB, &a1B );
		bli_acquire_mpart_t2b( BLIS_SUBPART1,
		                       i, b_alg, cA, &c1A );
        bli_acquire_mpart_t2b( BLIS_SUBPART1,
		                       i, b_alg, cB, &c1B );

        //obj_t a1;
        //bli_obj_create( bli_obj_datatype( a1A ), bli_obj_length( a1A ), bli_obj_width( a1A ), 0, 0, &a1 );
        ////a1A+gamma*a1B -> a1 -> a1_pack
        //bli_copym( &a1A, &a1 );

        ////e_val = bli_check_matrix_object( &a1B );
        ////if ( e_val == BLIS_EXPECTED_NONNULL_OBJECT_BUFFER )
        //if ( bli_obj_buffer( a1B ) != NULL ) {
        //    bli_axpym( gamma, &a1B, &a1);
        //}
        ////if ( a1B.is_dummy != 1 ) {
        ////    bli_axpym( gamma, &a1B, &a1);
        ////}

        //obj_t c_tmp;
        //bli_obj_create( bli_obj_datatype( c1A ), bli_obj_length( c1A ), bli_obj_width( c1A ), 0, 0, &c_tmp );

		
        // Initialize objects for packing a1A, a1B and c1A, c1B.
        if( thread_am_ichief( thread ) ) {
            //NEED to change this routine later to one routine with the following footprint:
            //bli_packm_axpy_init( &a1A, gamma, &a1B a1_pack,
            //                cntl_sub_packm_a( cntl ) );
            //a1A+gamma*a1B -> a1_pack

            //bli_packm_init( &a1, a1_pack,
            //                cntl_sub_packm_a( cntl ) );

            bli_packm_init( &a1A, a1_pack,
                            cntl_sub_packm_a( cntl ) );
            //bli_packm_init( &a1B, a1B_pack,
            //                cntl_sub_packm_a( cntl ) );


            //c1->c1_pack ? -> ALIASING
            //bli_packm_init( &c_tmp, c1_pack,
            //                cntl_sub_packm_c( cntl ) );
            bli_packm_init( &c1A, c1A_pack,
                            cntl_sub_packm_c( cntl ) );


            //Error????
            //if ( bli_obj_buffer( c1B ) != NULL )
            bli_packm_init( &c1B, c1B_pack,
                            cntl_sub_packm_c( cntl ) );
        }
        thread_ibarrier( thread );

        ////a1A+gamma*a1B -> a1_pack
		//// Pack A1 (if instructed).
		//bli_packm_int( &a1, a1_pack,
		//               cntl_sub_packm_a( cntl ),
        //               gemm_thread_sub_ipackm( thread ) );

        //printf( "Before packm_add\n" );

        if ( bli_obj_buffer( a1B ) == NULL ) {
            bli_packm_int( &a1A, a1_pack,
                    cntl_sub_packm_a( cntl ),
                    gemm_thread_sub_ipackm( thread ) );
        } else {
            bli_packm_add_int( &a1A, gamma, &a1B, a1_pack,
                    cntl_sub_packm_a( cntl ),
                    gemm_thread_sub_ipackm( thread ) );
            
        }

        //bli_printm( "a1_pack", a1_pack, "%4.6lf", "" );

        //printf( "After packm_add\n" );


        //c1A->c1A_pack ?
        //c1B->c1B_pack ?
		// Pack C1 (if instructed).
		//bli_packm_int( &c_tmp, c1_pack,
		//               cntl_sub_packm_c( cntl ),
        //               gemm_thread_sub_ipackm( thread ) );
        bli_packm_int( &c1A, c1A_pack,
		               cntl_sub_packm_c( cntl ),
                       gemm_thread_sub_ipackm( thread ) );

        //if ( bli_obj_buffer( c1B ) != NULL )
        bli_packm_int( &c1B, c1B_pack,
		               cntl_sub_packm_c( cntl ),
                       gemm_thread_sub_ipackm( thread ) );


		// Perform gemm subproblem: c_tmp = a1_pack * b_pack
        //bli_gemm_int( &BLIS_ONE,
        //            a1_pack, 
        //            b_pack,
        //            &BLIS_ZERO,
        //            c1A_pack,
        //            cntl_sub_gemm( cntl ),
        //            gemm_thread_sub_gemm( thread ) );

        //if ( bli_obj_buffer( c1B ) == NULL ) {



            bli_gemm_int( &BLIS_ONE,
                    a1_pack, 
                    b_pack,
                    &BLIS_ONE,
                    c1A_pack,
                    cntl_sub_gemm( cntl ),
                    gemm_thread_sub_gemm( thread ) );

            //bli_printm( "c1A_pack", c1A_pack, "%4.6lf", "" );
            

        //} else {
        //    //bli_obj_scalar_apply_scalar( &BLIS_ZERO, c1_pack );
        //    bli_straprim2_ker_var2( alpha1, alpha2,
        //            a1_pack,
        //            b_pack,
        //            beta1,  beta2,
        //            c1A_pack,
        //            c1B_pack,
        //            cntl_sub_gemm( cntl ),
        //            gemm_thread_sub_gemm( thread ) );
        //}


		// Perform gemm subproblem: c_tmp = a1_pack * b_pack

        // The following should only be computed in the last iteration of rank-k update!
        // NEED:
        // Perform: cA := beta1*cA+alpha1*c_tmp
        //          cB := beta2*cB+alpha2*c_tmp
       
        thread_ibarrier( thread );

		// Unpack C1 (if C1 was packed).
        // Currently must be done by 1 thread
        //bli_unpackm_int( c1_pack, &c_tmp,
        //                 cntl_sub_unpackm_c( cntl ),
        //                 gemm_thread_sub_ipackm( thread ) );
        bli_unpackm_int( c1A_pack, &c1A,
                         cntl_sub_unpackm_c( cntl ),
                         gemm_thread_sub_ipackm( thread ) );
        bli_unpackm_int( c1B_pack, &c1B,
                         cntl_sub_unpackm_c( cntl ),
                         gemm_thread_sub_ipackm( thread ) );


        //bli_scalm( beta1, &c1A );
        //bli_axpym( alpha1, &c_tmp, &c1A );
        //if ( bli_obj_buffer( c1B ) != NULL ) {
        //    bli_scalm( beta2, &c1B );
        //    bli_axpym( alpha2, &c_tmp, &c1B );
        //}

        //bli_obj_free( &a1 );
        //bli_obj_free( &c_tmp );
	}

	// If any packing buffers were acquired within packm, release them back
	// to the memory manager.
    thread_obarrier( thread );
    if( thread_am_ochief( thread ) )
	    bli_packm_release( b_pack, cntl_sub_packm_b( cntl ) );
    if( thread_am_ichief( thread ) ){
        bli_packm_release( a1_pack, cntl_sub_packm_a( cntl ) );

        bli_packm_release( c1A_pack, cntl_sub_packm_c( cntl ) );
        bli_packm_release( c1B_pack, cntl_sub_packm_c( cntl ) );
    }


    //bli_obj_free( &bb );
}

