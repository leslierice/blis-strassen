/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

//#define STRA_LIST_LEN 4

#include "bli_mulstraprim_front.h"
#include "bli_mulstraprim_int.h"

//#include "bli_mulstraprim_ukernel.h"
#include "bli_mulstraprim_ker_var2.h"

#include "bli_mulstraprim_blk_var1f.h"
#include "bli_mulstraprim_blk_var2f.h"
#include "bli_mulstraprim_blk_var3f.h"

#include "bli_mulstraprim_ukr_ref.h"



//
// Prototype object-based interface.
//c_list[0]     += alpha_list[0]    *(a_list[0]+gamma_list[1]*a_list[1]+..+gamma_list[len-1]*a_list[len-1])(b_list[0]+delta_list[1]*b_list[1]+...+delta_list[len-1]*b_list[len-1])
//c_list[1]     += alpha_list[1]    *(a_list[0]+gamma_list[1]*a_list[1]+..+gamma_list[len-1]*a_list[len-1])(b_list[0]+delta_list[1]*b_list[1]+...+delta_list[len-1]*b_list[len-1])
//...
//c_list[len-1] += alpha_list[len-1]*(a_list[0]+gamma_list[1]*a_list[1]+..+gamma_list[len-1]*a_list[len-1])(b_list[0]+delta_list[1]*b_list[1]+...+delta_list[len-1]*b_list[len-1])
void bli_mulstraprim( dim_t   len,
                      obj_t**  alpha_list, 
                      obj_t**  a_list, obj_t**  gamma_list,
                      obj_t**  b_list, obj_t**  delta_list,
                      obj_t**  beta_list, 
                      obj_t**  c_list);

/*
#undef  GENTPROT
#define GENTPROT( ctype, ch, opname ) \
\
void PASTEMAC(ch,opname)( \
                          trans_t transa, \
                          trans_t transb, \
                          dim_t   m, \
                          dim_t   n, \
                          dim_t   k, \
                          ctype*  alpha1, \
                          ctype*  alpha2, \
                          ctype*  a1, inc_t rs_a1, inc_t cs_a1, \
                          ctype*  gamma, \
                          ctype*  a2, inc_t rs_a2, inc_t cs_a2, \
                          ctype*  b1, inc_t rs_b1, inc_t cs_b1, \
                          ctype*  delta, \
                          ctype*  b2, inc_t rs_b2, inc_t cs_b2, \
                          ctype*  beta1, \
                          ctype*  beta2, \
                          ctype*  c1, inc_t rs_c1, inc_t cs_c1, \
                          ctype*  c2, inc_t rs_c2, inc_t cs_c2  \
                        );

INSERT_GENTPROT_BASIC( mulstraprim )

    */
