/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"
void dmulstraprim_ukernel_ref( dim_t k,
                               dim_t len,
                               double* restrict *alpha_list,
                               double* restrict a,
                               double* restrict b,
                               double* restrict *beta_list,
                               double* restrict *c_list, inc_t rs_c, inc_t cs_c,
                               auxinfo_t* data )
{
    //printf( "c_list[0][0]:%lf\n", c_list[ 0 ][ 0 ] );
    //printf("a[0]:%lf", a[0]);
    //printf("b[0]:%lf", b[0]);
    //printf("cA[0]:%lf", cA[0]);
    //printf("cB[0]:%lf", cB[0]);
    //printf("alpha1[0]:%lf", alpha1[0]);
    //printf("alpha2[0]:%lf", alpha2[0]);
    //printf("beta1[0]:%lf", beta1[0]);
    //printf("beta2[0]:%lf", beta2[0]);
    //printf("\n");

    const dim_t mr = BLIS_DEFAULT_MR_D;
    const dim_t nr = BLIS_DEFAULT_NR_D;

    //Need to have a temporary buffer so we can perform checksum...
    double AB[BLIS_DEFAULT_MR_D * BLIS_DEFAULT_NR_D];
    double ZERO = 0.0;
    double ONE  = 1.0;


    memset(AB, 0, sizeof(double)*BLIS_DEFAULT_MR_D * BLIS_DEFAULT_NR_D);

    //Call microkernel
    //BLIS_DGEMM_UKERNEL2( k,
    BLIS_DGEMM_UKERNEL( k,
                        &ONE, 
                        a, 
                        b,
                        &ZERO,
                        AB, 1, BLIS_DEFAULT_MR_D,
                        data );


    //printf( "AB[0][0] = %lf\n", AB[0] );

    for( int i = 0; i < mr; i++ ){
        for( int j = 0; j < nr; j++ ){
            //printf( "i: %d\tj: %d\n", i , j );
            //printf( "rs_c: %lu, cs_c: %lu\n", rs_c, cs_c );
            (c_list[ 0 ])[ i * rs_c + j * cs_c ] = *beta_list[ 0 ] * (c_list[ 0 ])[ i * rs_c + j * cs_c ] + *alpha_list[ 0 ] * AB[i*1 + j*BLIS_DEFAULT_MR_D];
            //cA[ i * rs_cA + j * cs_cA ] += *alpha1 * AB[i*1 + j*BLIS_DEFAULT_MR_D];
            //printf( "%lf ", cA[i*1 + j*BLIS_DEFAULT_MR_D] );
        }
        //printf("\n");
    }

    //printf( "c_list[0][0] = %lf\n", c_list[0][0] );
    //printf( "c_list[1][0] = %lf\n", c_list[1][0] );


    #pragma unroll
    for ( dim_t ii = 1; ii < STRA_LIST_LEN; ii++ ) {
        if ( c_list[ ii ] != NULL ) {
            for( int i = 0; i < mr; i++ ){
                for( int j = 0; j < nr; j++ ){
                    c_list[ ii ][ i * rs_c + j * cs_c ] = *beta_list[ ii ] * c_list[ ii ][ i * rs_c + j * cs_c ] + *alpha_list[ ii ] * AB[i*1 + j*BLIS_DEFAULT_MR_D];
                    //cB[ i * rs_cB + j * cs_cB ] += *alpha2 * AB[i*1 + j*BLIS_DEFAULT_MR_D];
                    //printf( "%lf ", cA[i*1 + j*BLIS_DEFAULT_MR_D] );
                }
                //printf("\n");
            }

        } else {
            printf( "ukernel:c_list[ %lu ] == NULL!\n", ii );
        }
    }

    //printf( "c_list[0][0] = %lf\n", c_list[0][0] );
    ////printf( "c_list[1][0] = %lf\n", c_list[1][0] );

}

void smulstraprim_ukernel_ref( dim_t  k,
                               dim_t  len,
                               float* restrict *alpha_list,
                               float* restrict a,
                               float* restrict b,
                               float* restrict *beta_list,
                               float* restrict *c_list, inc_t rs_c, inc_t cs_c,
                               auxinfo_t* data )
{
}

void cmulstraprim_ukernel_ref( dim_t k,
                               dim_t len,
                               scomplex* restrict *alpha_list,
                               scomplex* restrict a,
                               scomplex* restrict b,
                               scomplex* restrict *beta_list,
                               scomplex* restrict *c_list, inc_t rs_c, inc_t cs_c,
                               auxinfo_t* data )
{
}

void zmulstraprim_ukernel_ref( dim_t k,
                               dim_t len,
                               dcomplex* restrict *alpha_list,
                               dcomplex* restrict a,
                               dcomplex* restrict b,
                               dcomplex* restrict *beta_list,
                               dcomplex* restrict *c_list, inc_t rs_c, inc_t cs_c,
                               auxinfo_t* data )
{
}



